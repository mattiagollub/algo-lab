#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/hit_sample.in > test/result.out
diff test/hit_sample.out test/result.out 

echo "Test 1 competed!"

./main < test/hit_test.in > test/result.out
diff test/hit_test.out test/result.out

echo "Test 2 competed!"
