//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		Hit						    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	15.10.2015						   							  //
//									   										  //
//============================================================================//

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <iostream>
#include <cmath>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int n;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		
		bool hit = false;
		long x, y, a, b;
		std::cin >> x >> y >> a >> b;

		// Create ray
		K::Point_2 origin(x, y), through(a, b);
		K::Ray_2 ray(origin, through);

		// Read and check obstacles
		for (int i = 0; i < n; i++) {
			long r, s, t, u;
			std::cin >> r >> s >> t >> u;

			if (!hit) {
				// Create segment
				K::Point_2 start(r, s), end(t, u);
				K::Segment_2 segment(start, end);

				// Check for intersection
				if (CGAL::do_intersect(ray, segment))
					hit = true;
			}
		}

		if (hit)
			std::cout << "yes\n";
		else
			std::cout << "no\n";

		// Read next test
		std::cin >> n;	
	}
}
