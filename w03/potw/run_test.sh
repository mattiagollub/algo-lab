#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -O2 -std=c++11 main.cpp -o main

./main < test/sample.in > test/result.out
diff test/sample.out test/result.out
echo "Test sample competed!"

./main < test/snippets-test1.in > test/result.out
diff test/snippets-test1.out test/result.out 
echo "Test 1 competed!"

./main < test/snippets-test2.in > test/result.out
diff test/snippets-test2.out test/result.out
echo "Test 2 competed!"

./main < test/snippets-test3.in > test/result.out
diff test/snippets-test3.out test/result.out
echo "Test 3 competed!"
