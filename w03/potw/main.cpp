//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		Problem of the week - Search Snippets				   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	05.01.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

struct Occurrence {
	int word;	// 0-based
	int pos;	// 0-based

	bool operator < (const Occurrence& occ) const {
        return pos < occ.pos;
    }
};

void testcase() {
	
	// Read test settings
	int n;
	cin >> n;
	
	vector<int> occ_counts(n, 0);
	for (int i = 0; i < n; i++)
		cin >> occ_counts[i];

	// Create vector of occurrences
	vector<Occurrence> occurrences;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < occ_counts[i]; j++) {
			Occurrence o;
			o.word = i;
			cin >> o.pos;
			occurrences.push_back(o);
		}
	}
	sort(occurrences.begin(), occurrences.end());

	// Run a sliding window on all the occurrences, keeping of the shortest
	// snippet containing all the words.
	int a = 0, b = -1;
	int best_a = 1, best_b = INT_MAX;
	vector<int> counts(n, 0);
	int uniques = 0;
	do {
		// Extend the window by one
		b++;
		if (++counts[occurrences[b].word] == 1) uniques++;

		// If possible shrink the start of the window
		while (counts[occurrences[a].word] > 1) {
			counts[occurrences[a].word]--;
			a++;
		}

		// Update best interval if possible
		if (occurrences[b].pos - occurrences[a].pos < best_b - best_a && uniques == n) {
			best_a = occurrences[a].pos;
			best_b = occurrences[b].pos;
		}
		// cout << a << "-" << b << " " << best_a << "-" << best_b << " | " << uniques << "\n";
	} while (b < occurrences.size() - 1);

	cout << (best_b - best_a + 1) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
