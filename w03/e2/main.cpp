//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		Antenna					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.11.2015						   							  //
//									   										  //
//============================================================================//

#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <iostream>
#include <cmath>

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef CGAL::Min_circle_2_traits_2<K> Traits;
typedef CGAL::Min_circle_2<Traits> Min_circle;

double floor_to_double(const K::FT& x) {
	double a = std::floor(CGAL::to_double(x));
	while (a > x) a -= 1;
	while (a+1 <= x) a += 1;
	return a;
}

double ceil_to_double(const K::FT& x) {
	double a = std::ceil(CGAL::to_double(x));
	while (a < x) a += 1;
	while (a-1 >= x) a -= 1;
	return a;
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read citizens
		K::Point_2 C[n];
		for (int i = 0; i < n; i++) {
			long x, y;
			std::cin >> x >> y;
			C[i] = K::Point_2(x, y);
		}

		// Find minimum circle
		Min_circle mc(C, C + n, true);

		// Write output
		Traits::Circle c = mc.circle();
		std::cout << ceil_to_double(sqrt(c.squared_radius())) << '\n';

		// Read next test
		std::cin >> n;	
	}
}
