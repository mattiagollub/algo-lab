#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/almost-antenna_sample.in > test/result.out
diff test/almost-antenna_sample.out test/result.out 

echo "Test 1 competed!"

./main < test/almost-antenna_test.in > test/result.out
diff test/almost-antenna_test.out test/result.out

echo "Test 2 competed!"
