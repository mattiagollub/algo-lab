//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		First Hit						    						   		  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.11.2015						   							  //
//									   										  //
//============================================================================//

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <cmath>

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;

double floor_to_double(const K::FT& x) {
	double a = std::floor(CGAL::to_double(x));
	while (a > x) a -= 1;
	while (a+1 <= x) a += 1;
	return a;
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		
		long x, y, a, b;
		std::cin >> x >> y >> a >> b;

		// Create ray
		P origin(x, y), through(a, b);
		K::Ray_2 ray(origin, through);

		// Informations about the nearest hit so far
		bool intersection_found = false;
		K::FT nearest_dist_squared;
		P nearest_intersection;

		// Read and check obstacles
		for (int i = 0; i < n; i++) {
			long r, s, t, u;
			std::cin >> r >> s >> t >> u;

			// Create segment
			P start(r, s), end(t, u);
			K::Segment_2 segment(start, end);

			// Check for intersection
			if (CGAL::do_intersect(ray, segment)) {
				K::FT dist_squared;
				P intersection;

				auto o = CGAL::intersection(ray, segment);
				if (const P* op = boost::get<P>(&*o)) {
					intersection = *op;
					dist_squared = CGAL::squared_distance(intersection, origin);
				}
				else if (const K::Segment_2* op = boost::get<K::Segment_2>(&*o)) {
					// Pick nearest endpoint
					K::FT dist_squared_s = CGAL::squared_distance(op->source(), origin);
					K::FT dist_squared_t = CGAL::squared_distance(op->target(), origin);
					if (dist_squared_s < dist_squared_t) {
						intersection = op->source();
						dist_squared = dist_squared_s;
					}
					else {
						intersection = op->target();
						dist_squared = dist_squared_t;
					}
				}
				else {
					std::cout << "No please :/\n";
				}

				if (!intersection_found || dist_squared < nearest_dist_squared) {
					intersection_found = true;
					nearest_dist_squared = dist_squared;
					nearest_intersection = intersection;
				}
			}
		}

		if (intersection_found)
			std::cout <<
				floor_to_double(nearest_intersection.x()) << " " <<
				floor_to_double(nearest_intersection.y()) << "\n";
		else
			std::cout << "no\n";

		// Read next test
		std::cin >> n;	
	}
}
