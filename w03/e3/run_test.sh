#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/firsthit_sample.in > test/result.out
diff test/firsthit_sample.out test/result.out 

echo "Test 1 competed!"

./main < test/firsthit_test.in > test/result.out
diff test/firsthit_test.out test/result.out

echo "Test 2 competed!"
