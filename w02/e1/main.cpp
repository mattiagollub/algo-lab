//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Race Tracks				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <queue>
#include <limits.h>

using namespace std;

struct node {
	int x;
	int y;
	int vx;
	int vy;
	int distance;
	bool free;
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int X, Y, start_x, start_y, end_x, end_y, P;
		cin >> X;
		cin >> Y;
		cin >> start_x;
		cin >> start_y;
		cin >> end_x;
		cin >> end_y;
		cin >> P;

		// Build grid
		vector<vector<bool>> free(Y, vector<bool>(X, true));
		for (int p = 0; p < P; p++) {

			int x1, y1, x2, y2;
			cin >> x1;
			cin >> y1;
			cin >> x2;
			cin >> y2;

			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x <= x2; x++) {
					free[y][x] = false;
				}
			}
		}

		// Build graph
		node* start = nullptr;
		vector<vector<vector<node>>> graph(Y, vector<vector<node>>(X, vector<node>(7 * 7)));
		for (int y = 0; y < Y; y++) {
			for (int x = 0; x < X; x++) {
				for (int vy = -3; vy <= 3; vy++) {
					for (int vx = -3; vx <= 3; vx++) {
						graph[y][x][(vy + 3) * 7 + (vx + 3)] = { x, y, vx, vy, INT_MAX, free[y][x] };
						if (x == start_x && y == start_y && vx == 0 && vy == 0)
							start = &graph[y][x][(vy + 3) * 7 + (vx + 3)];
					}
				}
			}
		}
		start->distance = 0;

		// Perform BFS on the graph
		queue<node*> Q;
		Q.push(start);
		node* end = nullptr;

		while (Q.size() > 0) {
			node* current = Q.front();
			Q.pop();

			if (current->x == end_x && current->y == end_y) {
				end = current;
				break;
			}

			for (int vx = max(current->vx - 1, -3); vx <= min(current->vx + 1, 3); vx++) {
				for (int vy = max(current->vy - 1, -3); vy <= min(current->vy + 1, 3); vy++) {
					int new_x = current->x + vx;
					int new_y = current->y + vy;
					if (new_x < 0 || new_y < 0 || new_x >= X || new_y >= Y)
						continue;

					node* next = &graph[new_y][new_x][(vy + 3) * 7 + (vx + 3)];
					if (next->free && next->distance > current->distance + 1) {
						next->distance = current->distance + 1;
						Q.push(next);
					}
				}
			}
		}

		// Output results
		if (end == nullptr)
			cout << "No solution.\n";
		else
			cout << "Optimal solution takes " << end->distance << " hops.\n";
	}
}
