#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -std=c++11 main.cpp -o ex

./ex < test/racetracks_sample.in > test/result.out
diff test/racetracks_sample.out test/result.out 

echo "Test 1 competed!"

./ex < test/racetracks_test.in > test/result.out
diff test/racetracks_test.out test/result.out

echo "Test 2 competed!"
