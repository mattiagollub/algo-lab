//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Problem of the week - Next Path						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	21.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <queue>
#include <vector>
#include <limits.h>

using namespace std;

struct Edge;
struct Node {
	int name;
	vector<Edge*> exiting;
	int visits;
};

struct Edge {
	Node* start;
	Node* end;
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n, m;
		int start_node, end_node;
		vector<Node> nodes;
		vector<Edge> edges;	

		cin >> n;
		cin >> m;
		cin >> start_node;
		cin >> end_node;	

		// Create graph
		for (int i = 0; i < n; i++)
			nodes.push_back({ i + 1, vector<Edge*>(), 0 });
		for (int e = 0; e < m; e++) {
			int si, ti;
			cin >> si;
			cin >> ti;
			edges.push_back({ &nodes[si - 1], &nodes[ti - 1] });
		}
		for (int e = 0; e < edges.size(); e++)
			edges[e].start->exiting.push_back(&(edges[e]));

		// Find (Second) Shortest Path using BFS
		queue<Node*> todo;
		Node* marker = nullptr;
		Node* end = &nodes[end_node - 1];
		bool found = false;
		int distance = 0;

		todo.push(&nodes[start_node - 1]);
		todo.push(marker);

		while (!todo.empty() && !found) {
			Node* node = todo.front();
			todo.pop();	

			if (node == marker) {
				if (!todo.empty()) todo.push(marker);
				distance++;  // Starting from now the distance of all upcoming nodes increases
			}
			else {
				node->visits++;
				if (node == end && node->visits == 2)
					found = true;
				if (node->visits <= 3) {
					for (Edge* exiting : node->exiting)
						todo.push(exiting->end);
				}
			}
		}

		// Write result
		if (!found)
			cout << "no" << '\n';
		else
			cout << distance << '\n';
	}
}
