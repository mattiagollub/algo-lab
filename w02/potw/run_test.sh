#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -std=c++11 main.cpp -o ex

./ex < test/sample.in > test/result.out
diff test/sample.out test/result.out 

echo "Test competed!"
