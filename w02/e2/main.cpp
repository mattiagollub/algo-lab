//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Boats				 	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>

using namespace std;

struct Boat {
	int l;
	int p;

	bool operator < (const Boat& boat) const {
		return p < boat.p;	
	}
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n;
		cin >> n;

		// Read boat data
		vector<Boat> boats;
		for (int b = 0; b < n; b++) {

			int l, p;
			cin >> l;
			cin >> p;

			boats.push_back({l, p});
		}

		// Sort boats in ascending ring position order
		sort(boats.begin(), boats.end());		

		// Find optimal placement
		int right_prev = INT_MIN;
		int right_cur = INT_MIN;
		int tied = 0;

		for (Boat boat : boats) {
			if (right_cur <= boat.p) {
				right_prev = right_cur;
				right_cur = max(boat.p, right_prev + boat.l);
				tied++;			
			}
			else {
				int new_boat_right = max(boat.p, right_prev + boat.l);
				right_cur = min(new_boat_right, right_cur);
			}
		}

		// Output result
		cout << tied << "\n";
	}
}
