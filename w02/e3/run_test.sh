#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -O2 -std=c++11 main.cpp -o ex

./ex < test/sample.in > test/result.out
diff test/sample.out test/result.out 
echo "Test 1 competed!"

./ex < test/sample1.in > test/result.out
diff test/sample1.out test/result.out 
echo "Test 2 competed!"

./ex < test/sample2.in > test/result.out
diff test/sample2.out test/result.out 
echo "Test 3 competed!"

./ex < test/sample3.in > test/result.out
diff test/sample3.out test/result.out 
echo "Test 4 competed!"
