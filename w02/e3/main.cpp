//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Boats				 	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Alien {
	int p;
	int q;

	bool operator < (const Alien& alien) const {
		return p < alien.p || (p == alien.p && q > alien.q);
	}
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n, m;
		cin >> n;
		cin >> m;

		// Read alien data
		vector<Alien> aliens;

		for (int a = 0; a < n; a++) {

			int p, q;
			cin >> p;
			cin >> q;

			aliens.push_back({ p, q });
		}

		// Sort aliens in ascending order and remove duplicates
		sort(aliens.begin(), aliens.end());

		// Find superior aliens
		int superiors = 0;
		int ui = 0;
		bool superiors_exist = true;

		int last_p = 0;
		int last_q = 0;
		bool last_removed = false;

		for (int a = 0; a < aliens.size(); a++) {
			Alien current = aliens[a];
			int next_p = (a < aliens.size() - 1) ? aliens[a + 1].p : current.p + 1;
			int next_q = (a < aliens.size() - 1) ? aliens[a + 1].q : current.q + 1;

			if (current.p >= last_p && current.q <= last_q) {
				// Skip this alien, as it cannot be superior to everyone.
			}
			else if (next_p == current.p && next_q == current.q) {
				if (current.p > ui + 1) {
					superiors_exist = false;
				}
				else {
					ui = current.q;
				}
				last_p = current.p;
				last_q = current.q;
			}
			else {
				if (current.p > ui + 1) {
					superiors_exist = false;
				}
				else {
					ui = current.q;
					superiors++;
				}
				last_p = current.p;
				last_q = current.q;
			}
		}

		if (ui < m)
			superiors_exist = false;

		// Output result
		cout << (superiors_exist ? superiors : 0) << '\n';
	}
}
