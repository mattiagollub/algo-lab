#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -std=c++11 main.cpp -o main

./main < test/shopping_sample.in > test/result.out
diff test/shopping_sample.out test/result.out 
echo "Test 1 competed!"

./main < test/shopping_sample2.in > test/result.out
diff test/shopping_sample2.out test/result.out
echo "Test 2 competed!"
