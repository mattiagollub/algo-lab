//============================================================================//
//									     									  //
//		Algorithms Lab - Week 6						     					  //
//		Problem of the week - The Great Game				   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	08.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

struct Position {
	int moves_for;		// Moves needed if you want to take this meeple to the end
	int moves_against;	// Moves needed if you don't want to take this meeple to the end
};

void testcase() {
	
	// Read testcase settings
	int n, m, r, b;
	cin >> n >> m >> r >> b;

	// Read transitions
	vector<vector<int>> t(n, vector<int>());
	vector<Position> p(n + 1);
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		t[u].push_back(v);
	}

	// Precompute winning possibilities
	int pos = n - 1;
	p[n] = { 0, 0 };
	while (pos >= min(r, b)) {
		int moves_for = INT_MAX;
		int moves_against = 0;
		for (int d : t[pos]) {
			moves_for = min(p[d].moves_against + 1, moves_for);
			moves_against = max(p[d].moves_for + 1, moves_against);
		}
		p[pos].moves_for = moves_for;
		p[pos].moves_against = moves_against;
		pos--;
	}

	// Output results
	if (p[r].moves_for < p[b].moves_for) cout << "0\n";
	else if (p[r].moves_for > p[b].moves_for) cout << "1\n";
	else if ((p[r].moves_for % 2) == 1 && p[r].moves_for == p[b].moves_for) cout << "0\n";
	else cout << "1\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
