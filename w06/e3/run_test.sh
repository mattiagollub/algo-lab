#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -std=c++11 main.cpp -o main

./main < test/defence.sin > test/result.out
diff test/defence.sout test/result.out 
echo "Test 1 competed!"

./main < test/defence.tin > test/result.out
diff test/defence.tout test/result.out
echo "Test 2 competed!"
