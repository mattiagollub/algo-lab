//============================================================================//
//									     									  //
//		Algorithms Lab - Week 6						     					  //
//		Kingdom Defence			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	26.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> 				Traits;
typedef adjacency_list<vecS, vecS, directedS,
	no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor>>>> 	Graph;
typedef	property_map<Graph, edge_capacity_t>::type					EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type			ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type					ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor						Vertex;
typedef	graph_traits<Graph>::edge_descriptor						Edge;
typedef graph_traits<Graph>::edge_iterator							EdgeIt;

// Custom EdgeAdder that encapsulates the references to the graph and its
// property maps and always creates reverse edges with corresponding capacities.
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {

	// Read players and rounds
	int n, m;
	cin >> n >> m;

	// Build empty graph
	Graph G(n + 2);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);

	// Create nodes
	int source = n;
	int sink = n + 1;
	vector<int> nodes(n, 0);
	for (int i = 0; i < n; i++) {
		int g, d;
		cin >> g >> d;
		nodes[i] = g - d;
	}

	// Inser path edges
	for (int i = 0; i < m; i++) {
		int f, t, c, C;
		cin >> f >> t >> c >> C;
		if (f == t) continue;
		nodes[f] -= c;
		nodes[t] += c;
		ea.addEdge(f, t, C - c);

		// cout << "Edge " << f << "-" << t << " with capacity " << (C - c) << "\n";
	}

	// Insert source->city and city->sink edges
	int total_demand = 0;
	for (int i = 0; i < n; i++) {
		int balance = nodes[i];
		if (balance > 0) {
			ea.addEdge(source, i, balance);
			// cout << "Edge " << "source" << "-" << i << " with capacity " << balance << "\n";
		}
		if (balance < 0) {
			ea.addEdge(i, sink, -balance);
			total_demand += -balance;
			// cout << "Edge " << i << "-" << "sink" << " with capacity " << -balance << "\n";
		}
	}

	// Compute maximum flow
	int maximum_flow = push_relabel_max_flow(G, source, sink);
	// cout << "Max flow: " << maximum_flow << " with total demand: " << total_demand << "\n";

	// Write result
	if (maximum_flow == total_demand)
		cout << "yes\n";
	else
		cout << "no\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}
