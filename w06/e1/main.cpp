//============================================================================//
//									     									  //
//		Algorithms Lab - Week 6						     					  //
//		Coin Tossing Tournament	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> 				Traits;
typedef adjacency_list<vecS, vecS, directedS,
	no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor>>>> 	Graph;
typedef	property_map<Graph, edge_capacity_t>::type					EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type			ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type					ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor						Vertex;
typedef	graph_traits<Graph>::edge_descriptor						Edge;
typedef graph_traits<Graph>::edge_iterator							EdgeIt;

// Custom EdgeAdder that encapsulates the references to the graph and its
// property maps and always creates reverse edges with corresponding capacities.
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {

	// Read players and rounds
	int n, m;
	cin >> n >> m;

	// Build empty graph
	Graph G(1 + m + n + 1);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);

	// Create nodes
	int source = 0;
	int sink = 1 + m + n;
	vector<int> rounds(m, 0);
	vector<int> players(n, 0);
	for (int i = 0; i < m; i++) rounds[i] = 1 + i;
	for (int i = 0; i < n; i++) players[i] = 1 + m + i;

	// Insert source -> games edges
	for (int i = 0; i < m; i++) ea.addEdge(source, rounds[i], 1);

	// Insert games -> players edges
	for (int i = 0; i < m; i++) {
		int a, b, c;
		cin >> a >> b >> c;
		if (c != 2) ea.addEdge(rounds[i], players[a], 1);
		if (c != 1) ea.addEdge(rounds[i], players[b], 1);
	}

	// Insert players -> sink edges
	int total_score = 0;
	for (int i = 0; i < n; i++) {
		int score;
		cin >> score;
		total_score += score;
		ea.addEdge(players[i], sink, score);
	}

	// Compute maximum flow
	int maximum_flow = push_relabel_max_flow(G, source, sink);

	// Write result
	if (maximum_flow == total_score && maximum_flow == m)
		cout << "yes\n";
	else
		cout << "no\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}
