//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Build The Sum							   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	
	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {
		int n;
		cin >> n;

		// Run test case
		double sum = 0.0;
		double x = 0.0;

		for (int i = 0; i < n; i++) {
			cin >> x;
			sum += x;
		}

		cout << sum << '\n';
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Dominoes								   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {

	// Make file I/O faster
	std::ios_base::sync_with_stdio(false);	

	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {
		int n;
		cin >> n;

		// Run test case
		int max_pos = 2;  // Furthest position reachable with the current tiles
		int height = 0;	  // Height of the current tile
		int cur_pos = 0;  // Position of the current tile
		int i = 0;

		for (i = 0; i < n; i++) {

			cur_pos += 1;
			cin >> height;

			// If the current tile has been reached by a falling tile, then let
			// it fall too and update the furthest reachable position.
			if (cur_pos < max_pos)
				max_pos = max(max_pos, cur_pos + height);
			else {
				cur_pos -= 1;
				break;
			}
		}

		// Ignore remaining tiles
		for (i = i + 1; i < n; i++) {
			cin >> height;
		}

		cout << cur_pos << '\n';
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Shelves									   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {

	// Make file I/O faster
	std::ios_base::sync_with_stdio(false);	

	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {

		// Read test data		
		int l, m, n;
		cin >> l;
		cin >> m;
		cin >> n;
 
		// Find optimal coverage using only 
		int ncount = l / n;
		int mcount = (l - n * ncount) / m;
		int coverage = ncount * n + m * mcount;

		int best_ncount = ncount;
		int best_mcount = mcount;
		int best_coverage = coverage;

		int steps = 0;

		// Progressively substitute shelves of type 'n' with shelves of type 'm'
		// in order to find the best coverage.
		while (ncount > 0 && steps <= n) {
			
			steps += 1;
			ncount -= 1;
			mcount = (l - n * ncount) / m;
			coverage = n * ncount + m * mcount;

			// If we found a better combination, update shelve counts.
			if (coverage > best_coverage) {
				best_ncount = ncount;
				best_mcount = mcount;
				best_coverage = coverage;
			}

			// Stop if we already reached the best solution
			if (coverage == l)
				break;
		}

		// Write result
		cout << best_mcount << ' '
			 << best_ncount << ' '
			 << l - best_coverage << '\n';
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Even Matrices							   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

using namespace std;

// Add src to dst and leave the resul tin dst
void add_to_vector(vector<int>& dst, vector<int>& src) {
	
	for (int i = 0; i < dst.size(); i++)
		dst[i] = (src[i] == dst[i]) ? 0 : 1;
}

int main(int argc, char *argv[]) {

	// Make file I/O faster
	std::ios_base::sync_with_stdio(false);	

	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {

		// Read test data		
		int n;
		cin >> n;

		vector<vector<int>> M(n, vector<int>(n));
		vector<vector<int>> original(n, vector<int>(n));

		// Read matrix
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				cin >> M[i][j];
				original[i][j] = M[i][j];
			}		
		}

		// Run test case
		int total = 0;

		// Use the algorithm of the 'Even Pairs' problem on rows of different
		// heights. Runs in O(n^3).
		for (int b = 0; b < n; b++) {

			// Merge rows
			if (b != 0) {
				for (int r = 0; r < n - b; r++) {
					add_to_vector(M[r], original[r + b]);
				}
			}

			// Given a row height, iterate over each row.
			for (int i = 0; i < n - b; i++) {
				int open_1 = 0;
				int open_2 = -1;

				// Run the algorithm like in the 'Even Pairs' problem	
				for (int j = 0; j < n; j++) {

					// Read next bit
					int bit = M[i][j];

					// Swap and increment number of open even sequences 
					if (bit == 1)
						swap(open_1, open_2);
					open_1 += 1;

					// Add number of open sequences to the total
					total += open_1;
				}
			}
		}

		// Write result
		cout << total << '\n';
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Problem of the week - 	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	21.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>

using namespace std;



int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n, k;
		set<int> lighter, heavier, unweighted;
		cin >> n;
		cin >> k;

		// Initially insert all coins
		for (int c = 1; c <= n; c++) {
			lighter.insert(c);
			heavier.insert(c);
			unweighted.insert(c);
		}

		// Then progressively exclude coins using the informations given by the
		// weights
		for (int w = 0; w < k; w++) {

			int pi;
			set<int> ids_left, ids_right;
			char comp;
			cin >> pi;

			// Read coin identifiers and comparison
			for (int i = 0; i < pi; i++) {
				int id;
				cin >> id;
				ids_left.insert(id);
				unweighted.erase(id);
			}
			for (int i = 0; i < pi; i++) {
				int id;
				cin >> id;
				ids_right.insert(id);
				unweighted.erase(id);
			}
			cin >> comp;

			// Analyze data
			if (comp == '=') {
				for (auto it = ids_left.begin(); it != ids_left.end(); it++) {
					lighter.erase(*it);
					heavier.erase(*it);
				}
				for (auto it = ids_right.begin(); it != ids_right.end(); it++) {
					lighter.erase(*it);
					heavier.erase(*it);
				}
			}
			else {

				set<int> new_lighter, new_heavier;
				set<int>* lighter_ids;
				set<int>* heavier_ids;

				if (comp == '<') {
					lighter_ids = &ids_left;
					heavier_ids = &ids_right;
				}
				else {
					lighter_ids = &ids_right;
					heavier_ids = &ids_left;
				}

				set_intersection(lighter_ids->begin(), lighter_ids->end(),
								 lighter.begin(), lighter.end(),
								 inserter(new_lighter, new_lighter.begin()));
				set_intersection(heavier_ids->begin(), heavier_ids->end(),
								 heavier.begin(), heavier.end(),
								 inserter(new_heavier, new_heavier.begin()));

				lighter = new_lighter;
				heavier = new_heavier;
			}
		}

		// Output result
		set<int> candidates;
		set_symmetric_difference(lighter.begin(), lighter.end(),
								 heavier.begin(), heavier.end(),
								 inserter(candidates, candidates.begin()));
		
		if (n == 1)
			cout << 1;
		else if (candidates.size() == 1 && unweighted.size() <= 1)
			cout << *candidates.begin();
		else if (unweighted.size() == 1 && candidates.size() <= 1)
			cout << *unweighted.begin();		
		else
			cout << 0;

		cout << '\n';
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Even Pairs								   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <algorithm>

using namespace std;

int main(int argc, char *argv[]) {
	
	int n;
	cin >> n;

	// Run test case
	int total = 0;
	int open_1 = 0;
	int open_2 = -1;

	for (int i = 0; i < n; i++) {

		// Read next bit
		int bit;
		cin >> bit;

		// Swap and increment number of open even sequences 
		if (bit == 1)
			swap(open_1, open_2);
		open_1 += 1;

		// Add number of open sequences to the total
		total += open_1;
	}

	cout << total << '\n';
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Race Tracks				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <queue>
#include <limits.h>

using namespace std;

struct node {
	int x;
	int y;
	int vx;
	int vy;
	int distance;
	bool free;
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int X, Y, start_x, start_y, end_x, end_y, P;
		cin >> X;
		cin >> Y;
		cin >> start_x;
		cin >> start_y;
		cin >> end_x;
		cin >> end_y;
		cin >> P;

		// Build grid
		vector<vector<bool>> free(Y, vector<bool>(X, true));
		for (int p = 0; p < P; p++) {

			int x1, y1, x2, y2;
			cin >> x1;
			cin >> y1;
			cin >> x2;
			cin >> y2;

			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x <= x2; x++) {
					free[y][x] = false;
				}
			}
		}

		// Build graph
		node* start = nullptr;
		vector<vector<vector<node>>> graph(Y, vector<vector<node>>(X, vector<node>(7 * 7)));
		for (int y = 0; y < Y; y++) {
			for (int x = 0; x < X; x++) {
				for (int vy = -3; vy <= 3; vy++) {
					for (int vx = -3; vx <= 3; vx++) {
						graph[y][x][(vy + 3) * 7 + (vx + 3)] = { x, y, vx, vy, INT_MAX, free[y][x] };
						if (x == start_x && y == start_y && vx == 0 && vy == 0)
							start = &graph[y][x][(vy + 3) * 7 + (vx + 3)];
					}
				}
			}
		}
		start->distance = 0;

		// Perform BFS on the graph
		queue<node*> Q;
		Q.push(start);
		node* end = nullptr;

		while (Q.size() > 0) {
			node* current = Q.front();
			Q.pop();

			if (current->x == end_x && current->y == end_y) {
				end = current;
				break;
			}

			for (int vx = max(current->vx - 1, -3); vx <= min(current->vx + 1, 3); vx++) {
				for (int vy = max(current->vy - 1, -3); vy <= min(current->vy + 1, 3); vy++) {
					int new_x = current->x + vx;
					int new_y = current->y + vy;
					if (new_x < 0 || new_y < 0 || new_x >= X || new_y >= Y)
						continue;

					node* next = &graph[new_y][new_x][(vy + 3) * 7 + (vx + 3)];
					if (next->free && next->distance > current->distance + 1) {
						next->distance = current->distance + 1;
						Q.push(next);
					}
				}
			}
		}

		// Output results
		if (end == nullptr)
			cout << "No solution.\n";
		else
			cout << "Optimal solution takes " << end->distance << " hops.\n";
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Boats				 	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>

using namespace std;

struct Boat {
	int l;
	int p;

	bool operator < (const Boat& boat) const {
		return p < boat.p;	
	}
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n;
		cin >> n;

		// Read boat data
		vector<Boat> boats;
		for (int b = 0; b < n; b++) {

			int l, p;
			cin >> l;
			cin >> p;

			boats.push_back({l, p});
		}

		// Sort boats in ascending ring position order
		sort(boats.begin(), boats.end());		

		// Find optimal placement
		int right_prev = INT_MIN;
		int right_cur = INT_MIN;
		int tied = 0;

		for (Boat boat : boats) {
			if (right_cur <= boat.p) {
				right_prev = right_cur;
				right_cur = max(boat.p, right_prev + boat.l);
				tied++;			
			}
			else {
				int new_boat_right = max(boat.p, right_prev + boat.l);
				right_cur = min(new_boat_right, right_cur);
			}
		}

		// Output result
		cout << tied << "\n";
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Boats				 	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Alien {
	int p;
	int q;

	bool operator < (const Alien& alien) const {
		return p < alien.p || (p == alien.p && q > alien.q);
	}
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n, m;
		cin >> n;
		cin >> m;

		// Read alien data
		vector<Alien> aliens;

		for (int a = 0; a < n; a++) {

			int p, q;
			cin >> p;
			cin >> q;

			aliens.push_back({ p, q });
		}

		// Sort aliens in ascending order and remove duplicates
		sort(aliens.begin(), aliens.end());

		// Find superior aliens
		int superiors = 0;
		int ui = 0;
		bool superiors_exist = true;

		int last_p = 0;
		int last_q = 0;
		bool last_removed = false;

		for (int a = 0; a < aliens.size(); a++) {
			Alien current = aliens[a];
			int next_p = (a < aliens.size() - 1) ? aliens[a + 1].p : current.p + 1;
			int next_q = (a < aliens.size() - 1) ? aliens[a + 1].q : current.q + 1;

			if (current.p >= last_p && current.q <= last_q) {
				// Skip this alien, as it cannot be superior to everyone.
			}
			else if (next_p == current.p && next_q == current.q) {
				if (current.p > ui + 1) {
					superiors_exist = false;
				}
				else {
					ui = current.q;
				}
				last_p = current.p;
				last_q = current.q;
			}
			else {
				if (current.p > ui + 1) {
					superiors_exist = false;
				}
				else {
					ui = current.q;
					superiors++;
				}
				last_p = current.p;
				last_q = current.q;
			}
		}

		if (ui < m)
			superiors_exist = false;

		// Output result
		cout << (superiors_exist ? superiors : 0) << '\n';
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Problem of the week - Next Path						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	21.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <queue>
#include <vector>
#include <limits.h>

using namespace std;

struct Edge;
struct Node {
	int name;
	vector<Edge*> exiting;
	int visits;
};

struct Edge {
	Node* start;
	Node* end;
};

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n, m;
		int start_node, end_node;
		vector<Node> nodes;
		vector<Edge> edges;	

		cin >> n;
		cin >> m;
		cin >> start_node;
		cin >> end_node;	

		// Create graph
		for (int i = 0; i < n; i++)
			nodes.push_back({ i + 1, vector<Edge*>(), 0 });
		for (int e = 0; e < m; e++) {
			int si, ti;
			cin >> si;
			cin >> ti;
			edges.push_back({ &nodes[si - 1], &nodes[ti - 1] });
		}
		for (int e = 0; e < edges.size(); e++)
			edges[e].start->exiting.push_back(&(edges[e]));

		// Find (Second) Shortest Path using BFS
		queue<Node*> todo;
		Node* marker = nullptr;
		Node* end = &nodes[end_node - 1];
		bool found = false;
		int distance = 0;

		todo.push(&nodes[start_node - 1]);
		todo.push(marker);

		while (!todo.empty() && !found) {
			Node* node = todo.front();
			todo.pop();	

			if (node == marker) {
				if (!todo.empty()) todo.push(marker);
				distance++;  // Starting from now the distance of all upcoming nodes increases
			}
			else {
				node->visits++;
				if (node == end && node->visits == 2)
					found = true;
				if (node->visits <= 3) {
					for (Edge* exiting : node->exiting)
						todo.push(exiting->end);
				}
			}
		}

		// Write result
		if (!found)
			cout << "no" << '\n';
		else
			cout << distance << '\n';
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		Hit						    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	15.10.2015						   							  //
//									   										  //
//============================================================================//

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <iostream>
#include <cmath>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int n;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		
		bool hit = false;
		long x, y, a, b;
		std::cin >> x >> y >> a >> b;

		// Create ray
		K::Point_2 origin(x, y), through(a, b);
		K::Ray_2 ray(origin, through);

		// Read and check obstacles
		for (int i = 0; i < n; i++) {
			long r, s, t, u;
			std::cin >> r >> s >> t >> u;

			if (!hit) {
				// Create segment
				K::Point_2 start(r, s), end(t, u);
				K::Segment_2 segment(start, end);

				// Check for intersection
				if (CGAL::do_intersect(ray, segment))
					hit = true;
			}
		}

		if (hit)
			std::cout << "yes\n";
		else
			std::cout << "no\n";

		// Read next test
		std::cin >> n;	
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		Antenna					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.11.2015						   							  //
//									   										  //
//============================================================================//

#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <iostream>
#include <cmath>

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef CGAL::Min_circle_2_traits_2<K> Traits;
typedef CGAL::Min_circle_2<Traits> Min_circle;

double floor_to_double(const K::FT& x) {
	double a = std::floor(CGAL::to_double(x));
	while (a > x) a -= 1;
	while (a+1 <= x) a += 1;
	return a;
}

double ceil_to_double(const K::FT& x) {
	double a = std::ceil(CGAL::to_double(x));
	while (a < x) a += 1;
	while (a-1 >= x) a -= 1;
	return a;
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read citizens
		K::Point_2 C[n];
		for (int i = 0; i < n; i++) {
			long x, y;
			std::cin >> x >> y;
			C[i] = K::Point_2(x, y);
		}

		// Find minimum circle
		Min_circle mc(C, C + n, true);

		// Write output
		Traits::Circle c = mc.circle();
		std::cout << ceil_to_double(sqrt(c.squared_radius())) << '\n';

		// Read next test
		std::cin >> n;	
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		First Hit						    						   		  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.11.2015						   							  //
//									   										  //
//============================================================================//

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <cmath>

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;

double floor_to_double(const K::FT& x) {
	double a = std::floor(CGAL::to_double(x));
	while (a > x) a -= 1;
	while (a+1 <= x) a += 1;
	return a;
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		
		long x, y, a, b;
		std::cin >> x >> y >> a >> b;

		// Create ray
		P origin(x, y), through(a, b);
		K::Ray_2 ray(origin, through);

		// Informations about the nearest hit so far
		bool intersection_found = false;
		K::FT nearest_dist_squared;
		P nearest_intersection;

		// Read and check obstacles
		for (int i = 0; i < n; i++) {
			long r, s, t, u;
			std::cin >> r >> s >> t >> u;

			// Create segment
			P start(r, s), end(t, u);
			K::Segment_2 segment(start, end);

			// Check for intersection
			if (CGAL::do_intersect(ray, segment)) {
				K::FT dist_squared;
				P intersection;

				auto o = CGAL::intersection(ray, segment);
				if (const P* op = boost::get<P>(&*o)) {
					intersection = *op;
					dist_squared = CGAL::squared_distance(intersection, origin);
				}
				else if (const K::Segment_2* op = boost::get<K::Segment_2>(&*o)) {
					// Pick nearest endpoint
					K::FT dist_squared_s = CGAL::squared_distance(op->source(), origin);
					K::FT dist_squared_t = CGAL::squared_distance(op->target(), origin);
					if (dist_squared_s < dist_squared_t) {
						intersection = op->source();
						dist_squared = dist_squared_s;
					}
					else {
						intersection = op->target();
						dist_squared = dist_squared_t;
					}
				}
				else {
					std::cout << "No please :/\n";
				}

				if (!intersection_found || dist_squared < nearest_dist_squared) {
					intersection_found = true;
					nearest_dist_squared = dist_squared;
					nearest_intersection = intersection;
				}
			}
		}

		if (intersection_found)
			std::cout <<
				floor_to_double(nearest_intersection.x()) << " " <<
				floor_to_double(nearest_intersection.y()) << "\n";
		else
			std::cout << "no\n";

		// Read next test
		std::cin >> n;	
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		Almost Antenna			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.11.2015						   							  //
//									   										  //
//============================================================================//

#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <CGAL/utils.h>
#include <iostream>
#include <cmath>

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef CGAL::Min_circle_2_traits_2<K> Traits;
typedef CGAL::Min_circle_2<Traits> Min_circle;

double floor_to_double(const K::FT& x) {
	double a = std::floor(CGAL::to_double(x));
	while (a > x) a -= 1;
	while (a+1 <= x) a += 1;
	return a;
}

double ceil_to_double(const K::FT& x) {
	double a = std::ceil(CGAL::to_double(x));
	while (a < x) a += 1;
	while (a-1 >= x) a -= 1;
	return a;
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read citizens
		K::Point_2 C[n];
		for (int i = 0; i < n; i++) {
			long x, y;
			std::cin >> x >> y;
			C[i] = K::Point_2(x, y);
		}

		// Find minimum circle
		Min_circle mc(C, C + n, true);

		// Remove the citizen that reduce the radius by the most
		K::FT min_radius_squared = mc.circle().squared_radius();
		for (auto support = mc.support_points_begin();
				support != mc.support_points_end();
				support++) {
			K::Point_2 removed = *support;
			K::Point_2 A[n-1];
			int offset = 0;
			for (int i = 0; i < n; i++) {
				if (removed.x() == C[i].x() && removed.y() == C[i].y())
					offset = -1;
				else
					A[i + offset] = C[i];
			}
			Min_circle qmc(A, A + (n - 1), true);

			// Get new radius
			Traits::Circle c = qmc.circle();
			K::FT new_radius_squared = c.squared_radius();
			min_radius_squared = CGAL::min(min_radius_squared, new_radius_squared);
		}

		// Write output
		std::cout << ceil_to_double(sqrt(min_radius_squared)) << '\n';

		// Read next test
		std::cin >> n;	
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 3						     					  //
//		Problem of the week - Search Snippets				   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	05.01.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

struct Occurrence {
	int word;	// 0-based
	int pos;	// 0-based

	bool operator < (const Occurrence& occ) const {
        return pos < occ.pos;
    }
};

void testcase() {
	
	// Read test settings
	int n;
	cin >> n;
	
	vector<int> occ_counts(n, 0);
	for (int i = 0; i < n; i++)
		cin >> occ_counts[i];

	// Create vector of occurrences
	vector<Occurrence> occurrences;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < occ_counts[i]; j++) {
			Occurrence o;
			o.word = i;
			cin >> o.pos;
			occurrences.push_back(o);
		}
	}
	sort(occurrences.begin(), occurrences.end());

	// Run a sliding window on all the occurrences, keeping of the shortest
	// snippet containing all the words.
	int a = 0, b = -1;
	int best_a = 1, best_b = INT_MAX;
	vector<int> counts(n, 0);
	int uniques = 0;
	do {
		// Extend the window by one
		b++;
		if (++counts[occurrences[b].word] == 1) uniques++;

		// If possible shrink the start of the window
		while (counts[occurrences[a].word] > 1) {
			counts[occurrences[a].word]--;
			a++;
		}

		// Update best interval if possible
		if (occurrences[b].pos - occurrences[a].pos < best_b - best_a && uniques == n) {
			best_a = occurrences[a].pos;
			best_b = occurrences[b].pos;
		}
		// cout << a << "-" << b << " " << best_a << "-" << best_b << " | " << uniques << "\n";
	} while (b < occurrences.size() - 1);

	cout << (best_b - best_a + 1) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		First steps with BGL	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	11.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
			no_property,
			property<edge_weight_t, int> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef property_map<Graph, edge_weight_t>::type WeightMap;

void testcase() {

	// Read vertex and edge count	
	int V, E;
	cin >> V >> E;

	// Create graph
	Graph G(V);
	WeightMap W = get(edge_weight, G);

	// Fill graph
	for (int i = 0; i < E; i++) {
		int u, v, c;
		Edge e;
		cin >> u >> v >> c;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		W[e] = c;
	}

	// Find minimum spanning tree
	vector<Edge> mst; 
	kruskal_minimum_spanning_tree(G, back_inserter(mst));

	// Compute sum of the weights and build new graph
	int weights_sum = 0;
	vector<Edge>::iterator ebeg, eend = mst.end();
	for (ebeg = mst.begin(); ebeg != eend; ++ebeg)
		weights_sum += W[*ebeg];
	
	// Run dijkstra and find furthest point
	vector<Vertex> pred(V);	
	vector<int> dist(V);
	dijkstra_shortest_paths(G, 0,
		predecessor_map(
			make_iterator_property_map(
				pred.begin(),
				get(vertex_index, G)
			)
		).
		distance_map(
			make_iterator_property_map(
				dist.begin(),
				get(vertex_index, G)
			)
		)
	);
	int furthest = *max_element(dist.begin(), dist.end());

	// Write result
	cout << weights_sum << " " << furthest << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Ant Challenge			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	16.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
			no_property,
			property<edge_weight_t, int> > Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef property_map<Graph, edge_weight_t>::type WeightMap;

void testcase() {

	// Read problem settings	
	int T, E, S, a, b;
	cin >> T >> E >> S >> a >> b;

	// Read edge data
	Graph forest(T);
	Graph privates(T);
	WeightMap f_W = get(edge_weight, forest);
	WeightMap p_W = get(edge_weight, privates);
	vector<vector<int>> weights = vector<vector<int>>(S, vector<int>(E, 10000));
	vector<Edge> edges;

	for (int i = 0; i < E; i++) {
		int u, v;
		Edge e;
		cin >> u >> v;
		
		for (int s = 0; s < S; s++)
		 	cin >> weights[s][i];

		tie(e, tuples::ignore) = add_edge(u, v, forest);
		edges.push_back(e);
	}

	// Read hives positions
	vector<int> hives(S, 0);
	for (int h = 0; h < S; h++)
		cin >> hives[h];

	// Compute minimum spanning tree for each species and save weights
	for (int s = 0; s < S; s++) {		
		// Set weights
		for (int e = 0; e < E; e++)
			f_W[edges[e]] = weights[s][e];

		// Find minimum spanning tree (Prim's algorithm is teh one used 
		// by the ants for exploration)
		vector<Vertex> predecessors(T);
		prim_minimum_spanning_tree(forest, &predecessors[0], root_vertex(hives[s]));

		// Get edges belonging to the minimum spanning tree
		for (int v = 0; v < T; ++v) {
			Edge f_e, p_e; bool f_found, p_found;
			tie(f_e, f_found) = edge(v, predecessors[v], forest);
			tie(p_e, p_found) = edge(v, predecessors[v], privates);
			
			if (!f_found) continue;
			if (!p_found) {
				// Add edge if it doesn't exist in the tree
				tie(p_e, tuples::ignore) = add_edge(v, predecessors[v], privates);
				p_W[p_e] = f_W[f_e];				
			}
			else {
				// Set the weight of the edge to the minimum time until now
				p_W[p_e] = min(p_W[p_e], f_W[f_e]);
			}
		}
	}

	// Run dijkstra and find shortest path
	vector<Vertex> pred(T);
	vector<int> dist(T);
	dijkstra_shortest_paths(privates, a,
		predecessor_map(make_iterator_property_map(pred.begin(), get(vertex_index, privates))).
		distance_map(make_iterator_property_map(dist.begin(), get(vertex_index, privates))));

	// Write result
	cout << dist[b] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Important bridges		    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	16.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/biconnected_components.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace boost;

struct edge_component_t {
	enum {
		num = 555
	};
    typedef edge_property_tag kind;
} edge_component;

typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						property<edge_component_t, size_t>> Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph, edge_component_t>::type ComponentMap;

bool result_comparer(pair<int, int> i, pair<int, int> j) {
	return (i.first < j.first || (i.first == j.first && i.second <= j.second));
}

void testcase() {

	// Read problem settings	
	int V, E;
	cin >> V >> E;

	// Read edge data
	Graph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		add_edge(u-1, v-1, G);
	}

	// Find biconnected components
	ComponentMap C = get(edge_component, G);
	size_t num_components = biconnected_components(G, C);

	// If there is only one component we are done.
	// if (num_components <= 1) {
	//	cout << 0 << "\n";
	//	return;
	// }

	// Get edges for each component
	vector<vector<Edge>> components = vector<vector<Edge>>(num_components, vector<Edge>());
	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg)
		components[C[*ebeg]].push_back(*ebeg);
	
	// Find components with only one edge
	vector<pair<int, int>> bridges;
	for (int c = 0; c < components.size(); c++) {
		if (components[c].size() == 1) {
			Edge e = components[c][0];
			bridges.push_back(pair<int, int>(
				min(source(e, G), target(e, G)),
				max(source(e, G), target(e, G))));
		}
	}

	// Sort result
	sort(bridges.begin(), bridges.end());

	// Write result
	cout << bridges.size() << "\n";
	for (int b = 0; b < bridges.size(); b++)
		cout << bridges[b].first+1 << " " << bridges[b].second+1 << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Buddy selection			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <unordered_map>
#include <algorithm>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						property<edge_weight_t, int>> Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph, edge_weight_t>::type WeightMap;

void testcase() {

	// Read problem settings	
	int V, C, F;
	cin >> V >> C >> F;

	// Read edge data
	Graph G(V);
	Graph OG(V);
	WeightMap W = get(edge_weight, G);
	std::unordered_map<string, vector<int>> topics;

	for (int v = 0; v < V; v++) {
		for (int i = 0; i < C; i++) {
			// Read interest
			string topic;
			cin >> topic;
			
			// Find matching students
			auto interested = topics.find(topic);
			if (interested != topics.end()) {
				for (int student : interested->second) {
					Edge e; bool found;
					tie(e, found) = edge(v, student, G);
					if (!found) {
						tie(e, tuples::ignore) = add_edge(v, student, G);
						W[e] = 1;
					}
					else {
						W[e] += 1;
					}	
				}
			}

			// Save student interests
			if (topics.find(topic) == topics.end())
				topics[topic] = vector<int>();
			topics[topic].push_back(v);
		}		
	}

	// Recreate graph containing only the edges with enough common interests
	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg) {
		Edge e = *ebeg;
		if (W[e] >= F + 1) {
			add_edge(source(e, G), target(e, G), OG);
		}
	}

	// Find bipartite matching
	std::vector<Vertex> mate(V);
  	edmonds_maximum_cardinality_matching(OG, &mate[0]);
	int size = matching_size(OG, &mate[0]);

	// Write result
	if (size == V / 2)
		cout << "not optimal\n";
	else
		cout << "optimal\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Antenna					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	07.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cmath>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel	K;

using namespace std;

struct Triangle {
	pair<K::Point_2, K::Point_2> v[3];
};

void testcase() {
	
	// Read testcase settings
	int m, n;
	cin >> m >> n;

	// Read path points
	vector<K::Point_2> path;
	for (int i = 0; i < m; i++) {
		K::Point_2 p;
		cin >> p;
		path.push_back(p);
	}

	// Read triangles
	vector<Triangle> t(n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < 3; j++) {
			cin >> t[i].v[j].first;
			cin >> t[i].v[j].second;
		}
		// Swap points if necessary in order to preserve left-turn property
		for (int j = 0; j < 3; j++) {
			if (CGAL::right_turn(t[i].v[j].first, t[i].v[j].second, t[i].v[(j+1)%3].first))
				swap(t[i].v[j].first, t[i].v[j].second);
		}
	}

	// Determine which legs are contained in each triangle
	vector<vector<int>> l(n, vector<int>());
	for (int i = 0; i < n; i++) {
		bool prev_in = false;
		for (int p = 0; p < m; p++) {
			bool curr_in = true;
			for (int j = 0; j < 3; j++) {
				if (CGAL::right_turn(t[i].v[j].first, t[i].v[j].second, path[p]))
					curr_in = false;
			}
			if (prev_in && curr_in)
				l[i].push_back(p-1);
			prev_in = curr_in;
		}
	}

	// Similar to search snippets find the smallest interval containing all the legs
	int a = 0, b = -1;
	int best_a = 1, best_b = INT_MAX;
	int uniques = 0;
	vector<int> counts(m, 0);
	do  {
		// Extend the window by one
		b++;
		for (int leg : l[b]) {
			if (++counts[leg] == 1) uniques++;
		}

		// If possible shrink the start of the window
		bool can_shrink = true;
		while (can_shrink) {
			for (int leg : l[a]) {
				if (counts[leg] <= 1) can_shrink = false;
			}
			if (can_shrink) {
				for (int leg : l[a]) {
					counts[leg]--;
				}
				a++;
			}
		}

		// Update best interval if possible
		if (b - a < best_b - best_a && uniques == m - 1) {
			best_a = a;
			best_b = b;
		}
	} while (b < n - 1);

	// Print result
	cout << best_b - best_a + 1 << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		DHL						    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	18.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

using namespace std;

struct Data {

public:
	int N;
	vector<int> A;
	vector<int> B;
	vector<vector<int>> C;
	vector<int> SA;
	vector<int> SB;

	Data(const int& n)
		: N(n) {
		A = vector<int>(n, -1);
		B = vector<int>(n, -1);
		C = vector<vector<int>>(n, vector<int>(n, -1));
		SA = vector<int>(n, -1);
		SB = vector<int>(n, -1);
	}
};

int optimal_cost(const int& i, const int& j, Data& D) {
	if (i == 0) {
		D.C[0][j] = D.A[0] * D.SB[j];
		return D.C[0][j];
	}
	if (j == 0) {
		D.C[i][0] = D.SA[i] * D.B[0];
		return D.C[i][0];
	}

	int optimal = D.SA[i] * D.SB[j];
	for (int a = 0; a <= i-1; a++) { // Take one element from B
		int cost = (D.SA[i] - D.SA[a]) * D.B[j];
		if (D.C[a][j-1] < 0) optimal_cost(a, j-1, D);
		optimal = min(optimal, cost + D.C[a][j-1]);
	}
	for (int b = 0; b <= j-1; b++) { // Take one element from A
		int cost = D.A[i] * (D.SB[j] - D.SB[b]);
		if (D.C[i-1][b] < 0) optimal_cost(i-1, b, D);
		optimal = min(optimal, cost + D.C[i-1][b]);
	}

	D.C[i][j] = optimal;
	return optimal;
}

void testcase() {

	// Read parcels and compute partial sums
	int n;
	cin >> n;
	Data D(n);

	int sa = 0, sb = 0;
	for (int i = 0; i < n; i++) {
		cin >> D.A[i];
		D.A[i]--;
		sa += D.A[i];
		D.SA[i] = sa;
	}
	for (int i = 0; i < n; i++) {
		cin >> D.B[i];
		D.B[i]--;
		sb += D.B[i];
		D.SB[i] = sb;
	}

	// Write result
	cout << optimal_cost(n-1, n-1, D) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		Light Pattern			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	19.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

using namespace std;

void testcase() {

	// Read problem data
	unsigned int n, k, x;
	cin >> n >> k >> x;
	vector<int> P(k, 0);
	
	for (int p = 0; p < k; p++)
		P[k-p-1] = (x >> p) & 1;

	vector<int> prev_c(2, 0);

	// Compute optimal cost
	for (int b = 0; b < n; b++) {
		vector<int> next_c(2, 0);
		int pid = b % k;
		int state;
		cin >> state;

		// Here we have the possibility ALL the bulbs until b - 1
		if (pid == 0) {
			next_c[0] = min(prev_c[0], prev_c[1] + 1);
			next_c[1] = min(prev_c[1], prev_c[0] + 1);
			prev_c = next_c;
		}

		// Cost is the lights are not switched
		if (state == P[pid])
			next_c[0] = prev_c[0];
		else
			next_c[0] = prev_c[0] + 1;

		// Cost is the lights are switched
		if (state != P[pid])
			next_c[1] = prev_c[1];
		else
			next_c[1] = prev_c[1] + 1;

		prev_c = next_c;
	}

	// Write result
	cout << min(prev_c[0], prev_c[1] + 1) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		Poker Chips				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	19.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Table {

private:
	vector<int> T;
	vector<int> s;
	int dim;
	int zero;
public:
	Table(const vector<int>& size)
		: s(size), zero(0) {
		int n = 1;
		dim = size.size();
		for (int d = 0; d < dim; d++)
			n *= size[d];
		T = vector<int>(n, 0);
	}

	const int& size(const int& dim) {
		return s[dim - 1];
	}

	int& operator[](const vector<int>& idx) {
		int index = 0;
		int dim_mult = 1;
		for (int d = dim - 1; d >= 0; d--) {
			if (idx[d] < 0) return zero;
			index += dim_mult * idx[d];
			dim_mult *= s[d];
		}
		return T[index];
	}
};

int removal_score(vector<int>& fishes) {

	if (fishes.size() == 0) return 0;
	sort(fishes.begin(), fishes.end());
	int current_fish = fishes[0];
	int most_fish = fishes[0];
	int current_count = 0;
	int most_count = 0;

	for (auto f : fishes) {
		if (f == current_fish)
			current_count++;
			if (current_count > most_count) {
				most_fish = current_fish;
				most_count = current_count;
			}
			else {
				if (current_count > most_count) {
					most_fish = current_fish;
					most_count = current_count;
				}
				current_fish = f;
				current_count = 1;
			}
	}

	if (most_count > 1) return (int)pow(2.0, (double)(most_count - 2));
	else return 0;
}

void process_dimension(Table& T, const vector<vector<int>>& directions, vector<vector<int>>& stacks, const int& dim, vector<int>& fish_id) {

	if (dim > 0) {
		for (int i = 0; i < T.size(dim); i++) {
			fish_id[dim - 1] = i;
			process_dimension(T, directions, stacks, dim - 1, fish_id);
		}
	}
	else {
		int max_score = 0;
		for (int d = 0; d < directions.size(); d++) {

			// Get previous score
			vector<int> prev_idx(fish_id);
			for (int i = 0; i < prev_idx.size(); i++) prev_idx[i] -= directions[d][i];
			int score = T[prev_idx];

			// Compute removal score
			vector<int> fishes;
			for (int i = 0; i < stacks.size(); i++) {
				if (directions[d][i] == 1) fishes.push_back(stacks[i][fish_id[i]]);
			}
			score += removal_score(fishes);
			max_score = max(max_score, score);
		}
		T[fish_id] = max_score;
	}
}

void testcase() {

	// Read problem data
	unsigned int n;
	cin >> n;
	vector<vector<int>> stacks;
	vector<int> sizes(n, 0);
	for (int s = 0; s < n; s++) {
		cin >> sizes[s];
		sizes[s]++;
		stacks.push_back(vector<int>(sizes[s], 0));
	}

	for (int s = 0; s < n; s++) {
		stacks[s][0] = -s;
		for (int i = 1; i < stacks[s].size(); i++)
			cin >> stacks[s][i];
	}

	// Build removal directions
	vector<vector<int>> directions((int)pow(2.0, (double)n), vector<int>(n, 0));
	for (int d = 0; d < directions.size(); d++) {
		for (int i = 0; i < n; i++)
			directions[d][i] = (d >> i) & 1;
	}

	// Setup table and compute optimal sequence
	Table T(sizes);
	vector<int> fish_id(n, 0);
	process_dimension(T, directions, stacks, n, fish_id);

	// Write result
	cout << T[fish_id] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		Problem of the week - Tracking						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.01.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						property<edge_weight_t, int>> 	Graph;
typedef graph_traits<Graph>::vertex_descriptor 			Vertex;
typedef graph_traits<Graph>::edge_descriptor 			Edge;
typedef property_map<Graph, edge_weight_t>::type 		WeightMap;

void testcase() {
	
	// Read test settings
	int n, m, k, x, y;
	cin >> n >> m >> k >> x >> y;

	// Build empty graph
	Graph G(n * (k + 1));
	WeightMap W = get(edge_weight, G);	

	// Read roads and connect verices
	for (int i = 0; i < m; i++) {
		int a, b, c, d;
		cin >> a >> b >> c >> d;
		Edge e;		
		for (int j = 0; j < k + 1; j++) {
			tie(e, tuples::ignore) = add_edge(n*j + a, n*j + b, G);
			W[e] = c;
			if (d == 1) {
				if (j < k) {
					tie(e, tuples::ignore) = add_edge(n*j + a, n*(j+1) + b, G);
					W[e] = c;
					tie(e, tuples::ignore) = add_edge(n*(j+1) + a, n*j + b, G);
					W[e] = c;
				}	
			}
		}
	}

	// Run Dijkstra on the graph
	vector<Vertex> pred(n * (k + 1));	
	vector<int> dist(n * (k + 1));
	dijkstra_shortest_paths(G, x,
		predecessor_map(
			make_iterator_property_map(
				pred.begin(),
				get(vertex_index, G)
			)
		).
		distance_map(
			make_iterator_property_map(
				dist.begin(),
				get(vertex_index, G)
			)
		)
	);
	cout << dist[n * k + y] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 6						     					  //
//		Coin Tossing Tournament	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> 				Traits;
typedef adjacency_list<vecS, vecS, directedS,
	no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor>>>> 	Graph;
typedef	property_map<Graph, edge_capacity_t>::type					EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type			ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type					ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor						Vertex;
typedef	graph_traits<Graph>::edge_descriptor						Edge;
typedef graph_traits<Graph>::edge_iterator							EdgeIt;

// Custom EdgeAdder that encapsulates the references to the graph and its
// property maps and always creates reverse edges with corresponding capacities.
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {

	// Read players and rounds
	int n, m;
	cin >> n >> m;

	// Build empty graph
	Graph G(1 + m + n + 1);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);

	// Create nodes
	int source = 0;
	int sink = 1 + m + n;
	vector<int> rounds(m, 0);
	vector<int> players(n, 0);
	for (int i = 0; i < m; i++) rounds[i] = 1 + i;
	for (int i = 0; i < n; i++) players[i] = 1 + m + i;

	// Insert source -> games edges
	for (int i = 0; i < m; i++) ea.addEdge(source, rounds[i], 1);

	// Insert games -> players edges
	for (int i = 0; i < m; i++) {
		int a, b, c;
		cin >> a >> b >> c;
		if (c != 2) ea.addEdge(rounds[i], players[a], 1);
		if (c != 1) ea.addEdge(rounds[i], players[b], 1);
	}

	// Insert players -> sink edges
	int total_score = 0;
	for (int i = 0; i < n; i++) {
		int score;
		cin >> score;
		total_score += score;
		ea.addEdge(players[i], sink, score);
	}

	// Compute maximum flow
	int maximum_flow = push_relabel_max_flow(G, source, sink);

	// Write result
	if (maximum_flow == total_score && maximum_flow == m)
		cout << "yes\n";
	else
		cout << "no\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 6						     					  //
//		Shopping Trip			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> 				Traits;
typedef adjacency_list<vecS, vecS, directedS,
	no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor>>>> 	Graph;
typedef	property_map<Graph, edge_capacity_t>::type					EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type			ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type					ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor						Vertex;
typedef	graph_traits<Graph>::edge_descriptor						Edge;
typedef graph_traits<Graph>::edge_iterator							EdgeIt;

// Custom EdgeAdder that encapsulates the references to the graph and its
// property maps and always creates reverse edges with corresponding capacities.
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {

	// Read players and rounds
	int n, m, s;
	cin >> n >> m >> s;

	// Build empty graph
	Graph G(n + 1);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);

	// Create nodes
	int source = 0;
	int super_sink = n;
	vector<int> sinks(s, 0);
	for (int i = 0; i < s; i++) cin >> sinks[i];

	// Insert street edges
	for (int i = 0; i < m; i++) {
		int a, b;
		cin >> a >> b;
		ea.addEdge(a, b, 1);
		ea.addEdge(b, a, 1);
	}

	// Insert sink -> super sink edges
	for (int i = 0; i < s; i++) {
		ea.addEdge(sinks[i], super_sink, 1);
	}

	// Compute maximum flow
	int maximum_flow = push_relabel_max_flow(G, source, super_sink);

	// Write result
	if (maximum_flow == s)
		cout << "yes\n";
	else
		cout << "no\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 6						     					  //
//		Kingdom Defence			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	26.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> 				Traits;
typedef adjacency_list<vecS, vecS, directedS,
	no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor>>>> 	Graph;
typedef	property_map<Graph, edge_capacity_t>::type					EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type			ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type					ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor						Vertex;
typedef	graph_traits<Graph>::edge_descriptor						Edge;
typedef graph_traits<Graph>::edge_iterator							EdgeIt;

// Custom EdgeAdder that encapsulates the references to the graph and its
// property maps and always creates reverse edges with corresponding capacities.
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {

	// Read players and rounds
	int n, m;
	cin >> n >> m;

	// Build empty graph
	Graph G(n + 2);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);

	// Create nodes
	int source = n;
	int sink = n + 1;
	vector<int> nodes(n, 0);
	for (int i = 0; i < n; i++) {
		int g, d;
		cin >> g >> d;
		nodes[i] = g - d;
	}

	// Inser path edges
	for (int i = 0; i < m; i++) {
		int f, t, c, C;
		cin >> f >> t >> c >> C;
		if (f == t) continue;
		nodes[f] -= c;
		nodes[t] += c;
		ea.addEdge(f, t, C - c);

		// cout << "Edge " << f << "-" << t << " with capacity " << (C - c) << "\n";
	}

	// Insert source->city and city->sink edges
	int total_demand = 0;
	for (int i = 0; i < n; i++) {
		int balance = nodes[i];
		if (balance > 0) {
			ea.addEdge(source, i, balance);
			// cout << "Edge " << "source" << "-" << i << " with capacity " << balance << "\n";
		}
		if (balance < 0) {
			ea.addEdge(i, sink, -balance);
			total_demand += -balance;
			// cout << "Edge " << i << "-" << "sink" << " with capacity " << -balance << "\n";
		}
	}

	// Compute maximum flow
	int maximum_flow = push_relabel_max_flow(G, source, sink);
	// cout << "Max flow: " << maximum_flow << " with total demand: " << total_demand << "\n";

	// Write result
	if (maximum_flow == total_demand)
		cout << "yes\n";
	else
		cout << "no\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 6						     					  //
//		Problem of the week - The Great Game				   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	08.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

struct Position {
	int moves_for;		// Moves needed if you want to take this meeple to the end
	int moves_against;	// Moves needed if you don't want to take this meeple to the end
};

void testcase() {
	
	// Read testcase settings
	int n, m, r, b;
	cin >> n >> m >> r >> b;

	// Read transitions
	vector<vector<int>> t(n, vector<int>());
	vector<Position> p(n + 1);
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		t[u].push_back(v);
	}

	// Precompute winning possibilities
	int pos = n - 1;
	p[n] = { 0, 0 };
	while (pos >= min(r, b)) {
		int moves_for = INT_MAX;
		int moves_against = 0;
		for (int d : t[pos]) {
			moves_for = min(p[d].moves_against + 1, moves_for);
			moves_against = max(p[d].moves_for + 1, moves_against);
		}
		p[pos].moves_for = moves_for;
		p[pos].moves_against = moves_against;
		pos--;
	}

	// Output results
	if (p[r].moves_for < p[b].moves_for) cout << "0\n";
	else if (p[r].moves_for > p[b].moves_for) cout << "1\n";
	else if ((p[r].moves_for % 2) == 1 && p[r].moves_for == p[b].moves_for) cout << "0\n";
	else cout << "1\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Maximize it				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	26.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int p, a, b;
	std::cin >> p;
	
	Program p1(CGAL::SMALLER, true, 0, false, 0);
	Program p2(CGAL::LARGER, false, 0, false, 0);

	const int X = 0;
	const int Y = 1;
	const int Z = 2;

	p1.set_a(X, 0, 1);	p1.set_a(Y, 0, 1);	p1.set_b(0, 4);
	p1.set_a(X, 1, 4);	p1.set_a(Y, 1, 2);
	p1.set_a(X, 2, -1);	p1.set_a(Y, 2, 1);	p1.set_b(2, 1);
	
	p2.set_u(X, true, 0);	p2.set_u(Y, true, 0);
	p2.set_a(X, 0, 1);	p2.set_a(Y, 0, 1);	p2.set_a(Z, 0, 0);	p2.set_b(0, -4);
	p2.set_a(X, 1, 4);	p2.set_a(Y, 1, 2);	p2.set_a(Z, 1, 1);
	p2.set_a(X, 2, -1);	p2.set_a(Y, 2, 1);	p2.set_a(Z, 2, 0);	p2.set_b(2, -1);

	// Iterate over each test set	
	while (p != 0) {
		cin >> a >> b;

		// Set A entries
		p1.set_b(1, a * b);
		p2.set_b(1, -a * b);

		// Set objective function entries
		p1.set_d(X, X, 2 * a);
		p1.set_c(Y, -b);
		p2.set_d(X, X, 2 * a);
		p2.set_c(Y, b);	
		p2.set_d(Z, Z, 2);	

		// Solve problem
		Program* target;
		if (p == 1) target = &p1;
		else target = &p2;
		Solution s = CGAL::solve_quadratic_program(*target, ET());
		assert(s.solves_quadratic_program(*target));

		if (s.status() == CGAL::QP_INFEASIBLE)
			cout << "no\n";
		else if (s.status() == CGAL::QP_UNBOUNDED)
			cout << "unbounded\n";
		else {
			if (p == 1)
				cout << -std::ceil(CGAL::to_double(s.objective_value())) + 0.0<< "\n";
			else 
				cout << std::ceil(CGAL::to_double(s.objective_value())) + 0.0 << "\n";
		}
		std::cin >> p;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Diet					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	01.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n, m;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		cin >> m;

		// Create program
		Program p(CGAL::SMALLER, true, 0, false, 0);
	
		// Set entries in b (nutrient limits)
		for (int i = 0; i < n; i++) {
			int v, V;
			cin >> v >> V;
			p.set_b(i * 2 + 0, v); p.set_r(i * 2 + 0, CGAL::LARGER);
			p.set_b(i * 2 + 1, V); p.set_r(i * 2 + 1, CGAL::SMALLER);
		}

		// Set entries in A (nutrients total)
		for (int f = 0; f < m; f++) {

			// Read cost for the ingredient
			int c;
			cin >> c;
			p.set_c(f, c);

			// Read ingredient quantities
			for (int i = 0; i < n; i++) {
				int q;
				cin >> q;
				p.set_a(f, i * 2 + 0, q);
				p.set_a(f, i * 2 + 1, q);
			}
		}	

		// Solve problem
		Solution s = CGAL::solve_quadratic_program(p, ET());
		assert(s.solves_quadratic_program(p));

		if (s.status() == CGAL::QP_INFEASIBLE)
			cout << "No such diet.\n";
		else if (s.status() == CGAL::QP_UNBOUNDED)
			cout << "No such diet.\n";
		else
			cout << std::floor(CGAL::to_double(s.objective_value())) + 0.0 << "\n";
		std::cin >> n;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Portfolios				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	01.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n, m;
	std::cin >> n >> m;

	// Iterate over each test set	
	while (n != 0 && m != 0) {

		// Create program
		Program p(CGAL::SMALLER, true, 0, false, 0);
	
		// Set entries in A (costs and expected return)
		for (int i = 0; i < n; i++) {
			int c, r;
			cin >> c >> r;
			p.set_a(i, 0, c);
			p.set_a(i, 1, r);
		}

		// Set entries in D (variance)
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {	
				// Read and set variance
				int v;
				cin >> v;
				if (j <= i) p.set_d(i, j, 2 * v);
			}
		}	
		
		// Check portfolio for each investor
		for (int i = 0; i < m; i++) {

			// Set limits for the investor
			int C, R, V;
			cin >> C >> R >> V;
			p.set_b(0, C); p.set_r(0, CGAL::SMALLER);
			p.set_b(1, R); p.set_r(1, CGAL::LARGER);

			// Solve problem
			Solution s = CGAL::solve_quadratic_program(p, ET());
			assert(s.solves_quadratic_program(p));

			if (s.status() == CGAL::QP_INFEASIBLE)
				cout << "No.\n";
			else if (s.status() == CGAL::QP_UNBOUNDED)
				cout << "No.\n";
			else if (s.objective_value() > V)
				cout << "No.\n";
			else
				cout << "Yes.\n";
		}
		std::cin >> n >> m;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Inball					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	01.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n, d;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		cin >> d;

		// Create program
		Program p(CGAL::LARGER, false, 0, false, 0);
		p.set_l(d, true, 0);
	
		// Set entries in A (plane equations)
		for (int i = 0; i < n; i++) {
			// Set normal coefficients
			int squared_norm = 0;
			for (int dim = 0; dim < d; dim++) {
				int ai;
				cin >> ai;
				squared_norm += ai * ai;
				p.set_a(dim, i, ai);
			}

			// Set radius coefficient
			p.set_a(d, i, -sqrt(squared_norm));

			// Set offset
			int offset;
			cin >> offset;
			p.set_b(i, -offset);
		}

		// Set entries in c (maximum radius)
		p.set_c(d, -1);
		
		// Solve problem
		Solution s = CGAL::solve_quadratic_program(p, ET());
		assert(s.solves_quadratic_program(p));

		if (s.status() == CGAL::QP_INFEASIBLE)
			cout << "none\n";
		else if (s.status() == CGAL::QP_UNBOUNDED)
			cout << "inf\n";
		else
			cout << floor(-CGAL::to_double(s.objective_value())) + 0.0 << "\n";
		
		cin >> n;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Problem of the Week - The Phantom Menace			   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	07.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> 				Traits;
typedef adjacency_list<vecS, vecS, directedS,
	no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor>>>> 	Graph;
typedef	property_map<Graph, edge_capacity_t>::type					EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type			ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type					ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor						Vertex;
typedef	graph_traits<Graph>::edge_descriptor						Edge;
typedef graph_traits<Graph>::edge_iterator							EdgeIt;

// Custom EdgeAdder that encapsulates the references to the graph and its
// property maps and always creates reverse edges with corresponding capacities.
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {

	// Read testcase settings
	int n, m, s, d;
	cin >> n >> m >> s >> d;

	// Build empty graph
	Graph G(2 * n + 2);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);
	int source = 2 * n;
	int sink = 2 * n + 1;

	// Insert capacity edges
	for (int i = 0; i < n; i++)
		ea.addEdge(i, n + i, 1);

	// Insert link edges
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		ea.addEdge(n + u, v, 1);
	}

	// Insert source -> planets edges
	for (int i = 0; i < s; i++) {
		int si;
		cin >> si;
		ea.addEdge(source, si, 1);
	}

	// Insert planets -> sink edges
	for (int i = 0; i < d; i++) {
		int di;
		cin >> di;
		ea.addEdge(n + di, sink, 1);
	}

	// Compute maximum flow
	int maximum_flow = push_relabel_max_flow(G, source, sink);

	// Write result
	cout << maximum_flow << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Graypes					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	29.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;
typedef Triangulation::Finite_edges_iterator  				Edge_iterator;

double ceil_to_double(const K::FT& x) {
	double a = std::ceil(CGAL::to_double(x));
	while (a < x) a += 1;
	while (a-1 >= x) a -= 1;
	return a;
} 

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}

		// Create triangulation
		Triangulation t;
		t.insert(points.begin(), points.end());

		// Iterate over all edges and find the shortest one
		K::FT min_dist = INFINITY;
		for (Edge_iterator it = t.finite_edges_begin(); it != t.finite_edges_end(); it++) {
			K::Segment_2 s = t.segment(it);
			min_dist = min(s.squared_length(), min_dist);
		}

		cout << ceil_to_double(sqrt(min_dist)*50) << "\n";

		cin >> n;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Bistro					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	29.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}

		// Read query points
		int m;
		cin >> m;

		// Create triangulation
		Triangulation t;
		t.insert(points.begin(), points.end());

		// Compute minimum distance of query point to the nearest existing point
		for (int i = 0; i < m; i++) {
			K::Point_2 p;
			cin >> p;
			cout << CGAL::to_double(CGAL::squared_distance(t.nearest_vertex(p)->point(), p)) << "\n";
		}

		cin >> n;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		H1N1					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	02.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <queue>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel 	K;
typedef CGAL::Triangulation_vertex_base_2<K> 					Vb;
typedef CGAL::Triangulation_face_base_with_info_2<K::FT, K> 	Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> 			Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds>					Triangulation;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}

		// Create triangulation
		Triangulation t;
		t.insert(points.begin(), points.end());

		// Precompute maximum escape radius
		// ==================================

		// Initialize maximum radius.
		for (Triangulation::All_faces_iterator f = t.all_faces_begin(); f < t.all_faces_end(); f++)
			f->info() = K::FT(0);

		// Run Dijkstra to find the maximum escape radius of each triangle
		queue<Triangulation::Face_handle> todo;
		Triangulation::Face_circulator fc = t.incident_faces(t.infinite_vertex());		
		do {
			todo.push(fc);
			fc->info() = INFINITY;
		} while (++fc != t.incident_faces(t.infinite_vertex()));

		while (!todo.empty()) {
			Triangulation::Face_handle sf = todo.front();
			todo.pop();

			for (int i = 0; i < 3; i++) {
				Triangulation::Face_handle ef = sf->neighbor(i);
				if (t.is_infinite(ef)) continue;				

				K::FT width = t.segment(Triangulation::Edge(sf, i)).squared_length();
				K::FT max_radius = min(width / 4, sf->info());

				if (max_radius > ef->info()) {
					ef->info() = max_radius;
					todo.push(ef);
				}
			}
		}

		// Perform queries
		// =================

		// Read query points
		int m;
		cin >> m;

		// Check if the current person can escape or not
		for (int i = 0; i < m; i++) {
			K::Point_2 p;
			K::FT d;
			cin >> p;
			cin >> d;
			if (t.locate(p)->info() >= d && CGAL::squared_distance(t.nearest_vertex(p)->point(), p) >= d)
				cout << "y";
			else
				cout << "n";
		}
		cout << "\n";
		cin >> n;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Germs					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	02.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel 	K;
typedef CGAL::Delaunay_triangulation_2<K>						Triangulation;
typedef Triangulation::Finite_vertices_iterator  				Vertex_iterator;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read dish borders
		int l, b, r, to;
		cin >> l >> b >> r >> to;

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}
		
		if (n == 1) {
			K::Point_2 p = points[0];
			K::FT radius = min(min(p.x()-l, r-p.x()), min(p.y()-b, to-p.y()));
			double d = ceil(sqrt(CGAL::to_double(radius)-0.5));
			cout << d << " " << d << " " << d << "\n";
		}
		else {
			// Create triangulation
			Triangulation t;
			t.insert(points.begin(), points.end());

			// Extract bacteria radiuses
			vector<double> death_radiuses;
			death_radiuses.reserve(n);

			for (Vertex_iterator it = t.finite_vertices_begin(); it != t.finite_vertices_end(); it++) {
				K::Point_2 p = it->point();
				K::FT death_radius_sq = min(min(p.x()-l, r-p.x()), min(p.y()-b, to-p.y()));
				death_radius_sq = death_radius_sq * death_radius_sq;
			
				Triangulation::Edge_circulator ec = t.incident_edges(it);		
				do {
					if (!t.is_infinite(ec))
						death_radius_sq = min(death_radius_sq, t.segment(ec).squared_length() / 4);
				} while (++ec != t.incident_edges(it));
	
				death_radiuses.push_back(CGAL::to_double(death_radius_sq));
			}

			// Sort radiuses
			sort(death_radiuses.begin(), death_radiuses.end());
			cout << ceil(sqrt(sqrt(death_radiuses.front()) - 0.5)) << " ";
			cout << ceil(sqrt(sqrt(death_radiuses[n/2]) - 0.5)) << " ";
			cout << ceil(sqrt(sqrt(death_radiuses.back()) - 0.5)) << "\n";
		}

		cin >> n;
	}
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Problem of the Week - Stamp Exhibition				   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	08.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpq.h>

using namespace std;

typedef CGAL::Gmpq                                          ET;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Quadratic_program<ET> 						Program;
typedef CGAL::Quadratic_program_solution<ET> 				Solution;

void testcase() {

	// Read testcase settings
	int l, s, w;
	cin >> l >> s >> w;

	// Read lights, stamps and walls
	vector<K::Point_2> L(l);
	vector<K::Point_2> S(s);
	vector<ET> M(s);
	vector<K::Segment_2> W(w);

	for (int i = 0; i < l; i++)	
		cin >> L[i];
	for (int i = 0; i < s; i++)
		cin >> S[i] >> M[i];
	for (int i = 0; i < w; i++)
		cin >> W[i];

	// Setup program
	Program p(CGAL::SMALLER, true, 1, true, 1<<12);
	for (int i = 0; i < s; i++) {
		for (int j = 0; j < l; j++) {
			bool blocked = false;
			for (int wall = 0; wall < w && !blocked; wall++)
				blocked = CGAL::do_intersect(W[wall], K::Segment_2(S[i], L[j]));
			double d = (blocked) ? 0.0 : 1.0 / CGAL::squared_distance(S[i], L[j]);
			p.set_a(j, 2 * i, ET(d));
			p.set_a(j, 2 * i + 1, ET(d));
		}
		p.set_b(2 * i, 1);			p.set_r(2 * i, CGAL::LARGER);
		p.set_b(2 * i + 1, M[i]);	p.set_r(2 * i + 1, CGAL::SMALLER);
	}

	// Solve LP and output solution
	Solution sol = CGAL::solve_linear_program(p, ET());
	if (sol.status() == CGAL::QP_INFEASIBLE)
		cout << "no\n";
	else if (sol.status() == CGAL::QP_UNBOUNDED)
		cout << "eh?\n";
	else
		cout << "yes\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Satellites				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	03.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef	adjacency_list_traits<vecS, vecS, directedS>					Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, int,
		property<edge_residual_capacity_t, int,
			property<edge_reverse_t, Traits::edge_descriptor> > > >		Graph;
typedef	graph_traits<Graph>::edge_descriptor							Edge;
typedef graph_traits<Graph>::vertex_descriptor							Vertex;
typedef	graph_traits<Graph>::out_edge_iterator							OutEdgeIt;
typedef property_map<Graph, edge_capacity_t>::type						EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type				ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type						ReverseEdgeMap;

// Custom Add Edge for flow problems
void addEdge(int from, int to, int c, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge, Graph &G) {
	Edge e, reverseE;
	tie(e, tuples::ignore)			= add_edge(from, to, G);
	tie(reverseE, tuples::ignore)	= add_edge(to, from, G);
	capacity[e]						= c;
	capacity[reverseE]				= 0;
	rev_edge[e]						= reverseE;
	rev_edge[reverseE]				= e;
}

void testcase() {

	// Read problem setting
	int g, s, l;
	cin >> g >> s >> l;

	// Create graph
	Graph G(g + s + 2);
	EdgeCapacityMap		capacity	= get(edge_capacity, G);
	ReverseEdgeMap		rev_edge 	= get(edge_reverse, G);
	ResidualCapacityMap	residue		= get(edge_residual_capacity, G);
	Vertex source = g + s;
	Vertex sink = g + s + 1;

	// Create source-ground links
	for (int i = 0; i < g; i++)
		addEdge(source, i, 1, capacity, rev_edge, G);

	// Create ground-satellite links
	for (int i = 0; i < l; i++) {
		int u, v;
		cin >> u >> v;
		addEdge(u, g + v, 1, capacity, rev_edge, G);
	}

	// Create satellite-sink links
	for (int i = 0; i < s; i++)
		addEdge(g + i, sink, 1, capacity, rev_edge, G);

	// Run max flow
	push_relabel_max_flow(G, source, sink);

	// BFS to find visited vertices S
	vector<int> visited(g + s, false);
	std::queue<int> Q;
	Q.push(source);

	while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (tie(ebeg, eend) = out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = target(*ebeg, G);
			if (residue[*ebeg] == 0 || visited[v]) continue;
			visited[v] = true;
			Q.push(v);
		}
	}
	
	// Collect minimum vertex cover vertices
	vector<int> gs, ss;
	for (int i = 0; i < g; i++)
		if (!visited[i]) gs.push_back(i);
	for (int i = g; i < g + s; i++)
		if (visited[i]) ss.push_back(i - g);

	// Output results
	cout << gs.size() << " " << ss.size() << "\n";
	bool first = true;	
	for (int i = 0; i < gs.size(); i++) {
		if (!first) cout << " ";
		cout << gs[i];
		first = false;
	}
	for (int i = 0; i < ss.size(); i++) {
		if (!first) cout << " ";
		cout << ss[i];
		first = false;
	}
	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Algocoön				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	03.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <climits>
#include <algorithm>
#include <vector>
#include <queue>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef	adjacency_list_traits<vecS, vecS, directedS>	Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, int,
		property<edge_residual_capacity_t, int,
			property<edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
typedef	graph_traits<Graph>::edge_descriptor			Edge;
typedef graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	graph_traits<Graph>::out_edge_iterator			OutEdgeIt;
typedef property_map<Graph, edge_capacity_t>::type		EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type		ReverseEdgeMap;

// Custom edge adder
void addEdge(int from, int to, int c, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge, Graph &G) {
	Edge e, reverseE;
	tie(e, tuples::ignore)		= add_edge(from, to, G);
	tie(reverseE, tuples::ignore)	= add_edge(to, from, G);
	capacity[e]		= c;
	capacity[reverseE]	= 0;
	rev_edge[e]		= reverseE;
	rev_edge[reverseE]	= e;
}

void testcase() {

	// Read problem setting
	int n, m;
	cin >> n >> m;

	// Create graph
	Graph G(n);
	EdgeCapacityMap		capacity 	= get(edge_capacity, G);
	ReverseEdgeMap		rev_edge 	= get(edge_reverse, G);
	ResidualCapacityMap	residue 	= get(edge_residual_capacity, G);
	
	// Create limb edges
	for (int i = 0; i < m; i++) {
		int u, v, c;
		cin >> u >> v >> c;
		addEdge(u, v, c, capacity, rev_edge, G);
	}

	// Run max flow on different source-sink pairs
	int min_cost = INT_MAX;
	int min_cost_source = -1;
	for (int i = 0; i < n; i++) {

		int source = i;
		int sink = (i + 1) % n;
    	int cost = push_relabel_max_flow(G, source, sink);
		
		if (cost < min_cost) {
			min_cost = cost;
			min_cost_source = source;
		}
	}

	// Run max flow again on the best pair to get the residual capacity
	push_relabel_max_flow(G, min_cost_source, (min_cost_source + 1) % n);

	// BFS to find the vertices belonging to the mincut
	int count = 1;
	vector<int> visited(n, false);
	visited[min_cost_source] = true;
	std::queue<int> Q;
	Q.push(min_cost_source);
	while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (tie(ebeg, eend) = out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = target(*ebeg, G);
			if (residue[*ebeg] == 0 || visited[v]) continue;
			visited[v] = true;
			Q.push(v);
			count++;
		}
	}

	// Print solution
	cout << min_cost << "\n";
	cout << count;
	for (int i = 0; i < n; i++)
		if (visited[i]) cout << " " << i;
	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Canteen					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	04.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace boost;
using namespace std;

// Graph typedefs
typedef adjacency_list_traits<vecS, vecS, directedS> 			Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > 			Graph;

typedef property_map<Graph, edge_capacity_t>::type      		EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type      			EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type 	ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       		ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          		Vertex;
typedef graph_traits<Graph>::edge_descriptor            		Edge;
typedef graph_traits<Graph>::out_edge_iterator  				OutEdgeIt;

// Custom edge adder
struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

void testcase() {

	// Read problem setting
	int n;
	cin >> n;

	// Create graph
    Graph G(n + 2);
    EdgeCapacityMap	capacity 		= get(edge_capacity, G);
    EdgeWeightMap 	weight 			= get(edge_weight, G);
    ReverseEdgeMap 	rev_edge 		= get(edge_reverse, G);
    ResCapacityMap 	res_capacity	= get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);

	Vertex source = n;
	Vertex sink = n + 1;
	int offset = 20;
	int to_serve = 0;

	// Create source-day edges
	for (int i = 0; i < n; i++) {
		int a, c;
		cin >> a >> c;
		ea.addEdge(source, i, a, c);
	}

	// Create day-sink edges
	for (int i = 0; i < n; i++) {
		int s, p;
		cin >> s >> p;
		to_serve += s;
		ea.addEdge(i, sink, s, offset - p);
	}

	// Create day-day edges
	for (int i = 0; i < n-1; i++) {
		int v, e;
		cin >> v >> e;
		ea.addEdge(i, i+1, v, e);
	}

	// Find Min Cost Max Flow
	successive_shortest_path_nonnegative_weights(G, source, sink);
	int served = 0;
	OutEdgeIt e, eend;
    for(tie(e, eend) = out_edges(vertex(source,G), G); e != eend; ++e)
        served += capacity[*e] - res_capacity[*e];
	int cost = find_flow_cost(G) - offset * served;

	// Print solution
	if (served >= to_serve)
		cout << "possible ";
	else
		cout << "impossible ";
	cout << served << " " << -cost << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Problem of the Week - Real Estate Market			   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	08.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace boost;
using namespace std;

typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > 		Graph;

typedef property_map<Graph, edge_capacity_t>::type      	EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       	EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       	ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

void testcase() {

	// Read testcase settings
	int n, m, s;
	cin >> n >> m >> s;
	int max_bid = 100;

	// Build graph
	Graph G(n + m + s + 2);
    EdgeCapacityMap capacity = get(edge_capacity, G);
    EdgeWeightMap weight = get(edge_weight, G);
    ReverseEdgeMap rev_edge = get(edge_reverse, G);
    ResCapacityMap res_capacity = get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);
	int buyers = 0;
	int sites = n;
	int states = n + m;
	int source = n + m + s;
	int sink = n + m + s + 1;

	// Insert state -> sink edges
	for (int i = 0; i < s; i++) {
		int li;
		cin >> li;
		ea.addEdge(states + i, sink, li, 0);
	}

	// Insert site -> state edges
	for (int i = 0; i < m; i++) {
		int si;
		cin >> si;
		ea.addEdge(sites + i, states + si - 1, 1, 0);
	}

	// Insert buyer -> site edges
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			int bi;
			cin >> bi;
			ea.addEdge(buyers + i, sites + j, 1, max_bid - bi);
		}
	}

	// Insert source -> buyers edges
	for (int i = 0; i < n; i++)
		ea.addEdge(source, buyers + i, 1, 0);

	// Compute maximum sites sold and profit
	successive_shortest_path_nonnegative_weights(G, source, sink);
	int sold = 0;
	OutEdgeIt e, eend;
    for(tie(e, eend) = out_edges(vertex(source,G), G); e != eend; ++e)
        sold += capacity[*e] - res_capacity[*e];
    int profit = sold * max_bid - find_flow_cost(G);

	// Write result
	cout << sold << " " << profit << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Return Of The Jedi		    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	09.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <stack>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list < vecS, vecS, undirectedS,
    no_property, property < edge_weight_t, int > > 	Graph;
typedef graph_traits < Graph >::edge_descriptor 	Edge;
typedef graph_traits < Graph >::vertex_descriptor 	Vertex;
typedef graph_traits<Graph>::out_edge_iterator		EdgeIt;
typedef property_map<Graph, edge_weight_t>::type 	WeightMap;
typedef pair<Vertex, int>							DfsEntry;

void testcase() {

	// Read problem setting
	int n, s, w;
	cin >> n >> s;

	// Create graph
	Graph G(n);
	WeightMap W = get(edge_weight, G);
	vector<vector<int>> WM(n, vector<int>(n, 0));

	// Read link costs
	for (int i = 0; i < n - 1; i++) {
		for (int j = i + 1; j < n; j++) {
			cin >> w;
			Edge e;
			tie(e, tuples::ignore) = add_edge(i, j, G);
			W[e] = w;
			WM[i][j] = w;
			WM[j][i] = w;
		}
	}

	// Run and construct MST
	vector<Vertex> p(n);
	prim_minimum_spanning_tree(G, &p[0], root_vertex(s - 1));
	Graph MST(n);
	WeightMap W_MST = get(edge_weight, MST);	
	int mst_cost = 0;
	for (int i = 0; i < n; i++) {
		if (p[i] != i) {
			Edge e, e_old;
			tie(e, tuples::ignore) = add_edge(i, p[i], MST);
			tie(e_old, tuples::ignore) = edge(i, p[i], G);
			W_MST[e] = W[e_old];
			mst_cost += W[e_old];
		}
	}

	// BFS to find edge with maximum weight along each path u - v
	vector<vector<int>> MW(n, vector<int>(n, 0));
	for (Vertex i = 0; i < n; i++) {
		vector<bool> visited(n, false);
		stack<DfsEntry> S;
		S.push(DfsEntry(i, 0));

		while (!S.empty()) {
			const DfsEntry u = S.top();
			S.pop();
			visited[u.first] = true;
			EdgeIt ebeg, eend;
			for (tie(ebeg, eend) = out_edges(u.first, MST); ebeg != eend; ++ebeg) {
				const int v = target(*ebeg, MST);
				if (visited[v]) continue;
				int cost = max(u.second, W_MST[*ebeg]);
				S.push(DfsEntry(v, cost));
				MW[i][v] = cost;
				MW[v][i] = cost;
			}
		}
	}

	// Find edge can be substituted with the minimum cost
	int min_cost = INT_MAX;
	for (int u = 0; u < n - 1; u++) {
		for (int v = u + 1; v < n; v++) {
			bool found;
			tie(tuples::ignore, found) = edge(u, v, MST);
			if (!found)
				min_cost = min(min_cost, WM[u][v] - MW[u][v]);
		}
	}

	cout << mst_cost + min_cost << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Light The Stage				   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;

void testcase() {

	// Read problem setting
	int m, n;
	cin >> m >> n;

	// Read participants
	vector<K::Point_2> P(m);
	vector<K::FT> R(m);
	for (int i = 0; i < m; i++)
		cin >> P[i] >> R[i];

	// Read height (= radius) of the lights
	K::FT h;
	cin >> h;

	// Read light positions
	vector<K::Point_2> L(n);
	for (int i = 0; i < n; i++)
		cin >> L[i];

	// Create DT for the lights
	Triangulation t;
	t.insert(L.begin(), L.end());

	// Search for absolute winners
	bool found = false;
	for (int i = 0; i < m; i++) {
		K::FT d = CGAL::squared_distance(t.nearest_vertex(P[i])->point(), P[i]);
		K::FT max_d = (h + R[i]) * (h + R[i]);
		if (d >= max_d) {
			cout << i << " ";
			found = true;
		}
	}
	
	// If there is no one left at the end of the game search for people that
	// remain in the game longer
	vector<int> winners;
	int winner_l = 0;
	if (!found) {
		for (int i = 0; i < m; i++) {
			for (int l = 0; l < n; l++) {		
				K::FT d = CGAL::squared_distance(L[l], P[i]);
				K::FT max_d = (h + R[i]) * (h + R[i]);
				if (d < max_d) {
					if (l > winner_l) {
						winners.clear();
						winner_l = l;
					}
					if (l == winner_l) winners.push_back(i);
					break;
				}
			}
		}
	}
	
	for (int i = 0; i < winners.size(); i++)
		cout << winners[i] << " ";
	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Burning Coins From Two Sides   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	09.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

void testcase() {

	// Read problem setting
	int n;
	cin >> n;

	// Create DP and coins tables
	vector<vector<int>> T(n, vector<int>(n, 0));
	vector<vector<int>> F(n, vector<int>(n, 0));
	vector<int> C(n, 0);

	// Read coins
	for (int i = 0; i < n; i++) {
		cin >> C[i];
		T[i][i] = C[i];
		F[i][i] = 0;
	}
	
	// Fill table
	for (int l = 1; l < n; l++) {
		for (int i = 0; i < n - l; i++) {

			// Find optimal move whn playing on coins [i - j]
			int j = i + l;
			T[i][j] = max(C[i] + F[i+1][j], F[i][j-1] + C[j]);
			F[i][j] = min(T[i+1][j], T[i][j-1]);
		}
	}

	// Print result
	cout << T[0][n-1] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}


//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Problem of the Week - The Empire Strikes Back		   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>
#include <climits>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpzf.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;

typedef CGAL::Gmpzf                                         ET;
typedef CGAL::Quadratic_program<ET> 						Program;
typedef CGAL::Quadratic_program_solution<ET> 				Solution;

void testcase() {

	// Read problem setting
	int a, s, b, e;
	cin >> a >> s >> b >> e;

	// Read particles
	vector<K::Point_2> P(a);
	vector<K::FT> D(a);
	for (int i = 0; i < a; i++)
		cin >> P[i] >> D[i];

	// Read shooting points
	vector<K::Point_2> S(s);
	for (int i = 0; i < s; i++)
		cin >> 	S[i];

	// Read bounty hunter positions
	vector<K::Point_2> B;
	B = vector<K::Point_2>(b);
	for (int i = 0; i < b; i++)
		cin >> B[i];

	// Create DT for the bounty hunters
	Triangulation t;
	t.insert(B.begin(), B.end());

	// Create LP for the problem
	Program p(CGAL::LARGER, true, 0, false, 0);
	for (int i = 0; i < s; i++) {
	
		// Find maximum radius for shot
		double r_max = (b > 0) ? CGAL::squared_distance(t.nearest_vertex(S[i])->point(), S[i]) : DBL_MAX;
		
		for (int j = 0; j < a; j++) {
			double di = CGAL::squared_distance(S[i], P[j]);
			ET a_ij = (di < r_max) ? ET(1.0 / max(1.0, di)) : ET(0);
			p.set_a(i, j, a_ij);
		}
	
		// Contribution to maximum energy
		p.set_a(i, a, ET(1));
	}
	
	// Set RH side
	for (int j = 0; j < a; j++)
		p.set_b(j, ET(D[j]));	
	p.set_b(a, ET(e));
	p.set_r(a, CGAL::SMALLER);

	// Solve LP and output solution
	Solution sol = CGAL::solve_linear_program(p, ET());
	if (sol.status() == CGAL::QP_INFEASIBLE)
		cout << "n\n";
	else
		cout << "y\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	// std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Clues					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	11.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <queue>

#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/bipartite.hpp>
#include <boost/graph/connected_components.hpp>

using namespace std;
using namespace boost;

// CGAL typedefs
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb>            Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds> 				Triangulation;
typedef Triangulation::Vertex_handle						VH;
typedef Triangulation::Vertex_circulator					VC;
typedef Triangulation::Finite_edges_iterator				EI;

// Graph typedefs
typedef adjacency_list<vecS, vecS, undirectedS,
    					no_property,
						no_property>	 			Graph;
typedef graph_traits<Graph>::edge_descriptor 		Edge;
typedef graph_traits<Graph>::vertex_descriptor	 	Vertex;
typedef graph_traits<Graph>::out_edge_iterator	 	EdgeIt;

void testcase() {

	// Read problem setting
	int n, m, r;
	cin >> n >> m >> r;
	K::FT r_sq = K::FT(r) * K::FT(r);

	// Read stations and create triangulation
	vector<pair<K::Point_2, int>> S(n);
	for (int i = 0; i < n; i++) {
		cin >> S[i].first;
		S[i].second = i;
	}
	
	Triangulation t;
	t.insert(S.begin(), S.end());

	// Build graph and insert edges for any pair of communicating clients
	Graph G(n);
	for (EI it = t.finite_edges_begin(); it != t.finite_edges_end(); it++) {
		VH v1 = it->first->vertex((it->second + 1) % 3);
		VH v2 = it->first->vertex((it->second + 2) % 3);
		if (CGAL::squared_distance(v1->point(), v2->point()) <= r_sq)
			add_edge(v1->info(), v2->info(), G);
	}
	bool interferences = !is_bipartite(G);

	// Check if we missed some interferences
	if (!interferences) {
		for (EI it = t.finite_edges_begin(); it != t.finite_edges_end() && !interferences; it++) {
			VH v1 = it->first->vertex((it->second + 1) % 3);
			VH v2 = it->first->vertex((it->second + 2) % 3);
			if (CGAL::squared_distance(v1->point(), v2->point()) <= r_sq) {
				VC c = t.incident_vertices(v1);
				do {
					if (c != v2 && !t.is_infinite(c)) {
						if (CGAL::squared_distance(v1->point(), c->point()) <= r_sq &&
							CGAL::squared_distance(v2->point(), c->point()) <= r_sq)
							interferences = true;
					}
				} while (++c != t.incident_vertices(v1) && !interferences);
				c = t.incident_vertices(v2);
				do {
					if (c != v1 && !t.is_infinite(c)) {
						if (CGAL::squared_distance(v1->point(), c->point()) <= r_sq &&
							CGAL::squared_distance(v2->point(), c->point()) <= r_sq)
							interferences = true;
					}
				} while (++c != t.incident_vertices(v2) && !interferences);
			}
		}
	}

	int ncc = 0;
	vector<int> cc(n);
	if (!interferences)
		ncc = connected_components(G, make_iterator_property_map(cc.begin(), get(vertex_index, G)));
		
	// Read and process clues
	for (int i = 0; i < m; i++) {
		K::Point_2 a, b;
		cin >> a >> b;
		
		if (interferences) {
			cout << "n";
		}
		else if (CGAL::squared_distance(a, b) <= r_sq) {
			cout << "y";
		}
		else {
			VH va = t.nearest_vertex(a);
			VH vb = t.nearest_vertex(b);
			if (CGAL::squared_distance(a, va->point()) > r_sq || CGAL::squared_distance(b, vb->point()) > r_sq)
				cout << "n";
			else if (cc[va->info()] == cc[vb->info()])
				cout << "y";
			else
				cout << "n";
		}
	}

	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Monkey Island				   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	11.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, directedS,
    					no_property,
						no_property>	 			Graph;
typedef graph_traits<Graph>::edge_descriptor 		Edge;
typedef graph_traits<Graph>::vertex_descriptor	 	Vertex;
typedef graph_traits<Graph>::edge_iterator		 	EdgeIt;

void testcase() {

	// Read problem setting
	int n, m;
	cin >> n >> m;

	// Create graph and build roads
	Graph G(n);
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		add_edge(u - 1, v - 1, G);
	}

	// Find strong connected components
	vector<int> scc(n);
	int nscc = strong_components(G, &scc[0]);

	// Find which components don't have incoming edges (and thus need a station)
	vector<bool> needs_station(nscc, true);
	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg) {
		Vertex u = source(*ebeg, G);
		Vertex v = target(*ebeg, G);
		if (scc[u] != scc[v]) 
			needs_station[scc[v]] = false;
	}

	// Find cheapest position for a station in each component
	vector<int> cheapest_station(nscc, INT_MAX);
	for (int i = 0; i < n; i++) {
		int c;
		cin >> c;
		cheapest_station[scc[i]] = min(cheapest_station[scc[i]], c);
	}

	// Sum costs
	int cost = 0;
	for (int i = 0; i < nscc; i++) {
		if (needs_station[i]) cost += cheapest_station[i];
	}

	cout << cost << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Portfolios Revisited		   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	12.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> 			Program;
typedef CGAL::Quadratic_program_solution<ET> 	Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n, m;
	std::cin >> n >> m;

	// Iterate over each test set	
	while (n != 0 && m != 0) {

		// Create program
		Program p(CGAL::SMALLER, true, 0, false, 0);
	
		// Set entries in A (costs and expected return)
		for (int i = 0; i < n; i++) {
			int c, r;
			cin >> c >> r;
			p.set_a(i, 0, c);
			p.set_a(i, 1, r);
		}

		// Set entries in D (variance)
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {	
				// Read and set variance
				int v;
				cin >> v;
				if (j <= i) p.set_d(i, j, 2 * v);
			}
		}	
		
		// Check portfolio for each investor
		for (int i = 0; i < m; i++) {

			// Set limits for the investor
			int C, R, V, a = -1, b = n * 1000000 + 1;
			cin >> C >> V;
			p.set_b(0, C); p.set_r(0, CGAL::SMALLER);

			// Perform binary search
			bool ok = false;
			do {
				R = a + (b - a) / 2;			
				p.set_b(1, R); p.set_r(1, CGAL::LARGER);

				// Solve problem
				Solution s = CGAL::solve_quadratic_program(p, ET());

				if (s.status() == CGAL::QP_INFEASIBLE || s.status() == CGAL::QP_UNBOUNDED || s.objective_value() > V) {
					b = R;
					ok = false;
				}
				else {
					a = R;
					ok = true;
				}
			} while (b - a > 1 || !ok);
			
			cout << R << "\n";
		}
		std::cin >> n >> m;
	}
}


//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Placing Knights			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	12.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/tuple/tuple.hpp>


using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						no_property> 						Graph;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

void testcase() {

	// Read chessboard configuation
	int n = 0, nc = 0;
	cin >> n;
	vector<vector<bool>> C(n, vector<bool>(n, false));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			int is_cell;			
			cin >> is_cell;
			if (is_cell == 1) 
				C[i][j] = true;
			else
				nc++;
		}
	}

	// Create moving direction
	vector<pair<int, int>> dirs = {
		{-1, -2},
		{-2, -1},
		{-2,  1},
    	{-1,  2}
	};

	// Create adjacency graph
	Graph G(n * n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			for (auto dir : dirs) {
				int u = i + dir.first;
				int v = j + dir.second;
				if (u >= 0 && v >= 0 && v < n && C[i][j] && C[u][v])
					add_edge(i * n + j, u * n + v, G);
			}
		}
	}

	// Compute min-cost max-flow
	std::vector<Vertex> mate(n * n);
	edmonds_maximum_cardinality_matching(G, &mate[0]);
	int size = matching_size(G, &mate[0]);
	cout << n * n - nc - size << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Bonus Level				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	12.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <boost/tuple/tuple.hpp>


using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > 		Graph;

typedef property_map<Graph, edge_capacity_t>::type      	EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       	EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       	ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

// 
struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

void testcase() {

	// Read problem setting
	int n;
	cin >> n;

	// Create flow graph
	Graph G(n * n * 2);
    EdgeCapacityMap capacity = get(edge_capacity, G);
    EdgeWeightMap weight = get(edge_weight, G);
    ReverseEdgeMap rev_edge = get(edge_reverse, G);
    ResCapacityMap res_capacity = get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);
	Vertex source = 0;
	Vertex sink = 2 * n * n -1;

	// Read vertices
	int base_score = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			int c, w;
			cin >> w;

			// Avoid duplicated score
			if ((i == 0 && j == 0) || (i == n-1 & j == n-1)) {
				base_score = base_score - w;
				c = 2;
			}
			else c = 1;
			
			// Create edges
			ea.addEdge(i * n + j, n * n + i * n + j, c, 100 - w);
			if (j > 0) ea.addEdge(n * n + i * n + (j - 1), i * n + j, 1, 0);
			if (i > 0) ea.addEdge(n * n + (i - 1) * n + j, i * n + j, 1, 0);
		}
	}

	// Compute min-cost max-flow
	successive_shortest_path_nonnegative_weights(G, source, sink);
	cout << base_score + 2 * (2 * n - 1) * 100 - find_flow_cost(G) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Odd Route				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	13.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, directedS,
						no_property,
						property<edge_weight_t, int>> 	Graph;
typedef graph_traits<Graph>::vertex_descriptor 			Vertex;
typedef graph_traits<Graph>::edge_descriptor 			Edge;
typedef property_map<Graph, edge_weight_t>::type 		WeightMap;

void testcase() {
	
	// Read test settings
	int n, m, s, t;
	cin >> n >> m >> s >> t;

	// Build empty graph
	Graph G(n * 4);
	WeightMap W = get(edge_weight, G);	
	int ee = 0, eo = n, oe = 2*n, oo = 3*n;

	// Read edges and connect the four state graphs
	for (int i = 0; i < m; i++) {
		int a, b, w;
		cin >> a >> b >> w;
		Edge e;		
		bool odd = ((w % 2) == 1);

		tie(e, tuples::ignore) = add_edge(ee + a, (odd ? oo : oe) + b, G);
		W[e] = w;
		tie(e, tuples::ignore) = add_edge(eo + a, (odd ? oe : oo) + b, G);
		W[e] = w;
		tie(e, tuples::ignore) = add_edge(oe + a, (odd ? eo : ee) + b, G);
		W[e] = w;
		tie(e, tuples::ignore) = add_edge(oo + a, (odd ? ee : eo) + b, G);
		W[e] = w;
	}

	// Run Dijkstra on the graph
	vector<Vertex> pred(n * 4);	
	vector<int> dist(n * 4);
	dijkstra_shortest_paths(G, ee + s,
		predecessor_map(
			make_iterator_property_map(
				pred.begin(),
				get(vertex_index, G)
			)
		).
		distance_map(
			make_iterator_property_map(
				dist.begin(),
				get(vertex_index, G)
			)
		)
	);
	if (pred[oo + t] == oo + t)
		cout << "no\n";
	else
		cout << dist[oo + t] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}


//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Radiation Therapy			   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	13.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

using namespace std;

typedef CGAL::Gmpz                                          ET;
typedef CGAL::Quadratic_program<ET> 						Program;
typedef CGAL::Quadratic_program_solution<ET> 				Solution;

struct Cell {
	int x, y, z;
};

bool test_degree(const int& d, const int& h, const int& t, const vector<vector<vector<ET>>>& F) {

	// CGAL options
	CGAL::Quadratic_program_options options;
	options.set_pricing_strategy(CGAL::QP_BLAND);

	Program p(CGAL::SMALLER, false, 0, false, 0);
	for (int c = 0; c < h + t; c++) {
		
		// Create polynomials
		int pi = 0;
		for (int i = 0; i <= d; ++i) {
			for (int j = 0; j <= d - i; ++j) {
				for (int k = 0; k <= d - i - j; ++k) {
					p.set_a(pi, c, F[0][c][i] * F[1][c][j] * F[2][c][k]);
					pi++;
				}
			}
		}

		// Set relation and result
		if (c < h) {
			p.set_r(c, CGAL::LARGER);
			p.set_b(c, ET(1));
		}
		else {
			p.set_r(c, CGAL::SMALLER);
			p.set_b(c, ET(-1));
		}
	}

	// Solve problem
	Solution s = CGAL::solve_linear_program(p, ET(), options);
	return !(s.status() == CGAL::QP_INFEASIBLE);
}

void testcase() {

	// Read problem setting
	int h, t;
	cin >> h >> t;

	// Read coordinates of the measured cells
	vector<Cell> C;
	for (int i = 0; i < h + t; i++) {
		int x, y, z;
		cin >> x >> y >> z;
		C.push_back({ x, y, z });
	}

	// Precompute factors
	vector<vector<vector<ET>>> F(3, vector<vector<ET>>(h + t, vector<ET>(31, ET(1))));
	for (int c = 0; c < h + t; c++) {	
		for (int i = 1; i < 31; i++) {
			F[0][c][i] = F[0][c][i-1] * C[c].x;
			F[1][c][i] = F[1][c][i-1] * C[c].y;
			F[2][c][i] = F[2][c][i-1] * C[c].z;
		}
	}	

	// Perform linear search
	int d, a = 0, b = 1;
	bool stop = false;

	// Test degree 0
	if (test_degree(0, h, t, F)) {
		cout << 0 << "\n"; 
		return;
	}

	// Fast exponentiation
	while (b < 30) {
		if (test_degree(b, h, t, F)) break;
		b *= 2;
	}

	// Test degree 30
	if (b > 16) {
		if (!test_degree(30, h, t, F)) {
			cout << "Impossible!\n";
			return;
		}
		b = 30;
	}

	// Binary search
	do {	
		d = a + (b - a) / 2;		
		// cout << a << " " << d << " " << b << "\n";
		if (test_degree(d, h, t, F)) {
			b = d;
		}
		else {
			a = d;
		}
	} while (b - a > 1);

	cout << b << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Divisor Distance			   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	23.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>
#include <stack>

using namespace std;

int gcd(int u, int v) {
    while ( v != 0) {
        int r = u % v;
        u = v;
        v = r;
    }
    return u;
}

int source(int u, int v, const vector<int>& S) {
	stack<int> su, sv;
	su.push(u);
	sv.push(v);
	while (u > 1) {
		u = S[u];
		su.push(u);	
	}
	while (v > 1) {
		v = S[v];
		sv.push(v);	
	}

	int s = 0;
	while(!su.empty() && !sv.empty()) {
		if (su.top() == sv.top()) {
			s = su.top();
			su.pop();
			sv.pop();
		}
		else break;
	}

	return s;
}

void testcase(const vector<int>& T, const vector<int>& S) {

	// Read problem setting
	int n, c;
	cin >> n >> c;

	// Process query vertices
	for (int i = 0; i < c; i++) {
		int u, v;
		cin >> u >> v;
		cout << T[u] + T[v] - 2*T[source(u, v, S)] << "\n"; 
	}
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int t;
	std::cin >> t;

	// Create DP table
	int n = 10000000;
	vector<int> T(n + 1, 1);
	vector<int> S(n + 1, 1);
	
	// Fill table
	T[1] = 0;
	S[1] = 0;
	for (int u = 2; u <= n / 2; u++) {
		for (int m = 2; m <= u / S[u]; m++) {
			int v = m * u;
			if (v > n) break;
			T[v] = T[u] + 1;
			S[v] = u;
		}
	}

	// Iterate over each test set	
	while (t--) testcase(T, S);
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Problem of the Week - Revenge of the Sith			   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <queue>

#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/connected_components.hpp>

using namespace std;
using namespace boost;

// CGAL typedefs
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb>            Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds> 				Triangulation;
typedef Triangulation::Vertex_handle						VH;
typedef Triangulation::Vertex_circulator					VC;
typedef Triangulation::Finite_edges_iterator				EI;

// Graph typedefs
typedef adjacency_list<vecS, vecS, undirectedS,
    					no_property,
						no_property>	 			Graph;
typedef graph_traits<Graph>::edge_descriptor 		Edge;
typedef graph_traits<Graph>::vertex_descriptor	 	Vertex;
typedef graph_traits<Graph>::edge_iterator		 	EdgeIt;

void testcase() {

	// Read problem setting
	int n, r;
	cin >> n >> r;
	K::FT r_sq = K::FT(r) * K::FT(r);

	// Read planets
	vector<pair<K::Point_2, int>> P(n);
	for (int i = 0; i < n; i++) {
		cin >> P[i].first;
		P[i].second = i;
	}

	// Perform binary search finding the maximal number of rebel planets reachable
	int d, a = 1, b = n / 2 + 1;
	do {	
		d = a + (b - a) / 2;
		bool alliance_exists = false;
		
		// Create triangulation
		Triangulation t;
		t.insert(P.begin() + d, P.end());

		// Build graph and insert edges for any pair of reachable planets
		Graph G(n);
		for (EI it = t.finite_edges_begin(); it != t.finite_edges_end(); it++) {
			VH v1 = it->first->vertex((it->second + 1) % 3);
			VH v2 = it->first->vertex((it->second + 2) % 3);
			if (CGAL::squared_distance(v1->point(), v2->point()) <= r_sq)
				add_edge(v1->info(), v2->info(), G);
		}

		// Find biggest component
		vector<int> ccs(n);
	    int nccs = connected_components(G, &ccs[0]);
		vector<int> ccss(nccs, 0);
		int max_size = 0;
		for (int i = 0; i < n; i++) {
			ccss[ccs[i]]++;
			max_size = max(max_size, ccss[ccs[i]]);
		}
		alliance_exists = max_size >= d;

		if (!alliance_exists) {
			b = d;
		}
		else {
			a = d;
		}
	} while (b - a > 1);

	cout << a << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

//============================================================================//
//									     									  //
//		Algorithms Lab - Week 13					     					  //
//		Radiation II			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <unordered_set>

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/range_search_delaunay_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel 	K;

struct FaceInfo {
	unordered_set<K::Point_2> C;
};

typedef CGAL::Triangulation_vertex_base_2<K>					Vb;
typedef CGAL::Triangulation_face_base_with_info_2<FaceInfo, K>	Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb>			Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds>					Triangulation;
typedef Triangulation::Vertex_handle							VH;
typedef Triangulation::Face_handle								FH;
typedef Triangulation::Finite_faces_iterator					FFI;

// Custom comparer for sorting points
struct Pcomp{
	K::Point_2 e1, e2;
	bool operator() (const K::Point_2& p1, const K::Point_2& p2) {
		K::Circle_2 c(e1, e2, p2);
		return (c.bounded_side(p1) == CGAL::ON_UNBOUNDED_SIDE);
	}
};

void testcase() {
	
	// Read test settings
	int m, n;
	cin >> m >> n;

	// Read healthy and cancer cells
	vector<K::Point_2> H(m), C(n);
	for (int i = 0; i < m; i++) cin >> H[i];
	for (int i = 0; i < n; i++) cin >> C[i];

	// Create triangulations
	Triangulation t;
	t.insert(H.begin(), H.end());

	// Find which circumcircles contain cancer cells
	for (int i = 0; i < n; i++) {
		vector<FH> faces;
		t.get_conflicts(C[i], back_inserter(faces), FH());
		for (auto f : faces) f->info().insert(C[i]);
	}

	// Iterate over all triangles
	for (FFI f = t.finite_faces_begin(); f != t.finite_faces_end(); f++) {

		// Iterate over edges of the face
		for (int e = 0; e < 3; e++) {
			
			// Sort points contained in the circumcircles
			FHH f2 = f->neighbor((e+2)%3);
			vector<K::Points_2> p_f;
			vector<K::Points_2> p_f2;
			Pcomp comp(f->vertex(e)->point(), f->vertex((e+1)%3)->point());

			set_difference(f->info().C.begin(), f->info().C.end(),
							f2->info().C.begin(), f2->info().C.end(),
							back_inserter(p_f), comp);
			set_difference(f2->info().C.begin(), f2->info().C.end(),
							f->info().C.begin(), f->info().C.end(),
							back_inserter(p_f2), comp);
			
			int n_points = f2->info().C.size();
			for () {
			}
		}
	}

	// Check how many cancer cells can be destroyed by matching the shoot to the circumcircles of the
	// triangles.
	for (FFI f = th.finite_faces_begin(); f != th.finite_faces_end(); f++) {
		max_cancer = max(max_cancer, f->info().max_cancer);
		// cout << "mc = " << max_cancer << "\n";
	}

	cout << max_cancer << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	// std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}


//============================================================================//
//									     									  //
//		Algorithms Lab - Week 13					     					  //
//		Carsharing				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list_traits<vecS, vecS, directedS> 		Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;

typedef property_map<Graph, edge_capacity_t>::type      	EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       	EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       	ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

// Custom edge adder
struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

struct Request {
	int s, t, d, a, p;
};

void testcase() {
	
	// Read test settings
	int N, S;
	cin >> N >> S;

	// Read car availabilities and requests 
	// and create mappings from timestamps to station nodes
	int source = 0;
	int sink = 1;
	int next_node = 2;	
	vector<int> L(S);
	vector<Request> R(N);
	vector<map<int, int>> nodes(S);
	vector<int> timestamps = {0, 100000};

	for (int i = 0; i < S; i++) {
		nodes[i].emplace(0, next_node++);
		nodes[i].emplace(100000, next_node++);
	}

	for (int i = 0; i < S; i++) cin >> L[i];
	for (int i = 0; i < N; i++) {
		int s, t, d, a, p;
		cin >> s >> t >> d >> a >> p;
		s--; t--;
		R[i] = { s, t, d, a, p };

		if (nodes[s].emplace(d, next_node).second) {
			next_node++;
			timestamps.push_back(d);
		}
		if (nodes[t].emplace(a, next_node).second) {
			next_node++;
			timestamps.push_back(a);
		}
	}

	// Compute base costs for each timestamp
	sort(timestamps.begin(), timestamps.end());
	map<int, int> costs;
	int prev_ts = -1;
	int prev_cost = 0;
	for (int i = 0; i < timestamps.size(); i++) {
		if (timestamps[i] == prev_ts) continue;
		prev_ts = timestamps[i];
		int new_cost = prev_cost + 100;
		costs[timestamps[i]] = new_cost;
		prev_cost = new_cost;
	}

	// Create graph
	Graph G(next_node);
    EdgeCapacityMap capacity = get(edge_capacity, G);
    EdgeWeightMap weight = get(edge_weight, G);
    ReverseEdgeMap rev_edge = get(edge_reverse, G);
    ResCapacityMap res_capacity = get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);

	// Create source - stations nodes
	for (int i = 0; i < S; i++) {
		ea.addEdge(source, nodes[i][0], L[i], 0);
		ea.addEdge(nodes[i][100000], sink, INT_MAX, 0);
	}

	// Create timestamp - timestamp nodes
	for (int i = 0; i < S; i++) {
		for (auto it = nodes[i].begin(); it != nodes[i].end(); it++) {
			auto nit = next(it, 1);
			if (nit != nodes[i].end()) {
				ea.addEdge(it->second, nit->second, INT_MAX, costs[nit->first] - costs[it->first]);
			}
		}
	}

	// Create request edges
	for (int i = 0; i < N; i++)
		ea.addEdge(nodes[R[i].s][R[i].d], nodes[R[i].t][R[i].a], 1, costs[R[i].a] - costs[R[i].d] - R[i].p);

	// Compute min cost max flow
    successive_shortest_path_nonnegative_weights(G, source, sink);
    int cost = find_flow_cost(G);
    int flow = 0;
    
	// Iterate over all edges leaving the source
    OutEdgeIt e, eend;
    for(tie(e, eend) = out_edges(vertex(source,G), G); e != eend; ++e) {
        flow += capacity[*e] - res_capacity[*e];
    }

	// Compute and output final cost
	int revenue = flow * (costs[100000] - costs[0]) - cost;
	cout << revenue << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}


//============================================================================//
//									     									  //
//		Algorithms Lab - Week 13					     					  //
//		Carsharing				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
typedef	property_map<Graph, edge_capacity_t>::type		EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type		ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	graph_traits<Graph>::edge_descriptor			Edge;
typedef graph_traits<Graph>::edge_iterator	EdgeIt;	// Iterator

// Custom edge adder
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {
	
	// Read test settings
	int w, n;
	cin >> w >> n;
	
	// Create graph
	Graph G(2 * w);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);
	int source = 0;
	int sink = 2 * w - 1;

	// Read tetris bricks and insert them in the graph
	for (int i = 0; i < n; i++) {
		int u, v;
		cin >> u >> v;
		if (u > v) swap(u, v);

		// Insert edge if it doesn't exist
		bool found;
		tie(tuples::ignore, found) = edge(u, v + w - 1, G);
		if (!found || (u == 0 && v == w)) ea.addEdge(u, v + w - 1, 1);
	}

	// Insert caoacity edges
	for (int i = 1; i < w; i++) ea.addEdge(w + i - 1, i, 1);

	// Compute maximum flow and output result
	int flow = push_relabel_max_flow(G, source, sink);
	cout << flow << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}


