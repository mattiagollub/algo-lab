#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/bistro_sample.in > test/result.out
diff test/bistro_sample.out test/result.out 
echo "Test 1 competed!"

./main < test/bistro_test.in > test/result.out
diff test/bistro_test.out test/result.out
echo "Test 2 competed!"
