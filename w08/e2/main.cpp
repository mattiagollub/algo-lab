//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Bistro					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	29.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}

		// Read query points
		int m;
		cin >> m;

		// Create triangulation
		Triangulation t;
		t.insert(points.begin(), points.end());

		// Compute minimum distance of query point to the nearest existing point
		for (int i = 0; i < m; i++) {
			K::Point_2 p;
			cin >> p;
			cout << CGAL::to_double(CGAL::squared_distance(t.nearest_vertex(p)->point(), p)) << "\n";
		}

		cin >> n;
	}
}
