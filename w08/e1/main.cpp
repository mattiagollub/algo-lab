//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Graypes					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	29.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;
typedef Triangulation::Finite_edges_iterator  				Edge_iterator;

double ceil_to_double(const K::FT& x) {
	double a = std::ceil(CGAL::to_double(x));
	while (a < x) a += 1;
	while (a-1 >= x) a -= 1;
	return a;
} 

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}

		// Create triangulation
		Triangulation t;
		t.insert(points.begin(), points.end());

		// Iterate over all edges and find the shortest one
		K::FT min_dist = INFINITY;
		for (Edge_iterator it = t.finite_edges_begin(); it != t.finite_edges_end(); it++) {
			K::Segment_2 s = t.segment(it);
			min_dist = min(s.squared_length(), min_dist);
		}

		cout << ceil_to_double(sqrt(min_dist)*50) << "\n";

		cin >> n;
	}
}
