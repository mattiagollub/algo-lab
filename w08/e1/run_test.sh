#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/graypes_sample.in > test/result.out
diff test/graypes_sample.out test/result.out 
echo "Test 1 competed!"

./main < test/graypes_test.in > test/result.out
diff test/graypes_test.out test/result.out
echo "Test 2 competed!"
