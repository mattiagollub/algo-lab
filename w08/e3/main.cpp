//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		H1N1					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	02.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <queue>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel 	K;
typedef CGAL::Triangulation_vertex_base_2<K> 					Vb;
typedef CGAL::Triangulation_face_base_with_info_2<K::FT, K> 	Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> 			Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds>					Triangulation;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}

		// Create triangulation
		Triangulation t;
		t.insert(points.begin(), points.end());

		// Precompute maximum escape radius
		// ==================================

		// Initialize maximum radius.
		for (Triangulation::All_faces_iterator f = t.all_faces_begin(); f < t.all_faces_end(); f++)
			f->info() = K::FT(0);

		// Run Dijkstra to find the maximum escape radius of each triangle
		queue<Triangulation::Face_handle> todo;
		Triangulation::Face_circulator fc = t.incident_faces(t.infinite_vertex());		
		do {
			todo.push(fc);
			fc->info() = INFINITY;
		} while (++fc != t.incident_faces(t.infinite_vertex()));

		while (!todo.empty()) {
			Triangulation::Face_handle sf = todo.front();
			todo.pop();

			for (int i = 0; i < 3; i++) {
				Triangulation::Face_handle ef = sf->neighbor(i);
				if (t.is_infinite(ef)) continue;				

				K::FT width = t.segment(Triangulation::Edge(sf, i)).squared_length();
				K::FT max_radius = min(width / 4, sf->info());

				if (max_radius > ef->info()) {
					ef->info() = max_radius;
					todo.push(ef);
				}
			}
		}

		// Perform queries
		// =================

		// Read query points
		int m;
		cin >> m;

		// Check if the current person can escape or not
		for (int i = 0; i < m; i++) {
			K::Point_2 p;
			K::FT d;
			cin >> p;
			cin >> d;
			if (t.locate(p)->info() >= d && CGAL::squared_distance(t.nearest_vertex(p)->point(), p) >= d)
				cout << "y";
			else
				cout << "n";
		}
		cout << "\n";
		cin >> n;
	}
}
