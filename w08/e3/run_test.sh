#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/h1n1_sample.in > test/result.out
diff test/h1n1_sample.out test/result.out 
echo "Test 1 competed!"

./main < test/h1n1_test.in > test/result.out
diff test/h1n1_test.out test/result.out
echo "Test 2 competed!"
