//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Germs					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	02.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel 	K;
typedef CGAL::Delaunay_triangulation_2<K>						Triangulation;
typedef Triangulation::Finite_vertices_iterator  				Vertex_iterator;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n;
	cin >> n;

	// Iterate over each test set	
	while (n != 0) {

		// Read dish borders
		int l, b, r, to;
		cin >> l >> b >> r >> to;

		// Read points
		vector<K::Point_2> points;
		points.reserve(n);
		for (int i = 0; i < n; i++) {
			K::Point_2 p;
			cin >> p;
			points.push_back(p);
		}
		
		if (n == 1) {
			K::Point_2 p = points[0];
			K::FT radius = min(min(p.x()-l, r-p.x()), min(p.y()-b, to-p.y()));
			double d = ceil(sqrt(CGAL::to_double(radius)-0.5));
			cout << d << " " << d << " " << d << "\n";
		}
		else {
			// Create triangulation
			Triangulation t;
			t.insert(points.begin(), points.end());

			// Extract bacteria radiuses
			vector<double> death_radiuses;
			death_radiuses.reserve(n);

			for (Vertex_iterator it = t.finite_vertices_begin(); it != t.finite_vertices_end(); it++) {
				K::Point_2 p = it->point();
				K::FT death_radius_sq = min(min(p.x()-l, r-p.x()), min(p.y()-b, to-p.y()));
				death_radius_sq = death_radius_sq * death_radius_sq;
			
				Triangulation::Edge_circulator ec = t.incident_edges(it);		
				do {
					if (!t.is_infinite(ec))
						death_radius_sq = min(death_radius_sq, t.segment(ec).squared_length() / 4);
				} while (++ec != t.incident_edges(it));
	
				death_radiuses.push_back(CGAL::to_double(death_radius_sq));
			}

			// Sort radiuses
			sort(death_radiuses.begin(), death_radiuses.end());
			cout << ceil(sqrt(sqrt(death_radiuses.front()) - 0.5)) << " ";
			cout << ceil(sqrt(sqrt(death_radiuses[n/2]) - 0.5)) << " ";
			cout << ceil(sqrt(sqrt(death_radiuses.back()) - 0.5)) << "\n";
		}

		cin >> n;
	}
}
