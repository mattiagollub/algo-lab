//============================================================================//
//									     									  //
//		Algorithms Lab - Week 8						     					  //
//		Problem of the Week - Stamp Exhibition				   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	08.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpq.h>

using namespace std;

typedef CGAL::Gmpq                                          ET;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Quadratic_program<ET> 						Program;
typedef CGAL::Quadratic_program_solution<ET> 				Solution;

void testcase() {

	// Read testcase settings
	int l, s, w;
	cin >> l >> s >> w;

	// Read lights, stamps and walls
	vector<K::Point_2> L(l);
	vector<K::Point_2> S(s);
	vector<ET> M(s);
	vector<K::Segment_2> W(w);

	for (int i = 0; i < l; i++)	
		cin >> L[i];
	for (int i = 0; i < s; i++)
		cin >> S[i] >> M[i];
	for (int i = 0; i < w; i++)
		cin >> W[i];

	// Setup program
	Program p(CGAL::SMALLER, true, 1, true, 1<<12);
	for (int i = 0; i < s; i++) {
		for (int j = 0; j < l; j++) {
			bool blocked = false;
			for (int wall = 0; wall < w && !blocked; wall++)
				blocked = CGAL::do_intersect(W[wall], K::Segment_2(S[i], L[j]));
			double d = (blocked) ? 0.0 : 1.0 / CGAL::squared_distance(S[i], L[j]);
			p.set_a(j, 2 * i, ET(d));
			p.set_a(j, 2 * i + 1, ET(d));
		}
		p.set_b(2 * i, 1);			p.set_r(2 * i, CGAL::LARGER);
		p.set_b(2 * i + 1, M[i]);	p.set_r(2 * i + 1, CGAL::SMALLER);
	}

	// Solve LP and output solution
	Solution sol = CGAL::solve_linear_program(p, ET());
	if (sol.status() == CGAL::QP_INFEASIBLE)
		cout << "no\n";
	else if (sol.status() == CGAL::QP_UNBOUNDED)
		cout << "eh?\n";
	else
		cout << "yes\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}
