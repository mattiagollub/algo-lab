#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/stamps_sample.in > test/result.out
diff test/stamps_sample.out test/result.out 
echo "Test 1 completed!"

./main < test/stamps-test1.in > test/result.out
diff test/stamps-test1.out test/result.out 
echo "Test 2 completed!"

./main < test/stamps-test2.in > test/result.out
diff test/stamps-test2.out test/result.out 
echo "Test 3 completed!"
