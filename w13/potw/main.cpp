//============================================================================//
//									     									  //
//		Algorithms Lab - Week 13					     					  //
//		Carsharing				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
typedef	property_map<Graph, edge_capacity_t>::type		EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type		ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	graph_traits<Graph>::edge_descriptor			Edge;
typedef graph_traits<Graph>::edge_iterator	EdgeIt;	// Iterator

// Custom edge adder
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {
	
	// Read test settings
	int w, n;
	cin >> w >> n;
	
	// Create graph
	Graph G(2 * w);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);
	int source = 0;
	int sink = 2 * w - 1;

	// Read tetris bricks and insert them in the graph
	for (int i = 0; i < n; i++) {
		int u, v;
		cin >> u >> v;
		if (u > v) swap(u, v);

		// Insert edge if it doesn't exist
		bool found;
		tie(tuples::ignore, found) = edge(u, v + w - 1, G);
		if (!found || (u == 0 && v == w)) ea.addEdge(u, v + w - 1, 1);
	}

	// Insert caoacity edges
	for (int i = 1; i < w; i++) ea.addEdge(w + i - 1, i, 1);

	// Compute maximum flow and output result
	int flow = push_relabel_max_flow(G, source, sink);
	cout << flow << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

