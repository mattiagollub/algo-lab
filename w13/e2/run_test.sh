#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/radio2-sample.in > test/result.out
diff test/radio2-sample.out test/result.out 
echo "Test sample completed!"

./main < test/radio2-test1.in > test/result.out
diff test/radio2-test1.out test/result.out 
echo "Test 1 completed!"

./main < test/radio2-test2.in > test/result.out
diff test/radio2-test2.out test/result.out 
echo "Test 2 completed!"

./main < test/radio2-test3.in > test/result.out
diff test/radio2-test3.out test/result.out 
echo "Test 3 completed!"
