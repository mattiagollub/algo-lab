//============================================================================//
//									     									  //
//		Algorithms Lab - Week 13					     					  //
//		Radiation II			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <unordered_set>

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/range_search_delaunay_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel 	K;

struct FaceInfo {
	unordered_set<K::Point_2> C;
};

typedef CGAL::Triangulation_vertex_base_2<K>					Vb;
typedef CGAL::Triangulation_face_base_with_info_2<FaceInfo, K>	Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb>			Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds>					Triangulation;
typedef Triangulation::Vertex_handle							VH;
typedef Triangulation::Face_handle								FH;
typedef Triangulation::Finite_faces_iterator					FFI;

// Custom comparer for sorting points
struct Pcomp{
	K::Point_2 e1, e2;
	bool operator() (const K::Point_2& p1, const K::Point_2& p2) {
		K::Circle_2 c(e1, e2, p2);
		return (c.bounded_side(p1) == CGAL::ON_UNBOUNDED_SIDE);
	}
};

void testcase() {
	
	// Read test settings
	int m, n;
	cin >> m >> n;

	// Read healthy and cancer cells
	vector<K::Point_2> H(m), C(n);
	for (int i = 0; i < m; i++) cin >> H[i];
	for (int i = 0; i < n; i++) cin >> C[i];

	// Create triangulations
	Triangulation t;
	t.insert(H.begin(), H.end());

	// Find which circumcircles contain cancer cells
	for (int i = 0; i < n; i++) {
		vector<FH> faces;
		t.get_conflicts(C[i], back_inserter(faces), FH());
		for (auto f : faces) f->info().insert(C[i]);
	}

	// Iterate over all triangles
	for (FFI f = t.finite_faces_begin(); f != t.finite_faces_end(); f++) {

		// Iterate over edges of the face
		for (int e = 0; e < 3; e++) {
			
			// Sort points contained in the circumcircles
			FHH f2 = f->neighbor((e+2)%3);
			vector<K::Points_2> p_f;
			vector<K::Points_2> p_f2;
			Pcomp comp(f->vertex(e)->point(), f->vertex((e+1)%3)->point());

			set_difference(f->info().C.begin(), f->info().C.end(),
							f2->info().C.begin(), f2->info().C.end(),
							back_inserter(p_f), comp);
			set_difference(f2->info().C.begin(), f2->info().C.end(),
							f->info().C.begin(), f->info().C.end(),
							back_inserter(p_f2), comp);
			
			int n_points = f2->info().C.size();
			for () {
			}
		}
	}

	// Check how many cancer cells can be destroyed by matching the shoot to the circumcircles of the
	// triangles.
	for (FFI f = th.finite_faces_begin(); f != th.finite_faces_end(); f++) {
		max_cancer = max(max_cancer, f->info().max_cancer);
		// cout << "mc = " << max_cancer << "\n";
	}

	cout << max_cancer << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	// std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

