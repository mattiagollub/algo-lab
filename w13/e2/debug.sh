#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 main.cpp -o main

echo "$1" > gdb.txt
echo "r < test/sample.in" >> gdb.txt

gdb --command=gdb.txt  main

echo "Test 1 competed!"
