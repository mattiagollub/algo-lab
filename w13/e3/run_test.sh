#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 -O2 main.cpp -o main

./main < test/sample.in > test/result.out
diff test/sample.out test/result.out 
echo "Test sample completed!"

./main < test/test1.in > test/result.out
diff test/test1.out test/result.out 
echo "Test 1 completed!"

./main < test/test2.in > test/result.out
diff test/test2.out test/result.out 
echo "Test 2 completed!"

./main < test/test3.in > test/result.out
diff test/test3.out test/result.out 
echo "Test 3 completed!"

./main < test/test4.in > test/result.out
diff test/test4.out test/result.out 
echo "Test 4 completed!"

./main < test/test5.in > test/result.out
diff test/test5.out test/result.out 
echo "Test 5 completed!"
