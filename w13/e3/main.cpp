//============================================================================//
//									     									  //
//		Algorithms Lab - Week 13					     					  //
//		Carsharing				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	25.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list_traits<vecS, vecS, directedS> 		Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;

typedef property_map<Graph, edge_capacity_t>::type      	EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       	EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       	ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

// Custom edge adder
struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

struct Request {
	int s, t, d, a, p;
};

void testcase() {
	
	// Read test settings
	int N, S;
	cin >> N >> S;

	// Read car availabilities and requests 
	// and create mappings from timestamps to station nodes
	int source = 0;
	int sink = 1;
	int next_node = 2;	
	vector<int> L(S);
	vector<Request> R(N);
	vector<map<int, int>> nodes(S);
	vector<int> timestamps = {0, 100000};

	for (int i = 0; i < S; i++) {
		nodes[i].emplace(0, next_node++);
		nodes[i].emplace(100000, next_node++);
	}

	for (int i = 0; i < S; i++) cin >> L[i];
	for (int i = 0; i < N; i++) {
		int s, t, d, a, p;
		cin >> s >> t >> d >> a >> p;
		s--; t--;
		R[i] = { s, t, d, a, p };

		if (nodes[s].emplace(d, next_node).second) {
			next_node++;
			timestamps.push_back(d);
		}
		if (nodes[t].emplace(a, next_node).second) {
			next_node++;
			timestamps.push_back(a);
		}
	}

	// Compute base costs for each timestamp
	sort(timestamps.begin(), timestamps.end());
	map<int, int> costs;
	int prev_ts = -1;
	int prev_cost = 0;
	for (int i = 0; i < timestamps.size(); i++) {
		if (timestamps[i] == prev_ts) continue;
		prev_ts = timestamps[i];
		int new_cost = prev_cost + 100;
		costs[timestamps[i]] = new_cost;
		prev_cost = new_cost;
	}

	// Create graph
	Graph G(next_node);
    EdgeCapacityMap capacity = get(edge_capacity, G);
    EdgeWeightMap weight = get(edge_weight, G);
    ReverseEdgeMap rev_edge = get(edge_reverse, G);
    ResCapacityMap res_capacity = get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);

	// Create source - stations nodes
	for (int i = 0; i < S; i++) {
		ea.addEdge(source, nodes[i][0], L[i], 0);
		ea.addEdge(nodes[i][100000], sink, INT_MAX, 0);
	}

	// Create timestamp - timestamp nodes
	for (int i = 0; i < S; i++) {
		for (auto it = nodes[i].begin(); it != nodes[i].end(); it++) {
			auto nit = next(it, 1);
			if (nit != nodes[i].end()) {
				ea.addEdge(it->second, nit->second, INT_MAX, costs[nit->first] - costs[it->first]);
			}
		}
	}

	// Create request edges
	for (int i = 0; i < N; i++)
		ea.addEdge(nodes[R[i].s][R[i].d], nodes[R[i].t][R[i].a], 1, costs[R[i].a] - costs[R[i].d] - R[i].p);

	// Compute min cost max flow
    successive_shortest_path_nonnegative_weights(G, source, sink);
    int cost = find_flow_cost(G);
    int flow = 0;
    
	// Iterate over all edges leaving the source
    OutEdgeIt e, eend;
    for(tie(e, eend) = out_edges(vertex(source,G), G); e != eend; ++e) {
        flow += capacity[*e] - res_capacity[*e];
    }

	// Compute and output final cost
	int revenue = flow * (costs[100000] - costs[0]) - cost;
	cout << revenue << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

