#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 main.cpp -o main

./main < test/sample.in > test/result.out
diff test/sample.out test/result.out 
echo "Test sample completed!"

./main < test/bonus-level-sample1.in > test/result.out
diff test/bonus-level-sample1.out test/result.out 
echo "Test 1 completed!"

./main < test/bonus-level-sample1.in > test/result.out
diff test/bonus-level-sample1.out test/result.out 
echo "Test 2 completed!"

./main < test/bonus-level-sample1.in > test/result.out
diff test/bonus-level-sample1.out test/result.out 
echo "Test 3 completed!"
