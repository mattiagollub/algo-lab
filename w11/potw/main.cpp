//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Bonus Level				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	12.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <boost/tuple/tuple.hpp>


using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > 		Graph;

typedef property_map<Graph, edge_capacity_t>::type      	EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       	EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       	ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

// 
struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

void testcase() {

	// Read problem setting
	int n;
	cin >> n;

	// Create flow graph
	Graph G(n * n * 2);
    EdgeCapacityMap capacity = get(edge_capacity, G);
    EdgeWeightMap weight = get(edge_weight, G);
    ReverseEdgeMap rev_edge = get(edge_reverse, G);
    ResCapacityMap res_capacity = get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);
	Vertex source = 0;
	Vertex sink = 2 * n * n -1;

	// Read vertices
	int base_score = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			int c, w;
			cin >> w;

			// Avoid duplicated score
			if ((i == 0 && j == 0) || (i == n-1 & j == n-1)) {
				base_score = base_score - w;
				c = 2;
			}
			else c = 1;
			
			// Create edges
			ea.addEdge(i * n + j, n * n + i * n + j, c, 100 - w);
			if (j > 0) ea.addEdge(n * n + i * n + (j - 1), i * n + j, 1, 0);
			if (i > 0) ea.addEdge(n * n + (i - 1) * n + j, i * n + j, 1, 0);
		}
	}

	// Compute min-cost max-flow
	successive_shortest_path_nonnegative_weights(G, source, sink);
	cout << base_score + 2 * (2 * n - 1) * 100 - find_flow_cost(G) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
