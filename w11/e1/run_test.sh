#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/clues_sample.in > test/result.out
diff test/clues_sample.out test/result.out 
echo "Test sample completed!"

./main < test/clues_test1.in > test/result.out
diff test/clues_test1.out test/result.out 
echo "Test 1 completed!"

./main < test/clues_test2.in > test/result.out
diff test/clues_test2.out test/result.out 
echo "Test 2 completed!"

./main < test/clues_test3.in > test/result.out
diff test/clues_test3.out test/result.out 
echo "Test 3 completed!"

./main < test/clues_test4.in > test/result.out
diff test/clues_test4.out test/result.out 
echo "Test 4 completed!"
