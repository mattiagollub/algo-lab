//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Clues					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	11.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <queue>

#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/bipartite.hpp>
#include <boost/graph/connected_components.hpp>

using namespace std;
using namespace boost;

// CGAL typedefs
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb>            Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds> 				Triangulation;
typedef Triangulation::Vertex_handle						VH;
typedef Triangulation::Vertex_circulator					VC;
typedef Triangulation::Finite_edges_iterator				EI;

// Graph typedefs
typedef adjacency_list<vecS, vecS, undirectedS,
    					no_property,
						no_property>	 			Graph;
typedef graph_traits<Graph>::edge_descriptor 		Edge;
typedef graph_traits<Graph>::vertex_descriptor	 	Vertex;
typedef graph_traits<Graph>::out_edge_iterator	 	EdgeIt;

void testcase() {

	// Read problem setting
	int n, m, r;
	cin >> n >> m >> r;
	K::FT r_sq = K::FT(r) * K::FT(r);

	// Read stations and create triangulation
	vector<pair<K::Point_2, int>> S(n);
	for (int i = 0; i < n; i++) {
		cin >> S[i].first;
		S[i].second = i;
	}
	
	Triangulation t;
	t.insert(S.begin(), S.end());

	// Build graph and insert edges for any pair of communicating clients
	Graph G(n);
	for (EI it = t.finite_edges_begin(); it != t.finite_edges_end(); it++) {
		VH v1 = it->first->vertex((it->second + 1) % 3);
		VH v2 = it->first->vertex((it->second + 2) % 3);
		if (CGAL::squared_distance(v1->point(), v2->point()) <= r_sq)
			add_edge(v1->info(), v2->info(), G);
	}
	bool interferences = !is_bipartite(G);

	// Check if we missed some interferences
	if (!interferences) {
		for (EI it = t.finite_edges_begin(); it != t.finite_edges_end() && !interferences; it++) {
			VH v1 = it->first->vertex((it->second + 1) % 3);
			VH v2 = it->first->vertex((it->second + 2) % 3);
			if (CGAL::squared_distance(v1->point(), v2->point()) <= r_sq) {
				VC c = t.incident_vertices(v1);
				do {
					if (c != v2 && !t.is_infinite(c)) {
						if (CGAL::squared_distance(v1->point(), c->point()) <= r_sq &&
							CGAL::squared_distance(v2->point(), c->point()) <= r_sq)
							interferences = true;
					}
				} while (++c != t.incident_vertices(v1) && !interferences);
				c = t.incident_vertices(v2);
				do {
					if (c != v1 && !t.is_infinite(c)) {
						if (CGAL::squared_distance(v1->point(), c->point()) <= r_sq &&
							CGAL::squared_distance(v2->point(), c->point()) <= r_sq)
							interferences = true;
					}
				} while (++c != t.incident_vertices(v2) && !interferences);
			}
		}
	}

	int ncc = 0;
	vector<int> cc(n);
	if (!interferences)
		ncc = connected_components(G, make_iterator_property_map(cc.begin(), get(vertex_index, G)));
		
	// Read and process clues
	for (int i = 0; i < m; i++) {
		K::Point_2 a, b;
		cin >> a >> b;
		
		if (interferences) {
			cout << "n";
		}
		else if (CGAL::squared_distance(a, b) <= r_sq) {
			cout << "y";
		}
		else {
			VH va = t.nearest_vertex(a);
			VH vb = t.nearest_vertex(b);
			if (CGAL::squared_distance(a, va->point()) > r_sq || CGAL::squared_distance(b, vb->point()) > r_sq)
				cout << "n";
			else if (cc[va->info()] == cc[vb->info()])
				cout << "y";
			else
				cout << "n";
		}
	}

	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
