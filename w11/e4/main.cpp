//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Placing Knights			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	12.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/tuple/tuple.hpp>


using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						no_property> 						Graph;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

void testcase() {

	// Read chessboard configuation
	int n = 0, nc = 0;
	cin >> n;
	vector<vector<bool>> C(n, vector<bool>(n, false));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			int is_cell;			
			cin >> is_cell;
			if (is_cell == 1) 
				C[i][j] = true;
			else
				nc++;
		}
	}

	// Create moving direction
	vector<pair<int, int>> dirs = {
		{-1, -2},
		{-2, -1},
		{-2,  1},
    	{-1,  2}
	};

	// Create adjacency graph
	Graph G(n * n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			for (auto dir : dirs) {
				int u = i + dir.first;
				int v = j + dir.second;
				if (u >= 0 && v >= 0 && v < n && C[i][j] && C[u][v])
					add_edge(i * n + j, u * n + v, G);
			}
		}
	}

	// Compute min-cost max-flow
	std::vector<Vertex> mate(n * n);
	edmonds_maximum_cardinality_matching(G, &mate[0]);
	int size = matching_size(G, &mate[0]);
	cout << n * n - nc - size << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
