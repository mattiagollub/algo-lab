#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/portfolio_yield.ins > test/result.out
diff test/portfolio_yield.outs test/result.out 
echo "Test 1 competed!"

./main < test/portfolio_yield.int > test/result.out
diff test/portfolio_yield.outt test/result.out 
echo "Test 2 competed!"
