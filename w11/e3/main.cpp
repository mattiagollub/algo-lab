//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Portfolios Revisited		   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	12.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> 			Program;
typedef CGAL::Quadratic_program_solution<ET> 	Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n, m;
	std::cin >> n >> m;

	// Iterate over each test set	
	while (n != 0 && m != 0) {

		// Create program
		Program p(CGAL::SMALLER, true, 0, false, 0);
	
		// Set entries in A (costs and expected return)
		for (int i = 0; i < n; i++) {
			int c, r;
			cin >> c >> r;
			p.set_a(i, 0, c);
			p.set_a(i, 1, r);
		}

		// Set entries in D (variance)
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {	
				// Read and set variance
				int v;
				cin >> v;
				if (j <= i) p.set_d(i, j, 2 * v);
			}
		}	
		
		// Check portfolio for each investor
		for (int i = 0; i < m; i++) {

			// Set limits for the investor
			int C, R, V, a = -1, b = n * 1000000 + 1;
			cin >> C >> V;
			p.set_b(0, C); p.set_r(0, CGAL::SMALLER);

			// Perform binary search
			bool ok = false;
			do {
				R = a + (b - a) / 2;			
				p.set_b(1, R); p.set_r(1, CGAL::LARGER);

				// Solve problem
				Solution s = CGAL::solve_quadratic_program(p, ET());

				if (s.status() == CGAL::QP_INFEASIBLE || s.status() == CGAL::QP_UNBOUNDED || s.objective_value() > V) {
					b = R;
					ok = false;
				}
				else {
					a = R;
					ok = true;
				}
			} while (b - a > 1 || !ok);
			
			cout << R << "\n";
		}
		std::cin >> n >> m;
	}
}

