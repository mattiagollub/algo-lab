#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 main.cpp -o main

./main < test/sample.in > test/result.out
diff test/sample.out test/result.out 
echo "Test sample completed!"
