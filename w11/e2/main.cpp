//============================================================================//
//									     									  //
//		Algorithms Lab - Week 11					     					  //
//		Monkey Island				   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	11.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, directedS,
    					no_property,
						no_property>	 			Graph;
typedef graph_traits<Graph>::edge_descriptor 		Edge;
typedef graph_traits<Graph>::vertex_descriptor	 	Vertex;
typedef graph_traits<Graph>::edge_iterator		 	EdgeIt;

void testcase() {

	// Read problem setting
	int n, m;
	cin >> n >> m;

	// Create graph and build roads
	Graph G(n);
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		add_edge(u - 1, v - 1, G);
	}

	// Find strong connected components
	vector<int> scc(n);
	int nscc = strong_components(G, &scc[0]);

	// Find which components don't have incoming edges (and thus need a station)
	vector<bool> needs_station(nscc, true);
	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg) {
		Vertex u = source(*ebeg, G);
		Vertex v = target(*ebeg, G);
		if (scc[u] != scc[v]) 
			needs_station[scc[v]] = false;
	}

	// Find cheapest position for a station in each component
	vector<int> cheapest_station(nscc, INT_MAX);
	for (int i = 0; i < n; i++) {
		int c;
		cin >> c;
		cheapest_station[scc[i]] = min(cheapest_station[scc[i]], c);
	}

	// Sum costs
	int cost = 0;
	for (int i = 0; i < nscc; i++) {
		if (needs_station[i]) cost += cheapest_station[i];
	}

	cout << cost << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
