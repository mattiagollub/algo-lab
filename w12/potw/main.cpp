//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Problem of the Week - Revenge of the Sith			   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	24.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <queue>

#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/connected_components.hpp>

using namespace std;
using namespace boost;

// CGAL typedefs
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb>            Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds> 				Triangulation;
typedef Triangulation::Vertex_handle						VH;
typedef Triangulation::Vertex_circulator					VC;
typedef Triangulation::Finite_edges_iterator				EI;

// Graph typedefs
typedef adjacency_list<vecS, vecS, undirectedS,
    					no_property,
						no_property>	 			Graph;
typedef graph_traits<Graph>::edge_descriptor 		Edge;
typedef graph_traits<Graph>::vertex_descriptor	 	Vertex;
typedef graph_traits<Graph>::edge_iterator		 	EdgeIt;

void testcase() {

	// Read problem setting
	int n, r;
	cin >> n >> r;
	K::FT r_sq = K::FT(r) * K::FT(r);

	// Read planets
	vector<pair<K::Point_2, int>> P(n);
	for (int i = 0; i < n; i++) {
		cin >> P[i].first;
		P[i].second = i;
	}

	// Perform binary search finding the maximal number of rebel planets reachable
	int d, a = 1, b = n / 2 + 1;
	do {	
		d = a + (b - a) / 2;
		bool alliance_exists = false;
		
		// Create triangulation
		Triangulation t;
		t.insert(P.begin() + d, P.end());

		// Build graph and insert edges for any pair of reachable planets
		Graph G(n);
		for (EI it = t.finite_edges_begin(); it != t.finite_edges_end(); it++) {
			VH v1 = it->first->vertex((it->second + 1) % 3);
			VH v2 = it->first->vertex((it->second + 2) % 3);
			if (CGAL::squared_distance(v1->point(), v2->point()) <= r_sq)
				add_edge(v1->info(), v2->info(), G);
		}

		// Find biggest component
		vector<int> ccs(n);
	    int nccs = connected_components(G, &ccs[0]);
		vector<int> ccss(nccs, 0);
		int max_size = 0;
		for (int i = 0; i < n; i++) {
			ccss[ccs[i]]++;
			max_size = max(max_size, ccss[ccs[i]]);
		}
		alliance_exists = max_size >= d;

		if (!alliance_exists) {
			b = d;
		}
		else {
			a = d;
		}
	} while (b - a > 1);

	cout << a << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
