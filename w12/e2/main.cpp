//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Radiation Therapy			   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	13.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

using namespace std;

typedef CGAL::Gmpz                                          ET;
typedef CGAL::Quadratic_program<ET> 						Program;
typedef CGAL::Quadratic_program_solution<ET> 				Solution;

struct Cell {
	int x, y, z;
};

bool test_degree(const int& d, const int& h, const int& t, const vector<vector<vector<ET>>>& F) {

	// CGAL options
	CGAL::Quadratic_program_options options;
	options.set_pricing_strategy(CGAL::QP_BLAND);

	Program p(CGAL::SMALLER, false, 0, false, 0);
	for (int c = 0; c < h + t; c++) {
		
		// Create polynomials
		int pi = 0;
		for (int i = 0; i <= d; ++i) {
			for (int j = 0; j <= d - i; ++j) {
				for (int k = 0; k <= d - i - j; ++k) {
					p.set_a(pi, c, F[0][c][i] * F[1][c][j] * F[2][c][k]);
					pi++;
				}
			}
		}

		// Set relation and result
		if (c < h) {
			p.set_r(c, CGAL::LARGER);
			p.set_b(c, ET(1));
		}
		else {
			p.set_r(c, CGAL::SMALLER);
			p.set_b(c, ET(-1));
		}
	}

	// Solve problem
	Solution s = CGAL::solve_linear_program(p, ET(), options);
	return !(s.status() == CGAL::QP_INFEASIBLE);
}

void testcase() {

	// Read problem setting
	int h, t;
	cin >> h >> t;

	// Read coordinates of the measured cells
	vector<Cell> C;
	for (int i = 0; i < h + t; i++) {
		int x, y, z;
		cin >> x >> y >> z;
		C.push_back({ x, y, z });
	}

	// Precompute factors
	vector<vector<vector<ET>>> F(3, vector<vector<ET>>(h + t, vector<ET>(31, ET(1))));
	for (int c = 0; c < h + t; c++) {	
		for (int i = 1; i < 31; i++) {
			F[0][c][i] = F[0][c][i-1] * C[c].x;
			F[1][c][i] = F[1][c][i-1] * C[c].y;
			F[2][c][i] = F[2][c][i-1] * C[c].z;
		}
	}	

	// Perform linear search
	int d, a = 0, b = 1;
	bool stop = false;

	// Test degree 0
	if (test_degree(0, h, t, F)) {
		cout << 0 << "\n"; 
		return;
	}

	// Fast exponentiation
	while (b < 30) {
		if (test_degree(b, h, t, F)) break;
		b *= 2;
	}

	// Test degree 30
	if (b > 16) {
		if (!test_degree(30, h, t, F)) {
			cout << "Impossible!\n";
			return;
		}
		b = 30;
	}

	// Binary search
	do {	
		d = a + (b - a) / 2;		
		// cout << a << " " << d << " " << b << "\n";
		if (test_degree(d, h, t, F)) {
			b = d;
		}
		else {
			a = d;
		}
	} while (b - a > 1);

	cout << b << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
