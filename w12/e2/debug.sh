#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

echo "$1" > gdb.txt
echo "r < test/stamps_sample.in" >> gdb.txt

gdb --command=gdb.txt  main

echo "Test 1 competed!"
