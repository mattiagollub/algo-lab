#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/radiation_sample.in > test/result.out
diff test/radiation_sample.out test/result.out 
echo "Test sample completed!"

./main < test/radiation_test.in > test/result.out
diff test/radiation_test.out test/result.out 
echo "Test 1 completed!"
