//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Odd Route				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	13.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, directedS,
						no_property,
						property<edge_weight_t, int>> 	Graph;
typedef graph_traits<Graph>::vertex_descriptor 			Vertex;
typedef graph_traits<Graph>::edge_descriptor 			Edge;
typedef property_map<Graph, edge_weight_t>::type 		WeightMap;

void testcase() {
	
	// Read test settings
	int n, m, s, t;
	cin >> n >> m >> s >> t;

	// Build empty graph
	Graph G(n * 4);
	WeightMap W = get(edge_weight, G);	
	int ee = 0, eo = n, oe = 2*n, oo = 3*n;

	// Read edges and connect the four state graphs
	for (int i = 0; i < m; i++) {
		int a, b, w;
		cin >> a >> b >> w;
		Edge e;		
		bool odd = ((w % 2) == 1);

		tie(e, tuples::ignore) = add_edge(ee + a, (odd ? oo : oe) + b, G);
		W[e] = w;
		tie(e, tuples::ignore) = add_edge(eo + a, (odd ? oe : oo) + b, G);
		W[e] = w;
		tie(e, tuples::ignore) = add_edge(oe + a, (odd ? eo : ee) + b, G);
		W[e] = w;
		tie(e, tuples::ignore) = add_edge(oo + a, (odd ? ee : eo) + b, G);
		W[e] = w;
	}

	// Run Dijkstra on the graph
	vector<Vertex> pred(n * 4);	
	vector<int> dist(n * 4);
	dijkstra_shortest_paths(G, ee + s,
		predecessor_map(
			make_iterator_property_map(
				pred.begin(),
				get(vertex_index, G)
			)
		).
		distance_map(
			make_iterator_property_map(
				dist.begin(),
				get(vertex_index, G)
			)
		)
	);
	if (pred[oo + t] == oo + t)
		cout << "no\n";
	else
		cout << dist[oo + t] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

