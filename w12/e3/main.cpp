//============================================================================//
//									     									  //
//		Algorithms Lab - Week 12					     					  //
//		Divisor Distance			   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	23.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>
#include <stack>

using namespace std;

int gcd(int u, int v) {
    while ( v != 0) {
        int r = u % v;
        u = v;
        v = r;
    }
    return u;
}

int source(int u, int v, const vector<int>& S) {
	stack<int> su, sv;
	su.push(u);
	sv.push(v);
	while (u > 1) {
		u = S[u];
		su.push(u);	
	}
	while (v > 1) {
		v = S[v];
		sv.push(v);	
	}

	int s = 0;
	while(!su.empty() && !sv.empty()) {
		if (su.top() == sv.top()) {
			s = su.top();
			su.pop();
			sv.pop();
		}
		else break;
	}

	return s;
}

void testcase(const vector<int>& T, const vector<int>& S) {

	// Read problem setting
	int n, c;
	cin >> n >> c;

	// Process query vertices
	for (int i = 0; i < c; i++) {
		int u, v;
		cin >> u >> v;
		cout << T[u] + T[v] - 2*T[source(u, v, S)] << "\n"; 
	}
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int t;
	std::cin >> t;

	// Create DP table
	int n = 10000000;
	vector<int> T(n + 1, 1);
	vector<int> S(n + 1, 1);
	
	// Fill table
	T[1] = 0;
	S[1] = 0;
	for (int u = 2; u <= n / 2; u++) {
		for (int m = 2; m <= u / S[u]; m++) {
			int v = m * u;
			if (v > n) break;
			T[v] = T[u] + 1;
			S[v] = u;
		}
	}

	// Iterate over each test set	
	while (t--) testcase(T, S);
}
