//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Burning Coins From Two Sides   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	09.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

void testcase() {

	// Read problem setting
	int n;
	cin >> n;

	// Create DP and coins tables
	vector<vector<int>> T(n, vector<int>(n, 0));
	vector<vector<int>> F(n, vector<int>(n, 0));
	vector<int> C(n, 0);

	// Read coins
	for (int i = 0; i < n; i++) {
		cin >> C[i];
		T[i][i] = C[i];
		F[i][i] = 0;
	}
	
	// Fill table
	for (int l = 1; l < n; l++) {
		for (int i = 0; i < n - l; i++) {

			// Find optimal move whn playing on coins [i - j]
			int j = i + l;
			T[i][j] = max(C[i] + F[i+1][j], F[i][j-1] + C[j]);
			F[i][j] = min(T[i+1][j], T[i][j-1]);
		}
	}

	// Print result
	cout << T[0][n-1] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}

