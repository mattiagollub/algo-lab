#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/light-sample.in > test/result.out
diff test/light-sample.out test/result.out 
echo "Test sample completed!"

./main < test/light-test1.in > test/result.out
diff test/light-test1.out test/result.out 
echo "Test 1 completed!"

./main < test/light-test2.in > test/result.out
diff test/light-test2.out test/result.out 
echo "Test 2 completed!"
