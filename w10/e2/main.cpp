//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Light The Stage				   						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;

void testcase() {

	// Read problem setting
	int m, n;
	cin >> m >> n;

	// Read participants
	vector<K::Point_2> P(m);
	vector<K::FT> R(m);
	for (int i = 0; i < m; i++)
		cin >> P[i] >> R[i];

	// Read height (= radius) of the lights
	K::FT h;
	cin >> h;

	// Read light positions
	vector<K::Point_2> L(n);
	for (int i = 0; i < n; i++)
		cin >> L[i];

	// Create DT for the lights
	Triangulation t;
	t.insert(L.begin(), L.end());

	// Search for absolute winners
	bool found = false;
	for (int i = 0; i < m; i++) {
		K::FT d = CGAL::squared_distance(t.nearest_vertex(P[i])->point(), P[i]);
		K::FT max_d = (h + R[i]) * (h + R[i]);
		if (d >= max_d) {
			cout << i << " ";
			found = true;
		}
	}
	
	// If there is no one left at the end of the game search for people that
	// remain in the game longer
	vector<int> winners;
	int winner_l = 0;
	if (!found) {
		for (int i = 0; i < m; i++) {
			for (int l = 0; l < n; l++) {		
				K::FT d = CGAL::squared_distance(L[l], P[i]);
				K::FT max_d = (h + R[i]) * (h + R[i]);
				if (d < max_d) {
					if (l > winner_l) {
						winners.clear();
						winner_l = l;
					}
					if (l == winner_l) winners.push_back(i);
					break;
				}
			}
		}
	}
	
	for (int i = 0; i < winners.size(); i++)
		cout << winners[i] << " ";
	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
