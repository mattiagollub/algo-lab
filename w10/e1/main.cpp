//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Return Of The Jedi		    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	09.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <stack>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef adjacency_list < vecS, vecS, undirectedS,
    no_property, property < edge_weight_t, int > > 	Graph;
typedef graph_traits < Graph >::edge_descriptor 	Edge;
typedef graph_traits < Graph >::vertex_descriptor 	Vertex;
typedef graph_traits<Graph>::out_edge_iterator		EdgeIt;
typedef property_map<Graph, edge_weight_t>::type 	WeightMap;
typedef pair<Vertex, int>							DfsEntry;

void testcase() {

	// Read problem setting
	int n, s, w;
	cin >> n >> s;

	// Create graph
	Graph G(n);
	WeightMap W = get(edge_weight, G);
	vector<vector<int>> WM(n, vector<int>(n, 0));

	// Read link costs
	for (int i = 0; i < n - 1; i++) {
		for (int j = i + 1; j < n; j++) {
			cin >> w;
			Edge e;
			tie(e, tuples::ignore) = add_edge(i, j, G);
			W[e] = w;
			WM[i][j] = w;
			WM[j][i] = w;
		}
	}

	// Run and construct MST
	vector<Vertex> p(n);
	prim_minimum_spanning_tree(G, &p[0], root_vertex(s - 1));
	Graph MST(n);
	WeightMap W_MST = get(edge_weight, MST);	
	int mst_cost = 0;
	for (int i = 0; i < n; i++) {
		if (p[i] != i) {
			Edge e, e_old;
			tie(e, tuples::ignore) = add_edge(i, p[i], MST);
			tie(e_old, tuples::ignore) = edge(i, p[i], G);
			W_MST[e] = W[e_old];
			mst_cost += W[e_old];
		}
	}

	// BFS to find edge with maximum weight along each path u - v
	vector<vector<int>> MW(n, vector<int>(n, 0));
	for (Vertex i = 0; i < n; i++) {
		vector<bool> visited(n, false);
		stack<DfsEntry> S;
		S.push(DfsEntry(i, 0));

		while (!S.empty()) {
			const DfsEntry u = S.top();
			S.pop();
			visited[u.first] = true;
			EdgeIt ebeg, eend;
			for (tie(ebeg, eend) = out_edges(u.first, MST); ebeg != eend; ++ebeg) {
				const int v = target(*ebeg, MST);
				if (visited[v]) continue;
				int cost = max(u.second, W_MST[*ebeg]);
				S.push(DfsEntry(v, cost));
				MW[i][v] = cost;
				MW[v][i] = cost;
			}
		}
	}

	// Find edge can be substituted with the minimum cost
	int min_cost = INT_MAX;
	for (int u = 0; u < n - 1; u++) {
		for (int v = u + 1; v < n; v++) {
			bool found;
			tie(tuples::ignore, found) = edge(u, v, MST);
			if (!found)
				min_cost = min(min_cost, WM[u][v] - MW[u][v]);
		}
	}

	cout << mst_cost + min_cost << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
