#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/sample_test1.in > test/result.out
diff test/sample_test1.out test/result.out 
echo "Test 1 completed!"

./main < test/sample_test2.in > test/result.out
diff test/sample_test2.out test/result.out 
echo "Test 2 completed!"

./main < test/sample_test3.in > test/result.out
diff test/sample_test3.out test/result.out 
echo "Test 3 completed!"
