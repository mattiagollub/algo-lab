//============================================================================//
//									     									  //
//		Algorithms Lab - Week 10					     					  //
//		Problem of the Week - The Empire Strikes Back		   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>
#include <climits>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpzf.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  					Triangulation;

typedef CGAL::Gmpzf                                         ET;
typedef CGAL::Quadratic_program<ET> 						Program;
typedef CGAL::Quadratic_program_solution<ET> 				Solution;

void testcase() {

	// Read problem setting
	int a, s, b, e;
	cin >> a >> s >> b >> e;

	// Read particles
	vector<K::Point_2> P(a);
	vector<K::FT> D(a);
	for (int i = 0; i < a; i++)
		cin >> P[i] >> D[i];

	// Read shooting points
	vector<K::Point_2> S(s);
	for (int i = 0; i < s; i++)
		cin >> 	S[i];

	// Read bounty hunter positions
	vector<K::Point_2> B;
	B = vector<K::Point_2>(b);
	for (int i = 0; i < b; i++)
		cin >> B[i];

	// Create DT for the bounty hunters
	Triangulation t;
	t.insert(B.begin(), B.end());

	// Create LP for the problem
	Program p(CGAL::LARGER, true, 0, false, 0);
	for (int i = 0; i < s; i++) {
	
		// Find maximum radius for shot
		double r_max = (b > 0) ? CGAL::squared_distance(t.nearest_vertex(S[i])->point(), S[i]) : DBL_MAX;
		
		for (int j = 0; j < a; j++) {
			double di = CGAL::squared_distance(S[i], P[j]);
			ET a_ij = (di < r_max) ? ET(1.0 / max(1.0, di)) : ET(0);
			p.set_a(i, j, a_ij);
		}
	
		// Contribution to maximum energy
		p.set_a(i, a, ET(1));
	}
	
	// Set RH side
	for (int j = 0; j < a; j++)
		p.set_b(j, ET(D[j]));	
	p.set_b(a, ET(e));
	p.set_r(a, CGAL::SMALLER);

	// Solve LP and output solution
	Solution sol = CGAL::solve_linear_program(p, ET());
	if (sol.status() == CGAL::QP_INFEASIBLE)
		cout << "n\n";
	else
		cout << "y\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	// std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
