//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Inball					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	01.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n, d;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		cin >> d;

		// Create program
		Program p(CGAL::LARGER, false, 0, false, 0);
		p.set_l(d, true, 0);
	
		// Set entries in A (plane equations)
		for (int i = 0; i < n; i++) {
			// Set normal coefficients
			int squared_norm = 0;
			for (int dim = 0; dim < d; dim++) {
				int ai;
				cin >> ai;
				squared_norm += ai * ai;
				p.set_a(dim, i, ai);
			}

			// Set radius coefficient
			p.set_a(d, i, -sqrt(squared_norm));

			// Set offset
			int offset;
			cin >> offset;
			p.set_b(i, -offset);
		}

		// Set entries in c (maximum radius)
		p.set_c(d, -1);
		
		// Solve problem
		Solution s = CGAL::solve_quadratic_program(p, ET());
		assert(s.solves_quadratic_program(p));

		if (s.status() == CGAL::QP_INFEASIBLE)
			cout << "none\n";
		else if (s.status() == CGAL::QP_UNBOUNDED)
			cout << "inf\n";
		else
			cout << floor(-CGAL::to_double(s.objective_value())) + 0.0 << "\n";
		
		cin >> n;
	}
}
