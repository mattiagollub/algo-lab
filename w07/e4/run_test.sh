#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/inball.sin > test/result.out
diff test/inball.sout test/result.out 
echo "Test 1 competed!"

./main < test/inball.tin > test/result.out
diff test/inball.tout test/result.out
echo "Test 2 competed!"
