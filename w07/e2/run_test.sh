#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/diet.sin > test/result.out
diff test/diet.sout test/result.out 
echo "Test 1 competed!"

./main < test/diet.tin > test/result.out
diff test/diet.tout test/result.out
echo "Test 2 competed!"
