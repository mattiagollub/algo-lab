//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Diet					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	01.12.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int n, m;
	std::cin >> n;

	// Iterate over each test set	
	while (n != 0) {
		cin >> m;

		// Create program
		Program p(CGAL::SMALLER, true, 0, false, 0);
	
		// Set entries in b (nutrient limits)
		for (int i = 0; i < n; i++) {
			int v, V;
			cin >> v >> V;
			p.set_b(i * 2 + 0, v); p.set_r(i * 2 + 0, CGAL::LARGER);
			p.set_b(i * 2 + 1, V); p.set_r(i * 2 + 1, CGAL::SMALLER);
		}

		// Set entries in A (nutrients total)
		for (int f = 0; f < m; f++) {

			// Read cost for the ingredient
			int c;
			cin >> c;
			p.set_c(f, c);

			// Read ingredient quantities
			for (int i = 0; i < n; i++) {
				int q;
				cin >> q;
				p.set_a(f, i * 2 + 0, q);
				p.set_a(f, i * 2 + 1, q);
			}
		}	

		// Solve problem
		Solution s = CGAL::solve_quadratic_program(p, ET());
		assert(s.solves_quadratic_program(p));

		if (s.status() == CGAL::QP_INFEASIBLE)
			cout << "No such diet.\n";
		else if (s.status() == CGAL::QP_UNBOUNDED)
			cout << "No such diet.\n";
		else
			cout << std::floor(CGAL::to_double(s.objective_value())) + 0.0 << "\n";
		std::cin >> n;
	}
}
