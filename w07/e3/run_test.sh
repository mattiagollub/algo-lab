#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/portfolios.sin > test/result.out
diff test/portfolios.sout test/result.out 
echo "Test 1 competed!"

./main < test/portfolios.tin > test/result.out
diff test/portfolios.tout test/result.out
echo "Test 2 competed!"
