//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Problem of the Week - The Phantom Menace			   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	07.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> 				Traits;
typedef adjacency_list<vecS, vecS, directedS,
	no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor>>>> 	Graph;
typedef	property_map<Graph, edge_capacity_t>::type					EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type			ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type					ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor						Vertex;
typedef	graph_traits<Graph>::edge_descriptor						Edge;
typedef graph_traits<Graph>::edge_iterator							EdgeIt;

// Custom EdgeAdder that encapsulates the references to the graph and its
// property maps and always creates reverse edges with corresponding capacities.
struct EdgeAdder {
	EdgeAdder(Graph & G, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge) 
		: G(G), capacity(capacity), rev_edge(rev_edge) {}

	void addEdge(int u, int v, long c) {
		Edge e, reverseE;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		tie(reverseE, tuples::ignore) = add_edge(v, u, G);
		capacity[e] = c;
		capacity[reverseE] = 0;
		rev_edge[e] = reverseE;
		rev_edge[reverseE] = e;
	}
	Graph &G;
	EdgeCapacityMap	&capacity;
	ReverseEdgeMap	&rev_edge;
};

void testcase() {

	// Read testcase settings
	int n, m, s, d;
	cin >> n >> m >> s >> d;

	// Build empty graph
	Graph G(2 * n + 2);
	EdgeCapacityMap	capacity = get(edge_capacity, G);
	ReverseEdgeMap	rev_edge = get(edge_reverse, G);
	ResidualCapacityMap	res_capacity = get(edge_residual_capacity, G);
	EdgeAdder ea(G,capacity, rev_edge);
	int source = 2 * n;
	int sink = 2 * n + 1;

	// Insert capacity edges
	for (int i = 0; i < n; i++)
		ea.addEdge(i, n + i, 1);

	// Insert link edges
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		ea.addEdge(n + u, v, 1);
	}

	// Insert source -> planets edges
	for (int i = 0; i < s; i++) {
		int si;
		cin >> si;
		ea.addEdge(source, si, 1);
	}

	// Insert planets -> sink edges
	for (int i = 0; i < d; i++) {
		int di;
		cin >> di;
		ea.addEdge(n + di, sink, 1);
	}

	// Compute maximum flow
	int maximum_flow = push_relabel_max_flow(G, source, sink);

	// Write result
	cout << maximum_flow << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}
