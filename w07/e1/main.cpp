//============================================================================//
//									     									  //
//		Algorithms Lab - Week 7						     					  //
//		Maximize it				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	26.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#ifdef CGAL_USE_GMP
	#include <CGAL/Gmpz.h>
	typedef CGAL::Gmpz ET;
#else
	#include <CGAL/MP_Float.h>
	typedef CGAL::MP_Float ET;
#endif

using namespace std;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

	int p, a, b;
	std::cin >> p;
	
	Program p1(CGAL::SMALLER, true, 0, false, 0);
	Program p2(CGAL::LARGER, false, 0, false, 0);

	const int X = 0;
	const int Y = 1;
	const int Z = 2;

	p1.set_a(X, 0, 1);	p1.set_a(Y, 0, 1);	p1.set_b(0, 4);
	p1.set_a(X, 1, 4);	p1.set_a(Y, 1, 2);
	p1.set_a(X, 2, -1);	p1.set_a(Y, 2, 1);	p1.set_b(2, 1);
	
	p2.set_u(X, true, 0);	p2.set_u(Y, true, 0);
	p2.set_a(X, 0, 1);	p2.set_a(Y, 0, 1);	p2.set_a(Z, 0, 0);	p2.set_b(0, -4);
	p2.set_a(X, 1, 4);	p2.set_a(Y, 1, 2);	p2.set_a(Z, 1, 1);
	p2.set_a(X, 2, -1);	p2.set_a(Y, 2, 1);	p2.set_a(Z, 2, 0);	p2.set_b(2, -1);

	// Iterate over each test set	
	while (p != 0) {
		cin >> a >> b;

		// Set A entries
		p1.set_b(1, a * b);
		p2.set_b(1, -a * b);

		// Set objective function entries
		p1.set_d(X, X, 2 * a);
		p1.set_c(Y, -b);
		p2.set_d(X, X, 2 * a);
		p2.set_c(Y, b);	
		p2.set_d(Z, Z, 2);	

		// Solve problem
		Program* target;
		if (p == 1) target = &p1;
		else target = &p2;
		Solution s = CGAL::solve_quadratic_program(*target, ET());
		assert(s.solves_quadratic_program(*target));

		if (s.status() == CGAL::QP_INFEASIBLE)
			cout << "no\n";
		else if (s.status() == CGAL::QP_UNBOUNDED)
			cout << "unbounded\n";
		else {
			if (p == 1)
				cout << -std::ceil(CGAL::to_double(s.objective_value())) + 0.0<< "\n";
			else 
				cout << std::ceil(CGAL::to_double(s.objective_value())) + 0.0 << "\n";
		}
		std::cin >> p;
	}
}
