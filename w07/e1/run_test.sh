#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/maximizeit.sin > test/result.out
diff test/maximizeit.sout test/result.out 
echo "Test 1 competed!"

./main < test/maximizeit.tin > test/result.out
diff test/maximizeit.tout test/result.out
echo "Test 2 competed!"
