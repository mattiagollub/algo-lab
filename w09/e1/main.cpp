//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Satellites				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	03.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef	adjacency_list_traits<vecS, vecS, directedS>					Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, int,
		property<edge_residual_capacity_t, int,
			property<edge_reverse_t, Traits::edge_descriptor> > > >		Graph;
typedef	graph_traits<Graph>::edge_descriptor							Edge;
typedef graph_traits<Graph>::vertex_descriptor							Vertex;
typedef	graph_traits<Graph>::out_edge_iterator							OutEdgeIt;
typedef property_map<Graph, edge_capacity_t>::type						EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type				ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type						ReverseEdgeMap;

// Custom Add Edge for flow problems
void addEdge(int from, int to, int c, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge, Graph &G) {
	Edge e, reverseE;
	tie(e, tuples::ignore)			= add_edge(from, to, G);
	tie(reverseE, tuples::ignore)	= add_edge(to, from, G);
	capacity[e]						= c;
	capacity[reverseE]				= 0;
	rev_edge[e]						= reverseE;
	rev_edge[reverseE]				= e;
}

void testcase() {

	// Read problem setting
	int g, s, l;
	cin >> g >> s >> l;

	// Create graph
	Graph G(g + s + 2);
	EdgeCapacityMap		capacity	= get(edge_capacity, G);
	ReverseEdgeMap		rev_edge 	= get(edge_reverse, G);
	ResidualCapacityMap	residue		= get(edge_residual_capacity, G);
	Vertex source = g + s;
	Vertex sink = g + s + 1;

	// Create source-ground links
	for (int i = 0; i < g; i++)
		addEdge(source, i, 1, capacity, rev_edge, G);

	// Create ground-satellite links
	for (int i = 0; i < l; i++) {
		int u, v;
		cin >> u >> v;
		addEdge(u, g + v, 1, capacity, rev_edge, G);
	}

	// Create satellite-sink links
	for (int i = 0; i < s; i++)
		addEdge(g + i, sink, 1, capacity, rev_edge, G);

	// Run max flow
	push_relabel_max_flow(G, source, sink);

	// BFS to find visited vertices S
	vector<int> visited(g + s, false);
	std::queue<int> Q;
	Q.push(source);

	while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (tie(ebeg, eend) = out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = target(*ebeg, G);
			if (residue[*ebeg] == 0 || visited[v]) continue;
			visited[v] = true;
			Q.push(v);
		}
	}
	
	// Collect minimum vertex cover vertices
	vector<int> gs, ss;
	for (int i = 0; i < g; i++)
		if (!visited[i]) gs.push_back(i);
	for (int i = g; i < g + s; i++)
		if (visited[i]) ss.push_back(i - g);

	// Output results
	cout << gs.size() << " " << ss.size() << "\n";
	bool first = true;	
	for (int i = 0; i < gs.size(); i++) {
		if (!first) cout << " ";
		cout << gs[i];
		first = false;
	}
	for (int i = 0; i < ss.size(); i++) {
		if (!first) cout << " ";
		cout << ss[i];
		first = false;
	}
	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
