//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Problem of the Week - Real Estate Market			   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	08.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace boost;
using namespace std;

typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > 		Graph;

typedef property_map<Graph, edge_capacity_t>::type      	EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       	EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       	ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          	Vertex;
typedef graph_traits<Graph>::edge_descriptor            	Edge;
typedef graph_traits<Graph>::out_edge_iterator  			OutEdgeIt;

struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

void testcase() {

	// Read testcase settings
	int n, m, s;
	cin >> n >> m >> s;
	int max_bid = 100;

	// Build graph
	Graph G(n + m + s + 2);
    EdgeCapacityMap capacity = get(edge_capacity, G);
    EdgeWeightMap weight = get(edge_weight, G);
    ReverseEdgeMap rev_edge = get(edge_reverse, G);
    ResCapacityMap res_capacity = get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);
	int buyers = 0;
	int sites = n;
	int states = n + m;
	int source = n + m + s;
	int sink = n + m + s + 1;

	// Insert state -> sink edges
	for (int i = 0; i < s; i++) {
		int li;
		cin >> li;
		ea.addEdge(states + i, sink, li, 0);
	}

	// Insert site -> state edges
	for (int i = 0; i < m; i++) {
		int si;
		cin >> si;
		ea.addEdge(sites + i, states + si - 1, 1, 0);
	}

	// Insert buyer -> site edges
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			int bi;
			cin >> bi;
			ea.addEdge(buyers + i, sites + j, 1, max_bid - bi);
		}
	}

	// Insert source -> buyers edges
	for (int i = 0; i < n; i++)
		ea.addEdge(source, buyers + i, 1, 0);

	// Compute maximum sites sold and profit
	successive_shortest_path_nonnegative_weights(G, source, sink);
	int sold = 0;
	OutEdgeIt e, eend;
    for(tie(e, eend) = out_edges(vertex(source,G), G); e != eend; ++e)
        sold += capacity[*e] - res_capacity[*e];
    int profit = sold * max_bid - find_flow_cost(G);

	// Write result
	cout << sold << " " << profit << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();

}
