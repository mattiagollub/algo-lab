//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Algocoön				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	03.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>
#include <climits>
#include <algorithm>
#include <vector>
#include <queue>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

// Graph typedefs
typedef	adjacency_list_traits<vecS, vecS, directedS>	Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, int,
		property<edge_residual_capacity_t, int,
			property<edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
typedef	graph_traits<Graph>::edge_descriptor			Edge;
typedef graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	graph_traits<Graph>::out_edge_iterator			OutEdgeIt;
typedef property_map<Graph, edge_capacity_t>::type		EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type		ReverseEdgeMap;

// Custom edge adder
void addEdge(int from, int to, int c, EdgeCapacityMap &capacity, ReverseEdgeMap &rev_edge, Graph &G) {
	Edge e, reverseE;
	tie(e, tuples::ignore)		= add_edge(from, to, G);
	tie(reverseE, tuples::ignore)	= add_edge(to, from, G);
	capacity[e]		= c;
	capacity[reverseE]	= 0;
	rev_edge[e]		= reverseE;
	rev_edge[reverseE]	= e;
}

void testcase() {

	// Read problem setting
	int n, m;
	cin >> n >> m;

	// Create graph
	Graph G(n);
	EdgeCapacityMap		capacity 	= get(edge_capacity, G);
	ReverseEdgeMap		rev_edge 	= get(edge_reverse, G);
	ResidualCapacityMap	residue 	= get(edge_residual_capacity, G);
	
	// Create limb edges
	for (int i = 0; i < m; i++) {
		int u, v, c;
		cin >> u >> v >> c;
		addEdge(u, v, c, capacity, rev_edge, G);
	}

	// Run max flow on different source-sink pairs
	int min_cost = INT_MAX;
	int min_cost_source = -1;
	for (int i = 0; i < n; i++) {

		int source = i;
		int sink = (i + 1) % n;
    	int cost = push_relabel_max_flow(G, source, sink);
		
		if (cost < min_cost) {
			min_cost = cost;
			min_cost_source = source;
		}
	}

	// Run max flow again on the best pair to get the residual capacity
	push_relabel_max_flow(G, min_cost_source, (min_cost_source + 1) % n);

	// BFS to find the vertices belonging to the mincut
	int count = 1;
	vector<int> visited(n, false);
	visited[min_cost_source] = true;
	std::queue<int> Q;
	Q.push(min_cost_source);
	while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (tie(ebeg, eend) = out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = target(*ebeg, G);
			if (residue[*ebeg] == 0 || visited[v]) continue;
			visited[v] = true;
			Q.push(v);
			count++;
		}
	}

	// Print solution
	cout << min_cost << "\n";
	cout << count;
	for (int i = 0; i < n; i++)
		if (visited[i]) cout << " " << i;
	cout << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
