#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 main.cpp -o main

./main < test/test.in > test/result.out
diff test/test.out test/result.out 
echo "Test 1 competed!"

#./main < test/graypes_test.in > test/result.out
#diff test/graypes_test.out test/result.out
#echo "Test 2 competed!"
