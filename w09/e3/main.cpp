//============================================================================//
//									     									  //
//		Algorithms Lab - Week 9						     					  //
//		Canteen					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	04.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cstdlib>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace boost;
using namespace std;

// Graph typedefs
typedef adjacency_list_traits<vecS, vecS, directedS> 			Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > 			Graph;

typedef property_map<Graph, edge_capacity_t>::type      		EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type      			EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type 	ResCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       		ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          		Vertex;
typedef graph_traits<Graph>::edge_descriptor            		Edge;
typedef graph_traits<Graph>::out_edge_iterator  				OutEdgeIt;

// Custom edge adder
struct EdgeAdder {
    EdgeAdder(Graph & G, EdgeCapacityMap &capacity, EdgeWeightMap &weight, ReverseEdgeMap &rev_edge) 
        : G(G), capacity(capacity), weight(weight), rev_edge(rev_edge) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacity[e] = c;
        weight[e] = w;
        capacity[reverseE] = 0;
        weight[reverseE] = -w;
        rev_edge[e] = reverseE; 
        rev_edge[reverseE] = e; 
    }
    Graph &G;
    EdgeCapacityMap &capacity;
    EdgeWeightMap &weight;
    ReverseEdgeMap  &rev_edge;
};

void testcase() {

	// Read problem setting
	int n;
	cin >> n;

	// Create graph
    Graph G(n + 2);
    EdgeCapacityMap	capacity 		= get(edge_capacity, G);
    EdgeWeightMap 	weight 			= get(edge_weight, G);
    ReverseEdgeMap 	rev_edge 		= get(edge_reverse, G);
    ResCapacityMap 	res_capacity	= get(edge_residual_capacity, G);
    EdgeAdder ea(G, capacity, weight, rev_edge);

	Vertex source = n;
	Vertex sink = n + 1;
	int offset = 20;
	int to_serve = 0;

	// Create source-day edges
	for (int i = 0; i < n; i++) {
		int a, c;
		cin >> a >> c;
		ea.addEdge(source, i, a, c);
	}

	// Create day-sink edges
	for (int i = 0; i < n; i++) {
		int s, p;
		cin >> s >> p;
		to_serve += s;
		ea.addEdge(i, sink, s, offset - p);
	}

	// Create day-day edges
	for (int i = 0; i < n-1; i++) {
		int v, e;
		cin >> v >> e;
		ea.addEdge(i, i+1, v, e);
	}

	// Find Min Cost Max Flow
	successive_shortest_path_nonnegative_weights(G, source, sink);
	int served = 0;
	OutEdgeIt e, eend;
    for(tie(e, eend) = out_edges(vertex(source,G), G); e != eend; ++e)
        served += capacity[*e] - res_capacity[*e];
	int cost = find_flow_cost(G) - offset * served;

	// Print solution
	if (served >= to_serve)
		cout << "possible ";
	else
		cout << "impossible ";
	cout << served << " " << -cost << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
