#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -O2 -std=c++11 main.cpp -o main

./main < test/sample1.in > test/result.out
diff test/sample1.out test/result.out 

echo "Test 1 competed!"

./main < test/sample2.in > test/result.out
diff test/sample2.out test/result.out

echo "Test 2 competed!"
