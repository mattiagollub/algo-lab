//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		Poker Chips				    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	19.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Table {

private:
	vector<int> T;
	vector<int> s;
	int dim;
	int zero;
public:
	Table(const vector<int>& size)
		: s(size), zero(0) {
		int n = 1;
		dim = size.size();
		for (int d = 0; d < dim; d++)
			n *= size[d];
		T = vector<int>(n, 0);
	}

	const int& size(const int& dim) {
		return s[dim - 1];
	}

	int& operator[](const vector<int>& idx) {
		int index = 0;
		int dim_mult = 1;
		for (int d = dim - 1; d >= 0; d--) {
			if (idx[d] < 0) return zero;
			index += dim_mult * idx[d];
			dim_mult *= s[d];
		}
		return T[index];
	}
};

int removal_score(vector<int>& fishes) {

	if (fishes.size() == 0) return 0;
	sort(fishes.begin(), fishes.end());
	int current_fish = fishes[0];
	int most_fish = fishes[0];
	int current_count = 0;
	int most_count = 0;

	for (auto f : fishes) {
		if (f == current_fish)
			current_count++;
			if (current_count > most_count) {
				most_fish = current_fish;
				most_count = current_count;
			}
			else {
				if (current_count > most_count) {
					most_fish = current_fish;
					most_count = current_count;
				}
				current_fish = f;
				current_count = 1;
			}
	}

	if (most_count > 1) return (int)pow(2.0, (double)(most_count - 2));
	else return 0;
}

void process_dimension(Table& T, const vector<vector<int>>& directions, vector<vector<int>>& stacks, const int& dim, vector<int>& fish_id) {

	if (dim > 0) {
		for (int i = 0; i < T.size(dim); i++) {
			fish_id[dim - 1] = i;
			process_dimension(T, directions, stacks, dim - 1, fish_id);
		}
	}
	else {
		int max_score = 0;
		for (int d = 0; d < directions.size(); d++) {

			// Get previous score
			vector<int> prev_idx(fish_id);
			for (int i = 0; i < prev_idx.size(); i++) prev_idx[i] -= directions[d][i];
			int score = T[prev_idx];

			// Compute removal score
			vector<int> fishes;
			for (int i = 0; i < stacks.size(); i++) {
				if (directions[d][i] == 1) fishes.push_back(stacks[i][fish_id[i]]);
			}
			score += removal_score(fishes);
			max_score = max(max_score, score);
		}
		T[fish_id] = max_score;
	}
}

void testcase() {

	// Read problem data
	unsigned int n;
	cin >> n;
	vector<vector<int>> stacks;
	vector<int> sizes(n, 0);
	for (int s = 0; s < n; s++) {
		cin >> sizes[s];
		sizes[s]++;
		stacks.push_back(vector<int>(sizes[s], 0));
	}

	for (int s = 0; s < n; s++) {
		stacks[s][0] = -s;
		for (int i = 1; i < stacks[s].size(); i++)
			cin >> stacks[s][i];
	}

	// Build removal directions
	vector<vector<int>> directions((int)pow(2.0, (double)n), vector<int>(n, 0));
	for (int d = 0; d < directions.size(); d++) {
		for (int i = 0; i < n; i++)
			directions[d][i] = (d >> i) & 1;
	}

	// Setup table and compute optimal sequence
	Table T(sizes);
	vector<int> fish_id(n, 0);
	process_dimension(T, directions, stacks, n, fish_id);

	// Write result
	cout << T[fish_id] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
