//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		Problem of the week - Tracking						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	10.01.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						property<edge_weight_t, int>> 	Graph;
typedef graph_traits<Graph>::vertex_descriptor 			Vertex;
typedef graph_traits<Graph>::edge_descriptor 			Edge;
typedef property_map<Graph, edge_weight_t>::type 		WeightMap;

void testcase() {
	
	// Read test settings
	int n, m, k, x, y;
	cin >> n >> m >> k >> x >> y;

	// Build empty graph
	Graph G(n * (k + 1));
	WeightMap W = get(edge_weight, G);	

	// Read roads and connect verices
	for (int i = 0; i < m; i++) {
		int a, b, c, d;
		cin >> a >> b >> c >> d;
		Edge e;		
		for (int j = 0; j < k + 1; j++) {
			tie(e, tuples::ignore) = add_edge(n*j + a, n*j + b, G);
			W[e] = c;
			if (d == 1) {
				if (j < k) {
					tie(e, tuples::ignore) = add_edge(n*j + a, n*(j+1) + b, G);
					W[e] = c;
					tie(e, tuples::ignore) = add_edge(n*(j+1) + a, n*j + b, G);
					W[e] = c;
				}	
			}
		}
	}

	// Run Dijkstra on the graph
	vector<Vertex> pred(n * (k + 1));	
	vector<int> dist(n * (k + 1));
	dijkstra_shortest_paths(G, x,
		predecessor_map(
			make_iterator_property_map(
				pred.begin(),
				get(vertex_index, G)
			)
		).
		distance_map(
			make_iterator_property_map(
				dist.begin(),
				get(vertex_index, G)
			)
		)
	);
	cout << dist[n * k + y] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
