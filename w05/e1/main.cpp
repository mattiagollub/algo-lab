//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		DHL						    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	18.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

using namespace std;

struct Data {

public:
	int N;
	vector<int> A;
	vector<int> B;
	vector<vector<int>> C;
	vector<int> SA;
	vector<int> SB;

	Data(const int& n)
		: N(n) {
		A = vector<int>(n, -1);
		B = vector<int>(n, -1);
		C = vector<vector<int>>(n, vector<int>(n, -1));
		SA = vector<int>(n, -1);
		SB = vector<int>(n, -1);
	}
};

int optimal_cost(const int& i, const int& j, Data& D) {
	if (i == 0) {
		D.C[0][j] = D.A[0] * D.SB[j];
		return D.C[0][j];
	}
	if (j == 0) {
		D.C[i][0] = D.SA[i] * D.B[0];
		return D.C[i][0];
	}

	int optimal = D.SA[i] * D.SB[j];
	for (int a = 0; a <= i-1; a++) { // Take one element from B
		int cost = (D.SA[i] - D.SA[a]) * D.B[j];
		if (D.C[a][j-1] < 0) optimal_cost(a, j-1, D);
		optimal = min(optimal, cost + D.C[a][j-1]);
	}
	for (int b = 0; b <= j-1; b++) { // Take one element from A
		int cost = D.A[i] * (D.SB[j] - D.SB[b]);
		if (D.C[i-1][b] < 0) optimal_cost(i-1, b, D);
		optimal = min(optimal, cost + D.C[i-1][b]);
	}

	D.C[i][j] = optimal;
	return optimal;
}

void testcase() {

	// Read parcels and compute partial sums
	int n;
	cin >> n;
	Data D(n);

	int sa = 0, sb = 0;
	for (int i = 0; i < n; i++) {
		cin >> D.A[i];
		D.A[i]--;
		sa += D.A[i];
		D.SA[i] = sa;
	}
	for (int i = 0; i < n; i++) {
		cin >> D.B[i];
		D.B[i]--;
		sb += D.B[i];
		D.SB[i] = sb;
	}

	// Write result
	cout << optimal_cost(n-1, n-1, D) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
