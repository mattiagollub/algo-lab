//============================================================================//
//									     									  //
//		Algorithms Lab - Week 5						     					  //
//		Light Pattern			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	19.11.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

using namespace std;

void testcase() {

	// Read problem data
	unsigned int n, k, x;
	cin >> n >> k >> x;
	vector<int> P(k, 0);
	
	for (int p = 0; p < k; p++)
		P[k-p-1] = (x >> p) & 1;

	vector<int> prev_c(2, 0);

	// Compute optimal cost
	for (int b = 0; b < n; b++) {
		vector<int> next_c(2, 0);
		int pid = b % k;
		int state;
		cin >> state;

		// Here we have the possibility ALL the bulbs until b - 1
		if (pid == 0) {
			next_c[0] = min(prev_c[0], prev_c[1] + 1);
			next_c[1] = min(prev_c[1], prev_c[0] + 1);
			prev_c = next_c;
		}

		// Cost is the lights are not switched
		if (state == P[pid])
			next_c[0] = prev_c[0];
		else
			next_c[0] = prev_c[0] + 1;

		// Cost is the lights are switched
		if (state != P[pid])
			next_c[1] = prev_c[1];
		else
			next_c[1] = prev_c[1] + 1;

		prev_c = next_c;
	}

	// Write result
	cout << min(prev_c[0], prev_c[1] + 1) << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
