//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		First steps with BGL	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	11.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
			no_property,
			property<edge_weight_t, int> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef property_map<Graph, edge_weight_t>::type WeightMap;

void testcase() {

	// Read vertex and edge count	
	int V, E;
	cin >> V >> E;

	// Create graph
	Graph G(V);
	WeightMap W = get(edge_weight, G);

	// Fill graph
	for (int i = 0; i < E; i++) {
		int u, v, c;
		Edge e;
		cin >> u >> v >> c;
		tie(e, tuples::ignore) = add_edge(u, v, G);
		W[e] = c;
	}

	// Find minimum spanning tree
	vector<Edge> mst; 
	kruskal_minimum_spanning_tree(G, back_inserter(mst));

	// Compute sum of the weights and build new graph
	int weights_sum = 0;
	vector<Edge>::iterator ebeg, eend = mst.end();
	for (ebeg = mst.begin(); ebeg != eend; ++ebeg)
		weights_sum += W[*ebeg];
	
	// Run dijkstra and find furthest point
	vector<Vertex> pred(V);	
	vector<int> dist(V);
	dijkstra_shortest_paths(G, 0,
		predecessor_map(
			make_iterator_property_map(
				pred.begin(),
				get(vertex_index, G)
			)
		).
		distance_map(
			make_iterator_property_map(
				dist.begin(),
				get(vertex_index, G)
			)
		)
	);
	int furthest = *max_element(dist.begin(), dist.end());

	// Write result
	cout << weights_sum << " " << furthest << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
