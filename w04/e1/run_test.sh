#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -std=c++11 main.cpp -o main

./main < test/graphs.tin > test/result.out
diff test/graphs.tout test/result.out 

echo "Test 1 competed!"
