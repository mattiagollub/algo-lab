//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Buddy selection			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <unordered_map>
#include <algorithm>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						property<edge_weight_t, int>> Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph, edge_weight_t>::type WeightMap;

void testcase() {

	// Read problem settings	
	int V, C, F;
	cin >> V >> C >> F;

	// Read edge data
	Graph G(V);
	Graph OG(V);
	WeightMap W = get(edge_weight, G);
	std::unordered_map<string, vector<int>> topics;

	for (int v = 0; v < V; v++) {
		for (int i = 0; i < C; i++) {
			// Read interest
			string topic;
			cin >> topic;
			
			// Find matching students
			auto interested = topics.find(topic);
			if (interested != topics.end()) {
				for (int student : interested->second) {
					Edge e; bool found;
					tie(e, found) = edge(v, student, G);
					if (!found) {
						tie(e, tuples::ignore) = add_edge(v, student, G);
						W[e] = 1;
					}
					else {
						W[e] += 1;
					}	
				}
			}

			// Save student interests
			if (topics.find(topic) == topics.end())
				topics[topic] = vector<int>();
			topics[topic].push_back(v);
		}		
	}

	// Recreate graph containing only the edges with enough common interests
	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg) {
		Edge e = *ebeg;
		if (W[e] >= F + 1) {
			add_edge(source(e, G), target(e, G), OG);
		}
	}

	// Find bipartite matching
	std::vector<Vertex> mate(V);
  	edmonds_maximum_cardinality_matching(OG, &mate[0]);
	int size = matching_size(OG, &mate[0]);

	// Write result
	if (size == V / 2)
		cout << "not optimal\n";
	else
		cout << "optimal\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
