#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 main.cpp -o main

./main < test/buddies.sin > test/result.out
diff test/buddies.sout test/result.out 
echo "Test 1 competed!"

./main < test/buddies.tin > test/result.out
diff test/buddies.tout test/result.out
echo "Test 2 competed!"
