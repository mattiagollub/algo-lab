#!/bin/bash

# Run the algorithm and compare its output with the sample output
make

./main < test/hiking-maps-sample.in > test/result.out
diff test/hiking-maps-sample.out test/result.out 
echo "Test 1 competed!"

./main < test/hiking-maps-1.in > test/result.out
diff test/hiking-maps-1.out test/result.out 
echo "Test 2 competed!"

./main < test/hiking-maps-2.in > test/result.out
diff test/hiking-maps-2.out test/result.out 
echo "Test 3 competed!"
