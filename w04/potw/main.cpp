//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Antenna					    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	07.01.2016						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <cmath>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel	K;

using namespace std;

struct Triangle {
	pair<K::Point_2, K::Point_2> v[3];
};

void testcase() {
	
	// Read testcase settings
	int m, n;
	cin >> m >> n;

	// Read path points
	vector<K::Point_2> path;
	for (int i = 0; i < m; i++) {
		K::Point_2 p;
		cin >> p;
		path.push_back(p);
	}

	// Read triangles
	vector<Triangle> t(n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < 3; j++) {
			cin >> t[i].v[j].first;
			cin >> t[i].v[j].second;
		}
		// Swap points if necessary in order to preserve left-turn property
		for (int j = 0; j < 3; j++) {
			if (CGAL::right_turn(t[i].v[j].first, t[i].v[j].second, t[i].v[(j+1)%3].first))
				swap(t[i].v[j].first, t[i].v[j].second);
		}
	}

	// Determine which legs are contained in each triangle
	vector<vector<int>> l(n, vector<int>());
	for (int i = 0; i < n; i++) {
		bool prev_in = false;
		for (int p = 0; p < m; p++) {
			bool curr_in = true;
			for (int j = 0; j < 3; j++) {
				if (CGAL::right_turn(t[i].v[j].first, t[i].v[j].second, path[p]))
					curr_in = false;
			}
			if (prev_in && curr_in)
				l[i].push_back(p-1);
			prev_in = curr_in;
		}
	}

	// Similar to search snippets find the smallest interval containing all the legs
	int a = 0, b = -1;
	int best_a = 1, best_b = INT_MAX;
	int uniques = 0;
	vector<int> counts(m, 0);
	do  {
		// Extend the window by one
		b++;
		for (int leg : l[b]) {
			if (++counts[leg] == 1) uniques++;
		}

		// If possible shrink the start of the window
		bool can_shrink = true;
		while (can_shrink) {
			for (int leg : l[a]) {
				if (counts[leg] <= 1) can_shrink = false;
			}
			if (can_shrink) {
				for (int leg : l[a]) {
					counts[leg]--;
				}
				a++;
			}
		}

		// Update best interval if possible
		if (b - a < best_b - best_a && uniques == m - 1) {
			best_a = a;
			best_b = b;
		}
	} while (b < n - 1);

	// Print result
	cout << best_b - best_a + 1 << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
