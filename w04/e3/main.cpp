//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Important bridges		    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	16.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/biconnected_components.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace boost;

struct edge_component_t {
	enum {
		num = 555
	};
    typedef edge_property_tag kind;
} edge_component;

typedef adjacency_list<vecS, vecS, undirectedS,
						no_property,
						property<edge_component_t, size_t>> Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph, edge_component_t>::type ComponentMap;

bool result_comparer(pair<int, int> i, pair<int, int> j) {
	return (i.first < j.first || (i.first == j.first && i.second <= j.second));
}

void testcase() {

	// Read problem settings	
	int V, E;
	cin >> V >> E;

	// Read edge data
	Graph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		add_edge(u-1, v-1, G);
	}

	// Find biconnected components
	ComponentMap C = get(edge_component, G);
	size_t num_components = biconnected_components(G, C);

	// If there is only one component we are done.
	// if (num_components <= 1) {
	//	cout << 0 << "\n";
	//	return;
	// }

	// Get edges for each component
	vector<vector<Edge>> components = vector<vector<Edge>>(num_components, vector<Edge>());
	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg)
		components[C[*ebeg]].push_back(*ebeg);
	
	// Find components with only one edge
	vector<pair<int, int>> bridges;
	for (int c = 0; c < components.size(); c++) {
		if (components[c].size() == 1) {
			Edge e = components[c][0];
			bridges.push_back(pair<int, int>(
				min(source(e, G), target(e, G)),
				max(source(e, G), target(e, G))));
		}
	}

	// Sort result
	sort(bridges.begin(), bridges.end());

	// Write result
	cout << bridges.size() << "\n";
	for (int b = 0; b < bridges.size(); b++)
		cout << bridges[b].first+1 << " " << bridges[b].second+1 << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
