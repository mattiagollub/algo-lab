#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 -g main.cpp -o main

echo "r < test/ant_challenge.tin" > gdb.txt
gdb --command=gdb.txt  main

echo "Test 1 competed!"
