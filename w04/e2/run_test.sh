#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -I /home/mattia/Scaricati/boost_1_57_0 -std=c++11 main.cpp -o main

./main < test/ant_challenge.tin > test/result.out
diff test/ant_challenge.tout test/result.out 

echo "Test 1 competed!"
