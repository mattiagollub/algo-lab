//============================================================================//
//									     									  //
//		Algorithms Lab - Week 4						     					  //
//		Ant Challenge			    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	16.11.2015						   							  //
//									   										  //
//============================================================================//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,
			no_property,
			property<edge_weight_t, int> > Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef property_map<Graph, edge_weight_t>::type WeightMap;

void testcase() {

	// Read problem settings	
	int T, E, S, a, b;
	cin >> T >> E >> S >> a >> b;

	// Read edge data
	Graph forest(T);
	Graph privates(T);
	WeightMap f_W = get(edge_weight, forest);
	WeightMap p_W = get(edge_weight, privates);
	vector<vector<int>> weights = vector<vector<int>>(S, vector<int>(E, 10000));
	vector<Edge> edges;

	for (int i = 0; i < E; i++) {
		int u, v;
		Edge e;
		cin >> u >> v;
		
		for (int s = 0; s < S; s++)
		 	cin >> weights[s][i];

		tie(e, tuples::ignore) = add_edge(u, v, forest);
		edges.push_back(e);
	}

	// Read hives positions
	vector<int> hives(S, 0);
	for (int h = 0; h < S; h++)
		cin >> hives[h];

	// Compute minimum spanning tree for each species and save weights
	for (int s = 0; s < S; s++) {		
		// Set weights
		for (int e = 0; e < E; e++)
			f_W[edges[e]] = weights[s][e];

		// Find minimum spanning tree (Prim's algorithm is teh one used 
		// by the ants for exploration)
		vector<Vertex> predecessors(T);
		prim_minimum_spanning_tree(forest, &predecessors[0], root_vertex(hives[s]));

		// Get edges belonging to the minimum spanning tree
		for (int v = 0; v < T; ++v) {
			Edge f_e, p_e; bool f_found, p_found;
			tie(f_e, f_found) = edge(v, predecessors[v], forest);
			tie(p_e, p_found) = edge(v, predecessors[v], privates);
			
			if (!f_found) continue;
			if (!p_found) {
				// Add edge if it doesn't exist in the tree
				tie(p_e, tuples::ignore) = add_edge(v, predecessors[v], privates);
				p_W[p_e] = f_W[f_e];				
			}
			else {
				// Set the weight of the edge to the minimum time until now
				p_W[p_e] = min(p_W[p_e], f_W[f_e]);
			}
		}
	}

	// Run dijkstra and find shortest path
	vector<Vertex> pred(T);
	vector<int> dist(T);
	dijkstra_shortest_paths(privates, a,
		predecessor_map(make_iterator_property_map(pred.begin(), get(vertex_index, privates))).
		distance_map(make_iterator_property_map(dist.begin(), get(vertex_index, privates))));

	// Write result
	cout << dist[b] << "\n";
}

int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int T;
	std::cin >> T;

	// Iterate over each test set	
	while (T--) testcase();
}
