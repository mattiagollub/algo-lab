//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Shelves									   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {

	// Make file I/O faster
	std::ios_base::sync_with_stdio(false);	

	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {

		// Read test data		
		int l, m, n;
		cin >> l;
		cin >> m;
		cin >> n;
 
		// Find optimal coverage using only 
		int ncount = l / n;
		int mcount = (l - n * ncount) / m;
		int coverage = ncount * n + m * mcount;

		int best_ncount = ncount;
		int best_mcount = mcount;
		int best_coverage = coverage;

		int steps = 0;

		// Progressively substitute shelves of type 'n' with shelves of type 'm'
		// in order to find the best coverage.
		while (ncount > 0 && steps <= n) {
			
			steps += 1;
			ncount -= 1;
			mcount = (l - n * ncount) / m;
			coverage = n * ncount + m * mcount;

			// If we found a better combination, update shelve counts.
			if (coverage > best_coverage) {
				best_ncount = ncount;
				best_mcount = mcount;
				best_coverage = coverage;
			}

			// Stop if we already reached the best solution
			if (coverage == l)
				break;
		}

		// Write result
		cout << best_mcount << ' '
			 << best_ncount << ' '
			 << l - best_coverage << '\n';
	}
}
