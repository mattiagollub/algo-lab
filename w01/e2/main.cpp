//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Dominoes								   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {

	// Make file I/O faster
	std::ios_base::sync_with_stdio(false);	

	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {
		int n;
		cin >> n;

		// Run test case
		int max_pos = 2;  // Furthest position reachable with the current tiles
		int height = 0;	  // Height of the current tile
		int cur_pos = 0;  // Position of the current tile
		int i = 0;

		for (i = 0; i < n; i++) {

			cur_pos += 1;
			cin >> height;

			// If the current tile has been reached by a falling tile, then let
			// it fall too and update the furthest reachable position.
			if (cur_pos < max_pos)
				max_pos = max(max_pos, cur_pos + height);
			else {
				cur_pos -= 1;
				break;
			}
		}

		// Ignore remaining tiles
		for (i = i + 1; i < n; i++) {
			cin >> height;
		}

		cout << cur_pos << '\n';
	}
}
