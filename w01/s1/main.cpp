//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Even Pairs								   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <algorithm>

using namespace std;

int main(int argc, char *argv[]) {
	
	int n;
	cin >> n;

	// Run test case
	int total = 0;
	int open_1 = 0;
	int open_2 = -1;

	for (int i = 0; i < n; i++) {

		// Read next bit
		int bit;
		cin >> bit;

		// Swap and increment number of open even sequences 
		if (bit == 1)
			swap(open_1, open_2);
		open_1 += 1;

		// Add number of open sequences to the total
		total += open_1;
	}

	cout << total << '\n';
}
