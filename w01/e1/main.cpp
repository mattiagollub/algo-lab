//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Build The Sum							   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	
	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {
		int n;
		cin >> n;

		// Run test case
		double sum = 0.0;
		double x = 0.0;

		for (int i = 0; i < n; i++) {
			cin >> x;
			sum += x;
		}

		cout << sum << '\n';
	}
}
