//============================================================================//
//									     									  //
//		Algorithms Lab - Week 2						     					  //
//		Problem of the week - 	    						   			 	  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	21.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>

using namespace std;



int main(int argc, char *argv[]) {

	// Make I/O operations faster
	std::ios_base::sync_with_stdio(false);

	int ts;
	cin >> ts;

	// Iterate over each test set	
	for (int t = 0; t < ts; t++) {

		int n, k;
		set<int> lighter, heavier, unweighted;
		cin >> n;
		cin >> k;

		// Initially insert all coins
		for (int c = 1; c <= n; c++) {
			lighter.insert(c);
			heavier.insert(c);
			unweighted.insert(c);
		}

		// Then progressively exclude coins using the informations given by the
		// weights
		for (int w = 0; w < k; w++) {

			int pi;
			set<int> ids_left, ids_right;
			char comp;
			cin >> pi;

			// Read coin identifiers and comparison
			for (int i = 0; i < pi; i++) {
				int id;
				cin >> id;
				ids_left.insert(id);
				unweighted.erase(id);
			}
			for (int i = 0; i < pi; i++) {
				int id;
				cin >> id;
				ids_right.insert(id);
				unweighted.erase(id);
			}
			cin >> comp;

			// Analyze data
			if (comp == '=') {
				for (auto it = ids_left.begin(); it != ids_left.end(); it++) {
					lighter.erase(*it);
					heavier.erase(*it);
				}
				for (auto it = ids_right.begin(); it != ids_right.end(); it++) {
					lighter.erase(*it);
					heavier.erase(*it);
				}
			}
			else {

				set<int> new_lighter, new_heavier;
				set<int>* lighter_ids;
				set<int>* heavier_ids;

				if (comp == '<') {
					lighter_ids = &ids_left;
					heavier_ids = &ids_right;
				}
				else {
					lighter_ids = &ids_right;
					heavier_ids = &ids_left;
				}

				set_intersection(lighter_ids->begin(), lighter_ids->end(),
								 lighter.begin(), lighter.end(),
								 inserter(new_lighter, new_lighter.begin()));
				set_intersection(heavier_ids->begin(), heavier_ids->end(),
								 heavier.begin(), heavier.end(),
								 inserter(new_heavier, new_heavier.begin()));

				lighter = new_lighter;
				heavier = new_heavier;
			}
		}

		// Output result
		set<int> candidates;
		set_symmetric_difference(lighter.begin(), lighter.end(),
								 heavier.begin(), heavier.end(),
								 inserter(candidates, candidates.begin()));
		
		if (n == 1)
			cout << 1;
		else if (candidates.size() == 1 && unweighted.size() <= 1)
			cout << *candidates.begin();
		else if (unweighted.size() == 1 && candidates.size() <= 1)
			cout << *unweighted.begin();		
		else
			cout << 0;

		cout << '\n';
	}
}
