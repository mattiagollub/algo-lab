#!/bin/bash

# Run the algorithm and compare its output with the sample output
g++ -std=c++11 main.cpp -o ex

./ex < test/sample_test1.in > test/result.out
diff test/sample_test1.out test/result.out 

echo "Test 1 competed!"

./ex < test/sample_test2.in > test/result.out
diff test/sample_test2.out test/result.out

echo "Test 2 competed!"

./ex < test/custom.in > test/result.out
diff test/custom.out test/result.out

echo "Test custom competed!"
