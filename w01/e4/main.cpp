//============================================================================//
//									     									  //
//		Algorithms Lab - Week 1						     					  //
//		Even Matrices							   							  //
//									   										  //
//		Author: Mattia Gollub						   						  //
//		Date: 	17.09.2015						   							  //
//									   										  //
//============================================================================//

#include <iostream>
#include <vector>

using namespace std;

// Add src to dst and leave the resul tin dst
void add_to_vector(vector<int>& dst, vector<int>& src) {
	
	for (int i = 0; i < dst.size(); i++)
		dst[i] = (src[i] == dst[i]) ? 0 : 1;
}

int main(int argc, char *argv[]) {

	// Make file I/O faster
	std::ios_base::sync_with_stdio(false);	

	int ts;
	cin >> ts;

	for (int t = 0; t < ts; t++) {

		// Read test data		
		int n;
		cin >> n;

		vector<vector<int>> M(n, vector<int>(n));
		vector<vector<int>> original(n, vector<int>(n));

		// Read matrix
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				cin >> M[i][j];
				original[i][j] = M[i][j];
			}		
		}

		// Run test case
		int total = 0;

		// Use the algorithm of the 'Even Pairs' problem on rows of different
		// heights. Runs in O(n^3).
		for (int b = 0; b < n; b++) {

			// Merge rows
			if (b != 0) {
				for (int r = 0; r < n - b; r++) {
					add_to_vector(M[r], original[r + b]);
				}
			}

			// Given a row height, iterate over each row.
			for (int i = 0; i < n - b; i++) {
				int open_1 = 0;
				int open_2 = -1;

				// Run the algorithm like in the 'Even Pairs' problem	
				for (int j = 0; j < n; j++) {

					// Read next bit
					int bit = M[i][j];

					// Swap and increment number of open even sequences 
					if (bit == 1)
						swap(open_1, open_2);
					open_1 += 1;

					// Add number of open sequences to the total
					total += open_1;
				}
			}
		}

		// Write result
		cout << total << '\n';
	}
}
